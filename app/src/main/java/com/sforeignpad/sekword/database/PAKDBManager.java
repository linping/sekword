package com.sforeignpad.sekword.database;

import android.content.Context;
import android.database.Cursor;

import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.structs.PAKITEMData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class PAKDBManager {
	public PAKDBCon mDbConnection;
	private Context mContext;
	public int mUid;

	public PAKDBManager(Context context) {
		this(context, -1);
	}

	public PAKDBManager(Context context, int uid) {
		mContext = context;
		mDbConnection = PAKDBCon.open(mContext);
		mUid = uid;
	}

	public String getFileName(int nWordID, int nType){
		String query = String.format("SELECT * FROM word_data WHERE word_id=%s AND type=%s", nWordID, nType);
		PAKITEMData iData;
		String strFileName = null;

		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				iData = new PAKITEMData(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2) ,cursor.getInt(3), cursor.getInt(4), cursor.getInt(5));


				String strPakName = Define.DATA_PATH + String.format("t%03d.pk", iData.nFileID);
				//FileInputStream fStream = new FileInputStream(strPakName);
				//byte[] buffer= new byte[iData.nSize];
				//fStream.read(buffer, iData.nOffset, iData.nSize);

				byte[] buffer= new byte[iData.nSize];
				RandomAccessFile rFile = new RandomAccessFile(strPakName, "r");
				rFile.seek(iData.nOffset);
				rFile.read(buffer);


				RandomAccessFile ff;
				String strExt = ".png";
				if(iData.nType == 0)
					strExt = ".png";
				else if(iData.nType == 1)
					strExt = ".mp3";
				else if(iData.nType == 2)
					strExt = ".mp4";

				strFileName = Define.TMP_PATH + String.format("%d", iData.nWordID) + strExt;
				FileOutputStream oStream = new FileOutputStream(strFileName);
				oStream.write(buffer);
				oStream.close();
				//fStream.close();

				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}

		return strFileName;
	}

	
	public void excuteQuery(String query) {		
		DBConnection db = DBConnection.open(mContext);

		if (db == null)
			return;

		db.updateTableData(query);		
		db.close();
	}
	
	public void close(){
		if (mDbConnection != null) {
			mDbConnection.close();
			mDbConnection = null;
		}
	}
}
