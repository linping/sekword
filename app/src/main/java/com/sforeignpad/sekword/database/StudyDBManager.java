package com.sforeignpad.sekword.database;

import java.util.ArrayList;

import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.utils.ESUtils;

import android.content.Context;
import android.database.Cursor;

public class StudyDBManager extends DBManager {
	
	public StudyDBManager(Context context, int uid) {
		super(context, uid);
	}
	
	public ArrayList<WordData> getWordList(ScheduleDateData data) {
		ArrayList<WordData> result = new ArrayList<WordData>();
		
		excuteQuery("DELETE FROM test_tmp");
		
		/*String query =  "SELECT " +
						"	c.*, b.value " +
						"FROM schedule_date AS a " +
						"	LEFT JOIN schedule_word AS b " +
						"		ON a.id=b.did " +
						"	LEFT JOIN word_dic AS c " +
						"		ON b.wid=c.id " +
						"WHERE a.id="+data.getId();*/
		
		String query = "SELECT * FROM schedule_word";
		ArrayList<Integer[]> result1 = new ArrayList<Integer[]>();
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						Integer[] item = {cursor.getInt(0), cursor.getInt(1), cursor.getInt(3)};
						result1.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		
		query =  "SELECT " +
				"	c.*, b.value " +
				"FROM schedule_date AS a " +
				"	LEFT JOIN schedule_word AS b " +
				"		ON a.id=b.did " +
				"	LEFT JOIN word_dic AS c " +
				"		ON b.wid=c.id " +
				"WHERE a.id="+data.getId();
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						
						WordData item = new WordData(cursor.getInt(0), 
													 cursor.getString(2),
													 cursor.getString(1),
													 cursor.getString(0), 
													 cursor.getString(0), 
													 cursor.getInt(5), 
													 cursor.getInt(7),
													 cursor.getString(6));
						result.add(item);
						excuteQuery(String.format("INSERT INTO test_tmp (wid) VALUES (%s)", cursor.getInt(0)));
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		/*query = "SELECT b.* " +
				"FROM (SELECT wid " +
				"	   FROM word_pad " +
				"	   WHERE NOT(wid IN (SELECT b.wid FROM schedule_date AS a LEFT JOIN schedule_word AS b ON a.id=b.did WHERE a.id="+data.getId()+")) AND " +
				"            isComplete=0 AND " +
				"            uid=" + mUid +
				"      ORDER BY id DESC " +
				"      LIMIT 5) AS a " +
				"LEFT JOIN word_dic AS b ON a.wid=b.id";
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						
						WordData item = new WordData(cursor.getInt(0), 
													 cursor.getString(1), 
													 cursor.getString(2), 
													 cursor.getString(0), 
													 cursor.getString(0), 
													 cursor.getInt(5), 
													 0);
						result.add(item);
						excuteQuery(String.format("INSERT INTO test_tmp (wid) VALUES (%s)", cursor.getInt(0)));
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		*/
		return result;
	}
	
	public int getWordCount(int level) {
		return getValueInt(String.format("SELECT COUNT(id) FROM word_dic WHERE level=%s", level));
//		return getValueInt(String.format("SELECT COUNT(id) FROM word_dic WHERE level=1"));
	}
	
	public ArrayList<WordData> getLevelTestList(int kind) { 
		ArrayList<WordData> result = new ArrayList<WordData>();
		
		String query = String.format("SELECT b.* FROM test_dic AS a LEFT JOIN word_dic AS b ON a.wid=b.id WHERE a.kind=%s AND a.level=%s", kind, 5);
		
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null) {
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						
						WordData item = new WordData(cursor.getInt(0), 
													 cursor.getString(1), 
													 cursor.getString(2), 
													 cursor.getString(0), 
													 cursor.getString(0), 
													 cursor.getInt(5), 
													 0);
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	
	public ArrayList<WordData> getLevelTestList1(int kind) { 
		ArrayList<WordData> result = new ArrayList<WordData>();
		
//		SELECT * FROM word_dic where level = 2 ORDER BY RANDOM() LIMIT 5;
		int[] levelTestWordCounts = new int[5];
		
		levelTestWordCounts[0] = 5;
		levelTestWordCounts[1] = 8;
		levelTestWordCounts[2] = 14;
		levelTestWordCounts[3] = 8;
		levelTestWordCounts[4] = 5;
		
		for (int index = 0; index < levelTestWordCounts.length; index++) {
			String query = String.format("SELECT * FROM word_dic WHERE level=%s ORDER BY RANDOM() LIMIT %s",index+1,levelTestWordCounts[index]);
			try{
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null) {
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {
							
							WordData item = new WordData(cursor.getInt(0), 
														 cursor.getString(1), 
														 cursor.getString(2), 
														 cursor.getString(0), 
														 cursor.getString(0), 
														 cursor.getInt(5), 
														 0);
							result.add(item);
							cursor.moveToNext();
						}
					}
					cursor.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}		
		return result;
	}
	
	
	public ArrayList<WordData> getEachLevelTestList(int level) { 
		ArrayList<WordData> result = new ArrayList<WordData>();
		
		int levelTestWordCounts = 0;
		
		switch (level) {
		case 1:
			levelTestWordCounts = Define.LEVEL1TESTWORDCOUNT;
			break;
		case 2:
			levelTestWordCounts = Define.LEVEL2TESTWORDCOUNT;
			break;
		case 3:
			levelTestWordCounts = Define.LEVEL3TESTWORDCOUNT;
			break;
		case 4:
			levelTestWordCounts = Define.LEVEL4TESTWORDCOUNT;
			break;
		case 5:
			levelTestWordCounts = Define.LEVEL5TESTWORDCOUNT;
			break;

		default:
			break;
		}
			String query = String.format("SELECT * FROM word_dic WHERE level=%s ORDER BY RANDOM() LIMIT %s",level,levelTestWordCounts);
			try{
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null) {
					int nCount = cursor.getCount();
					if(nCount > 0) {
						for(int i=0; i<nCount; i++) {
							
							WordData item = new WordData(cursor.getInt(0), 
														 cursor.getString(2),
														 cursor.getString(1),
														 cursor.getString(0), 
														 cursor.getString(0), 
														 cursor.getInt(5), 
														 0);
							result.add(item);
							cursor.moveToNext();
						}
					}
					cursor.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}			
		return result;
	}
	public ArrayList<WordData> getAllTextList() { 
		ArrayList<WordData> result = new ArrayList<WordData>();

		
		for (int i = 0; i < 5; i++) {
			ArrayList<WordData> result_tmp = new ArrayList<WordData>();
			result_tmp = getEachLevelTestList(i+1);
			
			for (int j = 0; j < result_tmp.size(); j++) {
				result.add(result_tmp.get(j));
			}
			
		}
	
		return result;
	}

	public ArrayList<WordData> getAllTextList(int nStart, int nEnd) {
		ArrayList<WordData> result = new ArrayList<WordData>();


		for (int i = nStart; i <= nEnd; i++) {
			ArrayList<WordData> result_tmp = new ArrayList<WordData>();
			result_tmp = getEachLevelTestList(i+1);

			for (int j = 0; j < result_tmp.size(); j++) {
				result.add(result_tmp.get(j));
			}

		}

		return result;
	}

	
	public ArrayList<WordData> getTestList(ScheduleDateData data) { 
		ArrayList<WordData> result = new ArrayList<WordData>();
		
		ArrayList<Integer> mTestDic = getListValueInt("SELECT wid FROM test_tmp");
		ArrayList<Integer> mWordIds = new ArrayList<Integer>();
		for(int i=0; i<mTestDic.size(); i++) {
			
			if (i>9) break;
			
			int mWId = ESUtils.getRandomValue(mTestDic.size(), mWordIds);
			mWordIds.add(mWId);
			
			String query = "SELECT * FROM word_dic WHERE id="+mTestDic.get(mWId);
			try {
				Cursor cursor = mDbConnection.query(query);
				if(cursor != null){
					int nCount = cursor.getCount();
					if(nCount > 0) {					
						for(int j=0; j<nCount; j++) {
							
							WordData item = new WordData(cursor.getInt(0), 
														 cursor.getString(2),
														 cursor.getString(1),
														 cursor.getString(0), 
														 cursor.getString(0), 
														 cursor.getInt(5), 
														 0);
							result.add(item);
							excuteQuery(String.format("UPDATE test_tmp SET isComplete=1 WHERE wid=%s", cursor.getInt(0)));
							cursor.moveToNext();
						}
					}
					cursor.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
				close();
			}
		}
		
		return result;
	}
	
	public void setFormatTestTmpTable() { 
		excuteQuery("UPDATE test_tmp SET isComplete=0");
	}
	
	public void setWordpadComplete() { 
		ArrayList<Integer> mWordId = getListValueInt(String.format("SELECT * FROM word_pad AS a LEFT JOIN test_tmp AS b ON a.wid=b.wid WHERE a.uid=%s", mUid));
		for (int i=0; i<mWordId.size(); i++) {
			excuteQuery(String.format("UPDATE word_pad SET isComplete=1 WHERE wid=%s AND uid=%s", mWordId.get(i), mUid));
		}
	}
	
	public int getTestWordTotalCount() {
		return getValueInt(String.format("SELECT COUNT(id) FROM test_tmp"));
	}
	
	
	
	public void setTimes(ScheduleDateData data) { 
		excuteQuery(String.format("UPDATE schedule_date SET times=times+%s WHERE id=%s", data.getTimes(),data.getId()));
	}
	
	public void setMarks(ScheduleDateData data) {
		excuteQuery(String.format("UPDATE schedule_date SET marks=%s WHERE id=%s", data.getMarks(), data.getId()));
	}
	
	public void setDate(ScheduleDateData data) { 
		excuteQuery(String.format("UPDATE schedule_date SET sDate=%s WHERE id=%s", data.getDate(), data.getId()));
	}
	
	public void setWordComplete(int did, int wid) { 
		int value = getValueInt(String.format("SELECT value FROM schedule_word WHERE did=%s AND wid=%s", did, wid));
		value = (value==0) ? 1 : 0;
		excuteQuery(String.format("UPDATE schedule_word SET value=%s WHERE did=%s AND wid=%s", value, did, wid));
	}
	
}