package com.sforeignpad.sekword.database;

import java.util.ArrayList;

import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.structs.ScheduleData;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.utils.ESUtils;

import android.content.Context;
import android.database.Cursor;

public class ScheduleDBManager extends DBManager {
	
	public ScheduleDBManager(Context context, int uid) {
		super(context, uid);
	}
	
	public void createSchedule(ScheduleData data) {
		String query = "";
		
		query = String.format("INSERT INTO schedule (uid, level, startDate, days, wordCount) VALUES ('%s', '%s', '%s', '%s', '%s')", 
				  			   mUid, data.getLevel(), data.getStartDate(), data.getDays(), data.getWordCount());
		excuteQuery(query);
		
		query = String.format("UPDATE user SET level=%s WHERE id=%s", data.getLevel(), mUid);
		excuteQuery(query);
		
		int id = getValueInt(String.format("SELECT id FROM schedule WHERE uid=%s AND level=%s", mUid, data.getLevel()));
		createScheduleDate(id, data);
	}
	
	private void createScheduleDate(int id, ScheduleData data) {
		String query = "";
		
		for (int i=0; i<data.getDays(); i++) {
			query = String.format("INSERT INTO schedule_date (sid) VALUES ('%s')", id);
			excuteQuery(query);
		}		
	}
	
	public void createScheduleWord(int did) {
		if (getValueInt(String.format("SELECT COUNT(id) FROM schedule_word WHERE did=%s", did)) == 0) {
			
			String query = "";
			ScheduleData data = getScheduleData();
			
			///////////////////////////////////////////
//			data.setLevel(level);
			/////////////////////////////////////////////
			
			/*query =   "SELECT " +
					  "		d.id, " + did + " " +
					  "FROM word_dic AS d " +
					  "     LEFT JOIN (SELECT " +
					  "						c.wid AS wid" +
					  "                 FROM schedule AS a " +
					  "						LEFT JOIN schedule_date AS b ON a.id=b.sid " +
					  "						LEFT JOIN schedule_word AS c ON b.id=c.did " +
					  "					WHERE a.id=" + data.getId() + ") AS e " +
					  "		ON d.id<>e.wid " +
					  "WHERE d.level="+data.getLevel()+ " "+
					  "LIMIT "+data.getWordCount();*/
			
			query =   "SELECT id, " + did + " FROM word_dic WHERE level="+data.getLevel()+ " AND " +
					  "NOT(id IN (SELECT c.wid FROM schedule_word as c LEFT JOIN schedule_date as b ON b.id=c.did LEFT JOIN schedule as a ON a.id=b.sid WHERE a.id=" + data.getId() + ")) "+
					  "LIMIT "+data.getWordCount();
			
			excuteQuery(String.format("INSERT INTO schedule_word (wid, did) %s", query));
		}
	}
	
	public void updateSchedule(ScheduleData data) {
		String query = "";
		
		query = String.format("UPDATE schedule SET days='%s', wordCount='%s' WHERE id=%s", data.getDays(), data.getWordCount(), data.getId());
		excuteQuery(query);
		
		ArrayList<Integer> mDateIdList = getListValueInt(String.format("SELECT id FROM schedule_date WHERE sid=%s AND marks=0", data.getId()));
		for (int i=0; i<mDateIdList.size(); i++) {
			query = String.format("DELETE FROM schedule_word WHERE did=%s", mDateIdList.get(i));
			excuteQuery(query);
		}
		
		query = String.format("DELETE FROM schedule_date WHERE sid=%s AND marks<80", data.getId());
		excuteQuery(query);
		
		int studyDays = getStudyDays(data);
		data.setDays(data.getDays() - studyDays);
		createScheduleDate(data.getId(), data);
	}
	
	public void deleteSchedule() {
		String query = "";
		int id = getValueInt(String.format("SELECT id FROM schedule WHERE uid=%s", mUid));
		ArrayList<Integer> mDateIdList = getListValueInt(String.format("SELECT id FROM schedule_date WHERE sid=%s AND marks=0", id));
		for (int i=0; i<mDateIdList.size(); i++) {
			query = String.format("DELETE FROM schedule_word WHERE did=%s", mDateIdList.get(i));
			excuteQuery(query);
		}
		
		excuteQuery(String.format("DELETE FROM schedule_date WHERE sid=%s", id));
		excuteQuery(String.format("DELETE FROM schedule WHERE uid=%s", mUid));
		
		query = String.format("UPDATE user SET level=0 WHERE id=%s", mUid);
		excuteQuery(query);
	}
	
	public ArrayList<ScheduleDateData> getScheduleListFromActive() {
		String query = String.format("SELECT id FROM schedule WHERE uid=%s", mUid);
		return getScheduleList(getValueInt(query));
	}
	
	public ArrayList<ScheduleDateData> getScheduleListFromLevel(int level) {
		String query = String.format("SELECT id FROM schedule WHERE uid=%s AND level=%s", mUid, level);
		return getScheduleList(getValueInt(query));
	}
	
	private ArrayList<ScheduleDateData> getScheduleList(int id) {
		String query = String.format("SELECT * FROM schedule_date WHERE sid=%s ORDER BY id", id);
		ArrayList<ScheduleDateData> result = new ArrayList<ScheduleDateData>();
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null) {
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						ScheduleDateData item = new ScheduleDateData(cursor.getInt(0), cursor.getInt(1), cursor.getLong(2), cursor.getInt(3), cursor.getLong(4));
						result.add(item);
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	public ScheduleData getScheduleData() {
		int level = getValueInt(String.format("SELECT level FROM schedule WHERE uid=%s", mUid));
		return getScheduleData(level);
	}
	
	public ScheduleData getScheduleData(int level) {
		String query = String.format("SELECT * FROM schedule WHERE level=%s AND uid=%s", level, mUid);
		ScheduleData result = null;
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						result = new ScheduleData(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getLong(3), cursor.getInt(4), cursor.getInt(5));
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	public boolean IsExistScheduleData() {
		int result = getValueInt(String.format("SELECT COUNT(id) FROM schedule WHERE uid=%s", mUid));
		return (result>0) ? true : false;
	}
	
	public ScheduleDateData getScheduleDateData(int id) {
		String query = String.format("SELECT * FROM schedule_date WHERE id=%s ORDER BY id", id);
		ScheduleDateData result = null;
		try{
			Cursor cursor = mDbConnection.query(query);
			if(cursor != null){
				int nCount = cursor.getCount();
				if(nCount > 0) {
					for(int i=0; i<nCount; i++) {
						result = new ScheduleDateData(cursor.getInt(0), cursor.getInt(1), cursor.getLong(2), cursor.getInt(3), cursor.getLong(4));
						cursor.moveToNext();
					}
				}
				cursor.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			close();
		}
		
		return result;
	}
	
	public int getWordCount(ScheduleData data) {
		String query = String.format("SELECT COUNT(b.id) FROM schedule_date AS a LEFT JOIN schedule_word AS b ON a.id=b.did WHERE a.sid=%s AND a.marks>79", data.getId());
		return getValueInt(query);
	}	
	
	public int getWordCountOfDay(ScheduleDateData data) {
		String query = String.format("SELECT COUNT(id) FROM schedule_word WHERE did=%s", data.getId());
		return getValueInt(query);
	}
	
	public int getStudyDays(ScheduleData data) {
		String query = String.format("SELECT COUNT(id) FROM schedule_date WHERE sid=%s AND marks>79", data.getId());
		return getValueInt(query);
	}
	
	public long getLastDate(ScheduleData data) {
		long lastDate = getValueLong(String.format("SELECT MAX(sDate) FROM schedule_date WHERE sid=%s AND marks>79 LIMIT 1", data.getId()));
		lastDate = (lastDate == 0) ? ESUtils.getNowDateMilliseconds() : lastDate;
		return lastDate+(data.getDays()-getStudyDays(data)-1)*Define.ONE_DAY_TIME;
	}
	
	public int getLevel(int sid) {
		return getValueInt(String.format("SELECT b.level FROM schedule_date AS a LEFT JOIN schedule AS b ON a.sid=b.id WHERE a.id=%s", sid));
	}
	
}
