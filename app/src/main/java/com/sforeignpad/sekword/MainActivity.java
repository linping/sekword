package com.sforeignpad.sekword;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;

import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.structs.UserData;
import com.sforeignpad.sekword.ui.setting.EsSettingActivity;
import com.sforeignpad.sekword.ui.studding.EsStuddingActivity;
import com.sforeignpad.sekword.ui.studding.SekWordPopup;
import com.sforeignpad.sekword.ui.studyresult.EsStudyResultActivity;
import com.sforeignpad.sekword.ui.usereg.EsUserListActivity;
import com.sforeignpad.sekword.ui.words.EsWordsActivity;
import com.sforeignpad.sekword.utils.ESUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author LJG
 * 2015.6.26
 * 프로그람이 기동하여 보여지는 첫 화면
 */
public class MainActivity extends Activity implements View.OnClickListener
{
   
	public final static String MAIN_APP_ID = "com.sforeignpad.sekmain";
	private static String DICT_APP_ID = "com.app.dic.samhung1";

	private TextView m_tvOrder = null;
	
	private UserDBManager mDbMana = null;
	private UserData mUserData = null;
	
	private UserOrderReceiver m_receiver = null;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

		//CreateDialogs();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;

        float density = getResources().getDisplayMetrics().density;

		if(!ESUtils.isVerified()){
			finish();
		}
        
        Log.e("TEST", String.format("width = %d, height = %d, density = %f",  width, height, density));

       
        if(!createDB(this))
        	this.finish();

		ESUtils.initializeSounds(this);
		DeleteTemp();
        initViews();
        registerReceiver();
        

    }

    private void CreateDialogs(){
		CreateDialog(R.string.app_back);
		CreateDialog(R.string.start_study_phase1);
		CreateDialog(R.string.start_exercise);
		CreateDialog(R.string.noscheduledata);
		CreateDialog(R.string.new_schedule);
		CreateDialog(R.string.studyResultMsg);
		CreateDialog(R.string.user_del_msg);
		CreateDialog(R.string.word_del_msg);
		CreateDialog(R.string.study_back);
	}

	private void CreateDialog(int str){
		SekWordPopup popup;
		popup = new SekWordPopup(MainActivity.this);
		popup.setMessage(this.getString(str));
		popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

			@Override
			public void onOK() {

			}
			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
			}

		});
		popup.show();
	}


    // delete temp files
	private void DeleteTemp(){
		File dir = new File(Define.TMP_PATH);

		if(dir.exists()){
			if (dir.isDirectory())
			{
				String[] children = dir.list();
				for (int i = 0; i < children.length; i++)
				{
					new File(dir, children[i]).delete();
				}
			}
		}else{
			dir.mkdirs();
		}
	}

    //모든 뷰 초기화
    private void initViews() {
    	initUserInfoTextView();
    	initMenuButtons();
    	initDateViews();
    }

	//사용자이름 및 급수 초기화
    @SuppressLint("NewApi")
	private void initUserInfoTextView() {
    	TextView tvUserName = (TextView)findViewById(R.id.tvUserName);
    	m_tvOrder = (TextView)findViewById(R.id.tvOrder);
    	
    	ImageView ivPhoto = (ImageView)findViewById(R.id.ivUserPhoto);
    	ImageView ivPhotoMask = (ImageView)findViewById(R.id.ivUserPhotoMask);
    	
    	ESUtils.setTextViewTypeFaceByRes(this, tvUserName, Define.getMainFont());
    	ESUtils.setTextViewTypeFaceByRes(this, m_tvOrder, Define.getMainFont());
    	if (mDbMana == null) 
			mDbMana = new UserDBManager(this);
		
		if (mDbMana == null) return;
		
		mUserData = mDbMana.getActiveUser();
		
		if (mUserData == null) {
			tvUserName.setText(Html.fromHtml(String.format("<u>%s</u>", getString(R.string.user_reg))));
			m_tvOrder.setText("0");
			
			ivPhoto.setVisibility(View.INVISIBLE);
			ivPhotoMask.setVisibility(View.INVISIBLE);
		} else {
			tvUserName.setText(Html.fromHtml(String.format("<u>%s</u>", mUserData.getName())));
			m_tvOrder.setText(String.valueOf(mUserData.getLevel()));
			
			if (mUserData.getPhoto() == null || mUserData.getPhoto().isEmpty()) {
				ivPhoto.setVisibility(View.INVISIBLE);
				ivPhotoMask.setVisibility(View.INVISIBLE);
			} else {
				Bitmap bmpPhoto = BitmapFactory.decodeFile(mUserData.getPhoto());
				if (bmpPhoto == null)
				{
					ivPhoto.setVisibility(View.INVISIBLE);
					ivPhotoMask.setVisibility(View.INVISIBLE);
				}
				else
				{
					ivPhoto.setImageBitmap(bmpPhoto);
					
					ivPhoto.setVisibility(View.VISIBLE);
					ivPhotoMask.setVisibility(View.VISIBLE);
				}
			}
		}
    }
    
	//기본메뉴단추 초기화
    private void initMenuButtons() {
    	findViewById(R.id.tvUserName).setOnClickListener(this);
    	findViewById(R.id.btnPhoto).setOnClickListener(this);
    	findViewById(R.id.btnHelp).setOnClickListener(this);
    	findViewById(R.id.btnSetting).setOnClickListener(this);
    	findViewById(R.id.btnStudding).setOnClickListener(this);
    	findViewById(R.id.btnStudyResult).setOnClickListener(this);
    	findViewById(R.id.btnWord).setOnClickListener(this);
    	findViewById(R.id.btnDict).setOnClickListener(this);
    }
    
	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(this);
		switch(v.getId()) {
			case R.id.tvUserName:		goUserRegActivity();		break;
			case R.id.btnPhoto:			goPhotoActivity();			break;
			case R.id.btnHelp:			goHelpActivity();			break;
			case R.id.btnSetting:		goSettingActivity();		break;
			case R.id.btnStudding:		goStuddingActivity();		break;
			case R.id.btnStudyResult:	goStudyResultActivity();	break;
			case R.id.btnWord:			goWordActivity();			break;
			case R.id.btnDict:			goDictActivity();			break;
		}
	}

	//날자 설정
    private void initDateViews() {
    	TextView tvMonth = (TextView)findViewById(R.id.tvMonth);
    	ESUtils.setTextViewTypeFaceByRes(this, tvMonth, Define.getMainFont());
    	
    	ImageView ivDay1 = (ImageView)findViewById(R.id.ivDayNum1);
    	ImageView ivDay2 = (ImageView)findViewById(R.id.ivDayNum2);
    	
    	Calendar calendar = Calendar.getInstance();
    	
    	String strMonth = Integer.toString(calendar.get(Calendar.MONTH)+1);
    	tvMonth.setText(strMonth + getResources().getString(R.string.month));
    	
    	int date = calendar.get(Calendar.DATE);
    	if(date > 9) {
    		ivDay2.setVisibility(View.VISIBLE);
    		
    		String strDate = Integer.toString(date);
    		String strNum1 = strDate.substring(0, 1);
    		String strNum2 = strDate.substring(1, 2);
    		
    		ivDay1.setImageResource(R.drawable.main_num0 + Integer.valueOf(strNum1));
    		ivDay2.setImageResource(R.drawable.main_num0 + Integer.valueOf(strNum2));
    	}
    	else {
    		ivDay1.setImageResource(R.drawable.main_num0 + date);
    		ivDay2.setVisibility(View.GONE);
    	}
    }
    
    //현재 사용자가 선택되였는가 판단
    private boolean isUserSelected() {
    	if(mUserData == null)
    		return false;
    	
    	return true;
    }
    
    //사용자등록화면으로 이행
    private void goUserRegActivity() {
    	Intent intent = new Intent(MainActivity.this, EsUserListActivity.class);
    	MainActivity.this.startActivity(intent);
    }
    
    private void goPhotoActivity() {
    	Intent intent = new Intent(MainActivity.this, EsUserListActivity.class);
    	MainActivity.this.startActivity(intent);
    }

	@Override
	public void onBackPressed() {
		// your code.
		goHelpActivity();
	}
    //도움말화면으로 이행
    private void goHelpActivity() {
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(this.getString(R.string.study_back))
		.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
				StudyWordActivity.this.finish();

			}
		})
		.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.show();*/

			SekWordPopup popup;

			popup = new SekWordPopup(MainActivity.this);
			popup.setMessage(this.getString(R.string.app_back));
			popup.setListener(new SekWordPopup.OnSEKLaunchListener() {

				@Override
				public void onOK() {
					//gotoMainApp();
					MainActivity.this.finish();
					///System.exit(0);

				}

				@Override
				public void onCancel() {
					// TODO Auto-generated method stub
				}

			});

			popup.show();

    }
    
    //설정화면으로 이행
    private void goSettingActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}
		
    	Intent intent = new Intent(MainActivity.this, EsSettingActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	MainActivity.this.startActivity(intent);
    }
    
    //학습진행화면으로 이행
    private void goStuddingActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}

		Intent intent = new Intent(this, EsStuddingActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	startActivity(intent);
    }
    
    //학습결과화면으로 이행
    private void goStudyResultActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}

		Intent intent = new Intent(MainActivity.this, EsStudyResultActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	MainActivity.this.startActivity(intent);
    }
    
    //단어장화면으로 이행
    private void goWordActivity() {
		if( !isUserSelected() )
		{
			goUserRegActivity();
			return;
		}

		Intent intent = new Intent(MainActivity.this, EsWordsActivity.class);
    	intent.putExtra(Define.USER_ID, mUserData.getId());
    	MainActivity.this.startActivity(intent);
    }
    
    //사전화면으로 이행
    private void goDictActivity() {
		Intent launchIntent = getPackageManager().getLaunchIntentForPackage(DICT_APP_ID);
		if (launchIntent != null) {
			startActivity(launchIntent);//null pointer check in case package name was not found
		}
    }


    //자료기지창조
    private boolean createDB(Context context) {
    	try{
			File databaseFile = new File(Define.DB_PATH);

			if (!databaseFile.isDirectory()) {
				if (!databaseFile.mkdir()) 
					return false;
			}
			
			if (databaseFile.isDirectory()) {
				File dbFile = new File(Define.DB_PATH + "/" + Define.DBFileName);
				if(dbFile.exists())
					return true;
				
				FileOutputStream out = null;
				try {
					out = new FileOutputStream(dbFile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}
				
				int nRead = 0;	
				InputStream in = null;
				
				String strFile = "db/" + Define.DBFileName;
				in = context.getAssets().open(strFile);
					
				try {
					int len = in.available();
					byte[] buf = new byte[len];
					nRead = in.read(buf);
						
					out.write(buf, 0, nRead);						
				} catch(Exception e) {
					e.printStackTrace();
					in.close();
					out.close();
					return false;
				}
				in.close();				
				out.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}


		try{
			File databaseFile = new File(Define.DB_PATH);

			if (!databaseFile.isDirectory()) {
				if (!databaseFile.mkdir())
					return false;
			}

			if (databaseFile.isDirectory()) {
				File dbFile = new File(Define.DB_PATH + "/" + Define.PAKDBFileName);
				if(dbFile.exists())
					return true;

				FileOutputStream out = null;
				try {
					out = new FileOutputStream(dbFile);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				}

				int nRead = 0;
				InputStream in = null;

				String strFile = "db/" + Define.PAKDBFileName;
				in = context.getAssets().open(strFile);

				try {
					int len = in.available();
					byte[] buf = new byte[len];
					nRead = in.read(buf);

					out.write(buf, 0, nRead);
				} catch(Exception e) {
					e.printStackTrace();
					in.close();
					out.close();
					return false;
				}
				in.close();
				out.close();
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		initUserInfoTextView();
		
		super.onResume();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver();
		if(mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
		
		super.onDestroy();
	}

	//리시버 등록 및 해제
	private void registerReceiver() {
		IntentFilter filter = new IntentFilter(Define.SCHEDULE_VIEW_ACTION);
		
		m_receiver = new UserOrderReceiver();
		registerReceiver(m_receiver, filter);
	}
	
	private void unregisterReceiver() {
		if(m_receiver != null)
			unregisterReceiver(m_receiver);
	}
	
	private class UserOrderReceiver extends BroadcastReceiver {
	   public void onReceive(Context context, Intent intent) {
	    	if (intent.getAction().equals(Define.USER_ORDER_ACTION)) {
	    		m_tvOrder.setText(String.valueOf(mUserData.getLevel()));
	    	}
	    }
	}

	private void gotoMainApp(){
		Intent launchIntent = getPackageManager().getLaunchIntentForPackage(MAIN_APP_ID);
		if (launchIntent != null) {
			startActivity(launchIntent);//null pointer check in case package name was not found
		}
	}
}
