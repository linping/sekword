package com.sforeignpad.sekword.structs;

public class ScheduleDateData {
	
	private int id;
	private int sId;
	private long date;
	private int marks;
	private long times;
	
	public ScheduleDateData() {
		this(-1, -1, 0, 0, 0);
	}
	
	public ScheduleDateData(int id, int sId, long date, int marks, long times) {
		this.id = id; 
		this.sId = sId;
		this.date = date;
		this.marks = marks;
		this.times = times;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public long getTimes() {
		return times;
	}

	public void setTimes(long times) {
		this.times = times;
	}
	
}
