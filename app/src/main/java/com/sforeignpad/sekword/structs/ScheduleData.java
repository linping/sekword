package com.sforeignpad.sekword.structs;

public class ScheduleData {
	
	private int id;
	private int userId;
	private int level;
	private long startDate;
	private int days;
	private int wordCount;
	
	public ScheduleData() {
		this(-1, -1, 0, 0, 0, 0);
	}
	
	public ScheduleData(int id, int userId, int level, long startDate, int days, int wordCount) {
		this.id = id;
		this.level = level;
		this.days = days;
		this.userId = userId;
		this.startDate = startDate;
		this.wordCount = wordCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getWordCount() {
		return wordCount;
	}

	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}
	
}
