package com.sforeignpad.sekword.structs;

public class StudyResultData {
	private int day;
	private int value;
	
	public StudyResultData() {
		this(0,0);
	}
	
	public StudyResultData(int day, int value) {
		this.day = day;
		this.value = value;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
}
