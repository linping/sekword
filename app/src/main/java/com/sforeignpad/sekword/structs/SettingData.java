package com.sforeignpad.sekword.structs;

public class SettingData {
	
	private int id;
	private String name;
	private int value;
	private int active;
	
	public SettingData() {
		this.id = 0;
		this.value = 0;
		this.active = 0;
		this.name = "";
	}
	
	public SettingData(int id, int value, int active, String name) {
		this.id = id;
		this.value = value;
		this.active = active;
		this.name = name;
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
