package com.sforeignpad.sekword.structs;

public class DateData {

	private String date;
	private int days;
	private long startValue;
	private long endValue;
	
	public DateData() {
		this("",-1,-1, 0);
	}
	
	public DateData(String date, long startValue, long endValue, int days) {
		this.date = date;
		this.startValue = startValue;
		this.endValue = endValue;
		this.days = days;
	}


	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getStartValue() {
		return startValue;
	}

	public void setStartValue(long startValue) {
		this.startValue = startValue;
	}

	public long getEndValue() {
		return endValue;
	}

	public void setEndValue(long endValue) {
		this.endValue = endValue;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

}
