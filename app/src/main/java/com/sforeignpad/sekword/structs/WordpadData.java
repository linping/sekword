package com.sforeignpad.sekword.structs;

public class WordpadData {
	
	private int id;
	private int wid;
	private WordData word;
	
	public WordpadData() {
		this(-1, -1, new WordData());
	}
	
	public WordpadData(int id, int wid) {
		this(id, wid, new WordData());
	}
	
	public WordpadData(int id, int wid, WordData word) {
		this.id = id;
		this.wid = wid;
		this.word = word;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getWid() {
		return wid;
	}
	
	public void setWid(int wid) {
		this.wid = wid;
	}

	public WordData getWord() {
		return word;
	}

	public void setWord(WordData word) {
		this.word = word;
	}
	
}
