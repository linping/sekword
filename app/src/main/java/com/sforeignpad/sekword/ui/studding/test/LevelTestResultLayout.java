package com.sforeignpad.sekword.ui.studding.test;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.utils.ESUtils;

import java.util.ArrayList;

public class LevelTestResultLayout extends TestLayout implements View.OnClickListener {

	private Context m_context = null;

	private int m_nStudyWordCount = 50;

	private UserDBManager m_userDbMana = null;
	private OnLevelTestResultFinishListner m_listener;

	private TextView textResult;
	private ImageView imageStep;
	private ImageButton btnNextStep;

	private TextView textKoreans[][] = new TextView[5][3];
	private TextView textEnglishs[][] = new TextView[5][3];
	private ImageView imgCorrect[][] = new ImageView[5][3];

	private ImageView imgMasks[] = new ImageView[5];
	private FrameLayout frameLevels[] = new FrameLayout[5];

	public LevelTestResultLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;

		initLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.leveltestresult_layout, null);
		this.addView(v);

		textResult = (TextView) v.findViewById(R.id.textResult);
		imageStep = (ImageView) v.findViewById(R.id.imageStep);
		btnNextStep = (ImageButton) v.findViewById(R.id.btnNextStep);

		btnNextStep.setOnClickListener(this);

		imgMasks[0] = (ImageView) v.findViewById(R.id.imgMask1);
		imgMasks[1] = (ImageView) v.findViewById(R.id.imgMask2);
		imgMasks[2] = (ImageView) v.findViewById(R.id.imgMask3);
		imgMasks[3] = (ImageView) v.findViewById(R.id.imgMask4);
		imgMasks[4] = (ImageView) v.findViewById(R.id.imgMask5);

		frameLevels[0] = (FrameLayout) v.findViewById(R.id.frameLevel1);
		frameLevels[1] = (FrameLayout) v.findViewById(R.id.frameLevel2);
		frameLevels[2] = (FrameLayout) v.findViewById(R.id.frameLevel3);
		frameLevels[3] = (FrameLayout) v.findViewById(R.id.frameLevel4);
		frameLevels[4] = (FrameLayout) v.findViewById(R.id.frameLevel5);

		textKoreans[0][0] = (TextView) v.findViewById(R.id.textKorean_1_1);
		textKoreans[0][1] = (TextView) v.findViewById(R.id.textKorean_1_2);
		textKoreans[0][2] = (TextView) v.findViewById(R.id.textKorean_1_3);
		textKoreans[1][0] = (TextView) v.findViewById(R.id.textKorean_2_1);
		textKoreans[1][1] = (TextView) v.findViewById(R.id.textKorean_2_2);
		textKoreans[1][2] = (TextView) v.findViewById(R.id.textKorean_2_3);
		textKoreans[2][0] = (TextView) v.findViewById(R.id.textKorean_3_1);
		textKoreans[2][1] = (TextView) v.findViewById(R.id.textKorean_3_2);
		textKoreans[2][2] = (TextView) v.findViewById(R.id.textKorean_3_3);
		textKoreans[3][0] = (TextView) v.findViewById(R.id.textKorean_4_1);
		textKoreans[3][1] = (TextView) v.findViewById(R.id.textKorean_4_2);
		textKoreans[3][2] = (TextView) v.findViewById(R.id.textKorean_4_3);
		textKoreans[4][0] = (TextView) v.findViewById(R.id.textKorean_5_1);
		textKoreans[4][1] = (TextView) v.findViewById(R.id.textKorean_5_2);
		textKoreans[4][2] = (TextView) v.findViewById(R.id.textKorean_5_3);

		textEnglishs[0][0] = (TextView) v.findViewById(R.id.textEnglish_1_1);
		textEnglishs[0][1] = (TextView) v.findViewById(R.id.textEnglish_1_2);
		textEnglishs[0][2] = (TextView) v.findViewById(R.id.textEnglish_1_3);
		textEnglishs[1][0] = (TextView) v.findViewById(R.id.textEnglish_2_1);
		textEnglishs[1][1] = (TextView) v.findViewById(R.id.textEnglish_2_2);
		textEnglishs[1][2] = (TextView) v.findViewById(R.id.textEnglish_2_3);
		textEnglishs[2][0] = (TextView) v.findViewById(R.id.textEnglish_3_1);
		textEnglishs[2][1] = (TextView) v.findViewById(R.id.textEnglish_3_2);
		textEnglishs[2][2] = (TextView) v.findViewById(R.id.textEnglish_3_3);
		textEnglishs[3][0] = (TextView) v.findViewById(R.id.textEnglish_4_1);
		textEnglishs[3][1] = (TextView) v.findViewById(R.id.textEnglish_4_2);
		textEnglishs[3][2] = (TextView) v.findViewById(R.id.textEnglish_4_3);
		textEnglishs[4][0] = (TextView) v.findViewById(R.id.textEnglish_5_1);
		textEnglishs[4][1] = (TextView) v.findViewById(R.id.textEnglish_5_2);
		textEnglishs[4][2] = (TextView) v.findViewById(R.id.textEnglish_5_3);

		imgCorrect[0][0] = (ImageView) v.findViewById(R.id.imgCorrect_1_1);
		imgCorrect[0][1] = (ImageView) v.findViewById(R.id.imgCorrect_1_2);
		imgCorrect[0][2] = (ImageView) v.findViewById(R.id.imgCorrect_1_3);
		imgCorrect[1][0] = (ImageView) v.findViewById(R.id.imgCorrect_2_1);
		imgCorrect[1][1] = (ImageView) v.findViewById(R.id.imgCorrect_2_2);
		imgCorrect[1][2] = (ImageView) v.findViewById(R.id.imgCorrect_2_3);
		imgCorrect[2][0] = (ImageView) v.findViewById(R.id.imgCorrect_3_1);
		imgCorrect[2][1] = (ImageView) v.findViewById(R.id.imgCorrect_3_2);
		imgCorrect[2][2] = (ImageView) v.findViewById(R.id.imgCorrect_3_3);
		imgCorrect[3][0] = (ImageView) v.findViewById(R.id.imgCorrect_4_1);
		imgCorrect[3][1] = (ImageView) v.findViewById(R.id.imgCorrect_4_2);
		imgCorrect[3][2] = (ImageView) v.findViewById(R.id.imgCorrect_4_3);
		imgCorrect[4][0] = (ImageView) v.findViewById(R.id.imgCorrect_5_1);
		imgCorrect[4][1] = (ImageView) v.findViewById(R.id.imgCorrect_5_2);
		imgCorrect[4][2] = (ImageView) v.findViewById(R.id.imgCorrect_5_3);

		for(int i = 0; i < 5; i++){
			for (int j = 0; j < 3; j++){
				textKoreans[i][j].setSingleLine(true);
				textEnglishs[i][j].setSingleLine(true);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textKoreans[i][j].getLayoutParams();
				params.setMargins(25, 5, 25, 0);
				textKoreans[i][j].setLayoutParams(params);

				params = (LinearLayout.LayoutParams) textEnglishs[i][j].getLayoutParams();
				params.setMargins(25, 0, 25, 0);
				textEnglishs[i][j].setLayoutParams(params);

				ESUtils.setTextViewTypeFaceByRes(m_context, textKoreans[i][j], Define.getMainFont());
				ESUtils.setTextViewTypeFaceByRes(m_context, textEnglishs[i][j], Define.getEnglishFont());

				textKoreans[i][j].setTextSize(20);
				textEnglishs[i][j].setTextSize(20);

			}
		}

		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textResult), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textResult1), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textLevel1), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textLevel2), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textLevel3), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textLevel4), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.textLevel5), Define.getMainFont());

	}


	int nStartLevel = 0;
	int nEndLevel = 0;
	public void initData(){

		int nStudyLevel = -1;
		for(int i = 0; i < 5; i++){
			ArrayList<WordData> lvlData = getWordDataList(i+1);
			int nCompleteCount = 0;
			if(lvlData.size() == 3){
				for(int j = 0; j < 3; j++){
					WordData data = lvlData.get(j);
					textEnglishs[i][j].setText(data.getMeanEn());
					textKoreans[i][j].setText(data.getMeanKr());

					if(data.getComplete() == 1) {
						nCompleteCount++;
						imgCorrect[i][j].setVisibility(View.VISIBLE);
					}else{
						imgCorrect[i][j].setVisibility(View.INVISIBLE);
					}
				}
			}

			if(nCompleteCount <= 1 && nStudyLevel == -1) nStudyLevel = i;
		}

		if(nStudyLevel == -1) nStudyLevel = 4;

		imgMasks[nStudyLevel].setBackgroundResource(R.drawable.st_leveltest_line_two);
		imgMasks[nStudyLevel].setVisibility(View.VISIBLE);

		nStartLevel = nStudyLevel;
		nEndLevel = nStudyLevel;

		if(nStudyLevel > 0){
			imgMasks[nStudyLevel - 1].setBackgroundResource(R.drawable.st_leveltest_line_one);
			imgMasks[nStudyLevel - 1].setVisibility(View.VISIBLE);

			nStartLevel = nStudyLevel - 1;
		}

		if(nStudyLevel < 4){
			imgMasks[nStudyLevel + 1].setBackgroundResource(R.drawable.st_leveltest_line_one);
			imgMasks[nStudyLevel + 1].setVisibility(View.VISIBLE);

			nEndLevel = nEndLevel + 1;
		}

		textResult.setText(String.format("%s %d%s~%d%s %s", m_context.getString(R.string.string_leveltest_result1_1),
				nStartLevel + 1, m_context.getString(R.string.order) , nEndLevel + 1, m_context.getString(R.string.order) ,m_context.getString(R.string.string_leveltest_result1_2)));

	}

	public ArrayList<WordData> getWordDataList(int nLevel){
		ArrayList<WordData> rDataList = new ArrayList<WordData>();
		for(int i = 0; i < m_arrAllWordData.size(); i++){
			WordData data = m_arrAllWordData.get(i);
			if(data.getLevel() == nLevel){
				rDataList.add(data);
			}
		}

		for(int i = 0; i< rDataList.size(); i++){
			for(int j = 0; j > rDataList.size(); j++){
				WordData dataI = rDataList.get(i);
				WordData dataJ = rDataList.get(j);

				if(dataI.getComplete() == 0 && dataJ.getComplete() == 1){
					WordData tmp = dataI;
					dataI.setComplete(dataJ.getComplete());
					dataI.setId(dataJ.getId());
					dataI.setMeanEn(dataJ.getMeanEn());
					dataI.setMeanKr(dataJ.getMeanKr());

					dataJ.setComplete(tmp.getComplete());
					dataJ.setId(tmp.getId());
					dataJ.setMeanEn(tmp.getMeanEn());
					dataJ.setMeanKr(tmp.getMeanKr());
				}
			}
		}
		return rDataList;
	}

	public void setUserDbMana(UserDBManager dbMana) {
		m_userDbMana = dbMana;
	}
	
	public void setListener(OnLevelTestResultFinishListner listener) {
		m_listener = listener;
	}

	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(m_context);

		if(v.getId() == R.id.btnNextStep){
			m_listener.onAfter(nStartLevel, nEndLevel);
		}
	}

	public abstract interface OnLevelTestResultFinishListner {
		public abstract void onAfter(int nStartLevel, int nEndLevel);
	}
}
