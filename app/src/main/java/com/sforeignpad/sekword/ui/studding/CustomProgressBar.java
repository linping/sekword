/*
 * Copyright (C) 2010 Neil Davies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This code is base on the Android Gallery widget and was Created 
 * by Neil Davies neild001 'at' gmail dot com to be a Coverflow widget
 * 
 * @author Neil Davies
 */
package com.sforeignpad.sekword.ui.studding;

import com.sforeignpad.sekword.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class CustomProgressBar extends ProgressBar
{
	private Bitmap m_bmpBg = null;
	private Paint m_paint = new Paint();
	private Typeface m_tf = Typeface.create(Typeface.MONOSPACE, Typeface.BOLD);
	
	
	
	
	public CustomProgressBar(Context context)
	{
		super(context);
		initProgressBar();
	}
	 
	public CustomProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initProgressBar();
	}

	private void initProgressBar() {
		m_bmpBg = BitmapFactory.decodeResource(getResources(), R.drawable.schedule_progress_bg1);
	}
	
	@SuppressLint("DrawAllocation")
	public void onDraw(Canvas canvas)
	{
		if (m_bmpBg == null)
			return;
		
		canvas.drawBitmap(m_bmpBg, 0, 0, m_paint);
		
		m_paint.setColor(Color.rgb(255, 229, 87));
		
		int nProgrss = getProgress();
		if(nProgrss == 1) nProgrss++;
		
		RectF rect = new RectF();
		rect.left = 3;
		rect.top = 3;
		rect.right = rect.left + m_bmpBg.getWidth() * nProgrss/100 - 6;
		rect.bottom = m_bmpBg.getHeight() - 3;
		
		canvas.drawRoundRect(rect, 9.0f, 9.0f, m_paint);
		/*
		if(nProgrss != 100) {
			Rect rc = new Rect();
			rc.left = (int)rect.right - 10;
			rc.top = 3;
			rc.right = (int)rect.right+1;
			rc.bottom = (int)rect.bottom;
			canvas.drawRect(rc, m_paint);
		}*/
		
		m_paint.setTextSize(11);
		m_paint.setColor(Color.rgb(186, 99, 36));
		m_paint.setTextAlign(Paint.Align.LEFT);
		m_paint.setTypeface(m_tf);
		
		canvas.drawText(Integer.toString(nProgrss) + "%", m_bmpBg.getWidth()/2, m_bmpBg.getHeight() - 4, m_paint);
	}
}
