package com.sforeignpad.sekword.ui.words;

import com.sforeignpad.sekword.R;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

public class WordLauncherPopup extends Dialog implements View.OnClickListener {
	
	private Context mContext;
	private Button mBtnPlay;
	
	private OnWordLauncherListener mListener;

	public WordLauncherPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.words_launcher_popup, null);
		this.setContentView(localView);
		
		mBtnPlay = (Button)localView.findViewById(R.id.btnWordPlay);
		mBtnPlay.setOnClickListener(this);
	}
	
	public void setListener(OnWordLauncherListener listener) {
		mListener = listener;
	}
	
	@Override
	public void dismiss() {
	    super.dismiss();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btnWordPlay:
				mListener.onOK();
				this.dismiss();
				break;
		}
	}
	
	public abstract interface OnWordLauncherListener {
		public abstract void onOK();
	}

}
