package com.sforeignpad.sekword.ui.studding.test;

import java.util.ArrayList;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.utils.ESUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TestEngTypingLayout extends TestLayout {

	private Context m_context;
	
	private OnEngTypingTestFinishListener m_listener;
	
	private ImageView m_ivTrueMsg, m_ivFalseMsg;	//í‹€ë¦¼, í•©ê²©
	private TextView m_tvTrueWordBox;	//í‹€ë¦° ê²½ìš° ë‚˜ì˜¤ëŠ” ë‹µ
	boolean is_enable_thread = false;

	private LinearLayout m_layoutTyping;	//ìž…ë ¥í•œ ë¬¸ìž�ê°€ í˜„ì‹œ
	
	private TextView m_tvKorProblem;	//ì¡°ì„ ì–´ë¬¸ì œ
	private TextView m_tvTrueWords;		//ë§žížŒ ë‹¨ì–´ìˆ˜
	private TextView m_tvFalseWords;	//í‹€ë¦° ë‹¨ì–´ìˆ˜
	private TextView m_tvTotalWords;	//ì „ì²´ ë‹¨ì–´ìˆ˜
	
	private TextView tvengtypingtest_title1;	
	private TextView tvengtypingtest_title2;		
	private TextView tvengtypingtest_description;	
	
	private TestProgressBar m_progressbar = null;		//ProgressBar
	
	private boolean m_bShiftPressed = false; //Shiftê±´ì�´ ëˆŒë¦¬ìš´ ìƒ�íƒœ	
	private String m_strTypeWord = "";	//ìž…ë ¥í•œ ë¬¸ìž�
	private int m_nLetterCount = 0;		//ìž…ë ¥í•œ ë¬¸ìž�ìˆ˜
	private ArrayList<TextView> m_arrTvLetters = new ArrayList<TextView>();
	
	
	private Button btnKeyA;
	private Button btnKeyB;
	private Button btnKeyC;
	private Button btnKeyD;
	private Button btnKeyE;
	private Button btnKeyF;
	private Button btnKeyG;
	private Button btnKeyH;
	private Button btnKeyI;
	private Button btnKeyJ;
	private Button btnKeyK;
	private Button btnKeyL;
	private Button btnKeyM;
	private Button btnKeyN;
	private Button btnKeyO;
	private Button btnKeyP;
	private Button btnKeyQ;
	private Button btnKeyR;
	private Button btnKeyS;
	private Button btnKeyT;
	private Button btnKeyU;
	private Button btnKeyV;
	private Button btnKeyW;
	private Button btnKeyX;
	private Button btnKeyY;
	private Button btnKeyZ;
	private Button btnKeyShift;
	private Button btnKeyDel;
	
	
	final int MSG_NORMAL = 0;
	final int MSG_TIMEOUT = 1;

	public TestEngTypingLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		initLayout();
		initEngTypeTestLayout();
		initTypeButtons();
		initCheckButton();
	}
	
	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.engtypingtest_layout, null);
		this.addView(v);
	}

	private void initEngTypeTestLayout() {
		
		//Typeface textTypeFace_gulim = Typeface.createFromAsset(getContext().getAssets(), Define.TEXTFONT_GULIM_PATH);
		
		tvengtypingtest_title1 = (TextView)findViewById(R.id.tvengtypingtest_title1);
		tvengtypingtest_title2 = (TextView)findViewById(R.id.tvengtypingtest_title2);
		tvengtypingtest_description = (TextView)findViewById(R.id.tvengtypingtest_description);

		m_progressbar = (TestProgressBar)findViewById(R.id.progressbar);
		
		m_tvKorProblem = (TextView)findViewById(R.id.tvKorProblem);
		m_tvTrueWords = (TextView)findViewById(R.id.tvTrueWords);
		m_tvFalseWords = (TextView)findViewById(R.id.tvFalseWords);
		m_tvTotalWords = (TextView)findViewById(R.id.tvTotalWords);
		
		m_ivTrueMsg = (ImageView)findViewById(R.id.ivTrueMsg);
		m_ivFalseMsg = (ImageView)findViewById(R.id.ivFalseMsg);
		m_ivTrueMsg.setVisibility(View.GONE);
		m_ivFalseMsg.setVisibility(View.GONE);
		
		m_layoutTyping = (LinearLayout)findViewById(R.id.InsertLetterLayout);
		m_layoutTyping.removeAllViews();
		
		m_tvTrueWordBox = (TextView)findViewById(R.id.tvTrueWordBox);
		m_tvTrueWordBox.setVisibility(View.GONE);

		ESUtils.setTextViewTypeFaceByRes(m_context, tvengtypingtest_title1, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, tvengtypingtest_title2, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, tvengtypingtest_description, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvKorProblem, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvTrueWords, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvFalseWords, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvTotalWords, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvTrueWordBox, Define.getEnglishFont());

	}
	
	private void initTypeButtons() {
		btnKeyA = (Button)findViewById(R.id.btnKeyA);
		btnKeyA.setOnClickListener(m_clickListener);
		
		btnKeyB = (Button)findViewById(R.id.btnKeyB);
		btnKeyB.setOnClickListener(m_clickListener);
		
		btnKeyC = (Button)findViewById(R.id.btnKeyC);
		btnKeyC.setOnClickListener(m_clickListener);
		
		btnKeyD = (Button)findViewById(R.id.btnKeyD);
		btnKeyD.setOnClickListener(m_clickListener);
		
		btnKeyE = (Button)findViewById(R.id.btnKeyE);
		btnKeyE.setOnClickListener(m_clickListener);
		
		btnKeyF = (Button)findViewById(R.id.btnKeyF);
		btnKeyF.setOnClickListener(m_clickListener);
		
		btnKeyG = (Button)findViewById(R.id.btnKeyG);
		btnKeyG.setOnClickListener(m_clickListener);
		
		btnKeyH = (Button)findViewById(R.id.btnKeyH);
		btnKeyH.setOnClickListener(m_clickListener);
		
		btnKeyI = (Button)findViewById(R.id.btnKeyI);
		btnKeyI.setOnClickListener(m_clickListener);
		
		btnKeyJ = (Button)findViewById(R.id.btnKeyJ);
		btnKeyJ.setOnClickListener(m_clickListener);
		
		btnKeyK = (Button)findViewById(R.id.btnKeyK);
		btnKeyK.setOnClickListener(m_clickListener);
		
		btnKeyL = (Button)findViewById(R.id.btnKeyL);
		btnKeyL.setOnClickListener(m_clickListener);
		
		btnKeyM = (Button)findViewById(R.id.btnKeyM);
		btnKeyM.setOnClickListener(m_clickListener);
		
		btnKeyN = (Button)findViewById(R.id.btnKeyN);
		btnKeyN.setOnClickListener(m_clickListener);
		
		btnKeyO = (Button)findViewById(R.id.btnKeyO);
		btnKeyO.setOnClickListener(m_clickListener);
		
		btnKeyP = (Button)findViewById(R.id.btnKeyP);
		btnKeyP.setOnClickListener(m_clickListener);
		
		btnKeyQ = (Button)findViewById(R.id.btnKeyQ);
		btnKeyQ.setOnClickListener(m_clickListener);
		
		btnKeyR = (Button)findViewById(R.id.btnKeyR);
		btnKeyR.setOnClickListener(m_clickListener);
		
		btnKeyS = (Button)findViewById(R.id.btnKeyS);
		btnKeyS.setOnClickListener(m_clickListener);
		
		btnKeyT = (Button)findViewById(R.id.btnKeyT);
		btnKeyT.setOnClickListener(m_clickListener);
		
		btnKeyU = (Button)findViewById(R.id.btnKeyU);
		btnKeyU.setOnClickListener(m_clickListener);
		
		btnKeyV = (Button)findViewById(R.id.btnKeyV);
		btnKeyV.setOnClickListener(m_clickListener);
		
		btnKeyW = (Button)findViewById(R.id.btnKeyW);
		btnKeyW.setOnClickListener(m_clickListener);
		
		btnKeyX = (Button)findViewById(R.id.btnKeyX);
		btnKeyX.setOnClickListener(m_clickListener);
		
		btnKeyY = (Button)findViewById(R.id.btnKeyY);
		btnKeyY.setOnClickListener(m_clickListener);
		
		btnKeyZ = (Button)findViewById(R.id.btnKeyZ);
		btnKeyZ.setOnClickListener(m_clickListener);
		
		btnKeyShift = (Button)findViewById(R.id.btnKeyShift);
		btnKeyShift.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(m_bResultPressed) return false;
				
				if(event.getAction() == MotionEvent.ACTION_UP){				
					if(!m_bShiftPressed){
						m_bShiftPressed = true;
						changeUpperkeyboard();
					}else{ 
						m_bShiftPressed = false;
						changeLowerkeyboard();
					}
				}
				return false;
			}
		});
		
		btnKeyDel = (Button)findViewById(R.id.btnKeyDel);
		btnKeyDel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(m_bResultPressed) return;
				
				if(m_strTypeWord.length() > 0) {
					m_strTypeWord = m_strTypeWord.substring(0, m_strTypeWord.length()-1);	//ìž…ë ¥í•œ ë¬¸ìž�ë ¬ì—�ì„œ í•œ ë¬¸ìž� ì—†ì•¤ë‹¤.
					m_nLetterCount --;
					if(m_nLetterCount < 0)
						m_nLetterCount = 0;
					
					m_arrTvLetters.get(m_nLetterCount).setText("");
				}
			}
		});

		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyA, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyB, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyC, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyD, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyE, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyF, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyG, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyH, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyI, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyJ, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyK, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyL, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyM, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyN, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyO, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyP, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyQ, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyR, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyS, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyT, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyU, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyV, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyW, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyX, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyY, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyZ, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyDel, Define.getEnglishFont());
		ESUtils.setButtonTypeFaceByRes(m_context, btnKeyShift, Define.getEnglishFont());
	}
	
	@SuppressLint("DefaultLocale")
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			ESUtils.playBtnSound(m_context);

			if(m_bResultPressed) return;
			
			String strLetter = "";
			
			switch(v.getId()) {
			case R.id.btnKeyA:	strLetter = "a";	break;
			case R.id.btnKeyB:	strLetter = "b";	break;
			case R.id.btnKeyC:	strLetter = "c";	break;
			case R.id.btnKeyD:	strLetter = "d";	break;
			case R.id.btnKeyE:	strLetter = "e";	break;
			case R.id.btnKeyF:	strLetter = "f";	break;
			case R.id.btnKeyG:	strLetter = "g";	break;
			case R.id.btnKeyH:	strLetter = "h";	break;
			case R.id.btnKeyI:	strLetter = "i";	break;
			case R.id.btnKeyJ:	strLetter = "j";	break;
			case R.id.btnKeyK:	strLetter = "k";	break;
			case R.id.btnKeyL:	strLetter = "l";	break;
			case R.id.btnKeyM:	strLetter = "m";	break;
			case R.id.btnKeyN:	strLetter = "n";	break;
			case R.id.btnKeyO:	strLetter = "o";	break;
			case R.id.btnKeyP:	strLetter = "p";	break;
			case R.id.btnKeyQ:	strLetter = "q";	break;
			case R.id.btnKeyR:	strLetter = "r";	break;
			case R.id.btnKeyS:	strLetter = "s";	break;
			case R.id.btnKeyT:	strLetter = "t";	break;
			case R.id.btnKeyU:	strLetter = "u";	break;
			case R.id.btnKeyV:	strLetter = "v";	break;
			case R.id.btnKeyW:	strLetter = "w";	break;
			case R.id.btnKeyX:	strLetter = "x";	break;
			case R.id.btnKeyY:	strLetter = "y";	break;
			case R.id.btnKeyZ:	strLetter = "z";	break;
			}
			
			if(m_bShiftPressed)
				strLetter = strLetter.toUpperCase();
			
			m_strTypeWord += strLetter;
			
			m_nLetterCount ++;
			if(m_nLetterCount > m_arrTvLetters.size())
				m_nLetterCount = m_arrTvLetters.size();
			
			displayLetter(strLetter);
		}
	};
	
	private void insertLetterTextView(String strLetter, boolean bSpace) {
		TextView tvLetter = new TextView(m_context);
		tvLetter.setText(strLetter);
		tvLetter.setTextSize(20);
		//tvLetter.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		tvLetter.setTextColor(Color.rgb(0, 240, 255));
		tvLetter.setGravity(Gravity.CENTER);
		tvLetter.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.underline);

		ESUtils.setTextViewTypeFaceByRes(m_context, tvLetter, Define.getEnglishFont());

		if(bSpace) {
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)m_arrTvLetters.get(0).getLayoutParams();
			params.leftMargin = 7;
			tvLetter.setLayoutParams(params);
		}
		
		m_arrTvLetters.add(tvLetter);
		m_layoutTyping.addView(tvLetter);
	}
	
	private void displayLetter(String strLetter) {
		if (m_nLetterCount > 0)
			m_arrTvLetters.get(m_nLetterCount-1).setText(strLetter);
	}
	
	private void initCheckButton() {
		Button btnCheck = (Button)findViewById(R.id.btnCheck);
		btnCheck.setOnClickListener(new View.OnClickListener() {
			
			@SuppressLint("HandlerLeak")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(m_bResultPressed) return;
				
				m_bResultPressed = true;
				
				checkResult();
				setTestResult();
				
				new Handler() {
					public void handleMessage(Message msg) {						
						m_nTestTime = 0;
						m_bResultPressed = false;
					}
				}.sendEmptyMessageDelayed(MSG_NORMAL, 1000);
			}
		});
	}
	
	private void checkResult()
	{
		int nWordIndex;
		if(m_nCurrentTestProblemCount - 1 < m_arrWordData.size()){
			nWordIndex = m_nCurrentTestProblemCount;
		}else{
			nWordIndex = m_nTestCount;
		}

		if(m_strTypeWord.equals(m_arrWordData.get(nWordIndex-1).getMeanEn().replace(" ", ""))) {	//ìž…ë ¥í•œ ë¬¸ìž�ë“¤ì�´ ë§žë‹¤ë©´
			is_successProblem[m_nCurrentTestProblemCount-1] = true;
			m_nTrueResCount ++;					
			m_ivTrueMsg.setVisibility(View.VISIBLE);
			StudyTestActivity.playWordVoice("success");
		}
		else {
			doWrongProblemPeform();
		}
	}
	
	//ì‹œí—˜ê²°ê³¼ í˜„ì‹œ
	private void setTestResult() {
		m_nTestCurrentProblemIndex++;


		m_tvTrueWords.setText(Integer.toString(m_nTrueResCount));
		m_tvFalseWords.setText(Integer.toString(m_nFalseResCount));
		m_tvTotalWords.setText(Integer.toString(m_nCurrentTestProblemCount) + "/" 
									+ Integer.toString(m_nTotalTestProblemCount));
	}
	
	private void doWrongProblemPeform() {
		m_nFalseResCount ++;
		is_successProblem[m_nCurrentTestProblemCount-1] = false;

		m_ivFalseMsg.setVisibility(View.VISIBLE);

		m_tvTrueWordBox.setVisibility(View.VISIBLE);


        int nWordIndex;
        if(m_nCurrentTestProblemCount - 1 < m_arrWordData.size()){
            nWordIndex = m_nCurrentTestProblemCount;
        }else{
            nWordIndex = m_nTestCount;
        }

		m_tvTrueWordBox.setText(m_arrWordData.get(nWordIndex-1).getMeanEn());
		StudyTestActivity.playWordVoice("fail");
		
		//í‹€ë¦° ë‹¨ì–´ ë‹¨ì–´ìž¥ ì¶”ê°€
		insertWrongWord(m_arrWordData.get(nWordIndex-1).getId());
	}
	
	//ë¬¸ì œì œì‹œ
	@SuppressLint("HandlerLeak")
	private void setTestProblem() {
		if(m_nTestCount  > m_tvTotalProblems-1) {
			m_bTestStop = true;
			m_bResultPressed = true;
			
			new Handler() {
				public void handleMessage(Message msg) {
					m_bResultPressed = false;
					m_listener.onAfter(m_nCurrentTestProblemCount, m_nTrueResCount, m_nFalseResCount,m_nTestCurrentProblemIndex,is_successProblem,m_arrAllWordData);
				}
			}.sendEmptyMessageDelayed(MSG_NORMAL, 1000);			
		}
		else {
			initProblems();

			int nWordIndex;
			if(m_nCurrentTestProblemCount < m_arrWordData.size()){
				nWordIndex = m_nCurrentTestProblemCount;
			}else{
				nWordIndex = m_nTestCount;
			}

			m_tvKorProblem.setText(m_arrWordData.get(nWordIndex).getMeanKr());
			//m_tvKorProblem.setText(m_arrWordData.get(m_nTestCount).getMeanEn());
			
			addLetterTextView(m_arrWordData.get(nWordIndex).getMeanEn());
			
			m_nTestCount ++;
			m_nCurrentTestProblemCount++;
		}
		
		setTestResult();
	}
	
	private void initProblems() {
		m_ivTrueMsg.setVisibility(View.GONE);
		m_ivFalseMsg.setVisibility(View.GONE);		
		m_tvTrueWordBox.setVisibility(View.GONE);
		
		if(m_layoutTyping.getChildCount() > 0)
			m_layoutTyping.removeAllViews();
		
		m_arrTvLetters.clear();
		m_nLetterCount = 0;
		m_strTypeWord = "";
	}
	
	//ì‹œí—˜ë‹¨ì–´ê¸¸ì�´ë§Œí�¼ TextView ë„£ê¸°
	private void addLetterTextView(String strWord) {
		boolean bSpace = false;
		for(int i=0; i<strWord.length(); i++) {
			if(strWord.charAt(i) == ' ') {
				bSpace = true;
				continue;
			}
			
			insertLetterTextView("", bSpace);
			bSpace = false;
		}
	}
	
	//ì‹œí—˜ì‹œìž‘
	@SuppressLint("HandlerLeak")
	public void performEngTypingTest() {
		m_threadTest = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					switch(msg.what) {
					case MSG_NORMAL:
						setTestProblem();
						break;
					case MSG_TIMEOUT:
						m_bResultPressed = true;
						
						checkResult();
						setTestResult();
						
						new Handler() {
							public void handleMessage(Message msg) {						
								m_nTestTime = 0;
								m_bResultPressed = false;
							}
						}.sendEmptyMessageDelayed(MSG_NORMAL, 1000);
						
						break;
					}
				}
			};
			
			public void run() {
				while(!m_bTestStop) {
					pauseThread();
					sleepResultPressed();
					
					if(m_nTestTime == 0) 
						m_handler.sendEmptyMessage(MSG_NORMAL);	//ì‹œí—˜ë¬¸ì œ ì œì‹œ
					
					m_nTestTime += 5;
					
					if(m_nTestTime >= m_nTestLimitTime*1000)
						m_handler.sendEmptyMessage(MSG_TIMEOUT);
					
			
					
					Log.i("progress count:","Progress count:"+String.valueOf(m_nTestTime));
					
					m_progressbar.setProgress(100 * m_nTestTime / (m_nTestLimitTime *1000));
					try {
						Thread.sleep(5);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		getTestProblems(Define.KIND_TEST_TYPING);
		m_threadTest.setName("EngTypingTestThread");
		m_threadTest.setDaemon(true);
		m_threadTest.start();
	}
	
	//ë‹µì�„ ì„ íƒ�í•œí›„ 1ì´ˆìžˆë‹¤ê°€ ë‹¤ì�Œ ë¬¸ì œë¥¼ ì œì‹œ
	private void sleepResultPressed() {
		while(m_bResultPressed) {
			try {
				Thread.sleep(5);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void finishTest() {
		if(m_threadTest != null && m_threadTest.isAlive()) {
			m_bTestStop = true;
			try {
				m_threadTest.join();
			}
			catch(Exception e) {} 
		}
		
		super.finishTest();
	}
		
	public void setListener(OnEngTypingTestFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnEngTypingTestFinishListener {
		public abstract void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_);

	}
	
	
	public void changeUpperkeyboard(){
		btnKeyA.setText("A");
		btnKeyB.setText("B");
		btnKeyC.setText("C");
		btnKeyD.setText("D");
		btnKeyE.setText("E");
		btnKeyF.setText("F");
		btnKeyG.setText("G");
		btnKeyH.setText("H");
		btnKeyI.setText("I");
		btnKeyJ.setText("J");
		btnKeyK.setText("K");
		btnKeyL.setText("L");
		btnKeyM.setText("M");
		btnKeyN.setText("N");
		btnKeyO.setText("O");
		btnKeyP.setText("P");
		btnKeyQ.setText("Q");
		btnKeyR.setText("R");
		btnKeyS.setText("S");
		btnKeyT.setText("T");
		btnKeyU.setText("U");
		btnKeyV.setText("V");
		btnKeyW.setText("W");
		btnKeyX.setText("X");
		btnKeyY.setText("Y");
		btnKeyZ.setText("Z");
		btnKeyShift.setBackgroundResource(R.drawable.key_big_blue);
		btnKeyShift.setTextColor(Color.WHITE);
	}
	public void changeLowerkeyboard(){
		btnKeyA.setText("a");
		btnKeyB.setText("b");
		btnKeyC.setText("c");
		btnKeyD.setText("d");
		btnKeyE.setText("e");
		btnKeyF.setText("f");
		btnKeyG.setText("g");
		btnKeyH.setText("h");
		btnKeyI.setText("i");
		btnKeyJ.setText("j");
		btnKeyK.setText("k");
		btnKeyL.setText("l");
		btnKeyM.setText("m");
		btnKeyN.setText("n");
		btnKeyO.setText("o");
		btnKeyP.setText("p");
		btnKeyQ.setText("q");
		btnKeyR.setText("r");
		btnKeyS.setText("s");
		btnKeyT.setText("t");
		btnKeyU.setText("u");
		btnKeyV.setText("v");
		btnKeyW.setText("w");
		btnKeyX.setText("x");
		btnKeyY.setText("y");
		btnKeyZ.setText("z");
		btnKeyShift.setBackgroundResource(R.drawable.key_big);
		btnKeyShift.setTextColor(Color.parseColor("#282828"));

	}
}
