package com.sforeignpad.sekword.ui.studding.test;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.utils.ESUtils;

import java.util.ArrayList;

public class LevelTestResultFinalLayout extends TestLayout implements View.OnClickListener {

	private Context m_context = null;

	private int m_nStudyWordCount = 50;

	private UserDBManager m_userDbMana = null;
	private OnLevelTestResultFinishListner m_listener;

	private TextView textResult;
	private TextView textResultSub;

	private ImageView imageStep;
	private ImageButton btnNextStep;

	private TextView textKoreans[][] = new TextView[3][3];
	private TextView textEnglishs[][] = new TextView[3][3];
	private ImageView imgCorrect[][] = new ImageView[3][3];

	private TextView  textLevels[] = new TextView[3];
	private ImageView imgMasks[] = new ImageView[3];
	private FrameLayout frameLevels[] = new FrameLayout[3];

	public LevelTestResultFinalLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;

		initLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.leveltestresult_layout_final, null);
		this.addView(v);

		textResult = (TextView) v.findViewById(R.id.textResult);
		textResultSub = (TextView) v.findViewById(R.id.textResultSub);

		imageStep = (ImageView) v.findViewById(R.id.imageStep);
		btnNextStep = (ImageButton) v.findViewById(R.id.btnNextStep);

		btnNextStep.setOnClickListener(this);

		textLevels[0] = (TextView) v.findViewById(R.id.textLevel1);
		textLevels[1] = (TextView) v.findViewById(R.id.textLevel2);
		textLevels[2] = (TextView) v.findViewById(R.id.textLevel3);

		imgMasks[0] = (ImageView) v.findViewById(R.id.imgMask1);
		imgMasks[1] = (ImageView) v.findViewById(R.id.imgMask2);
		imgMasks[2] = (ImageView) v.findViewById(R.id.imgMask3);

		frameLevels[0] = (FrameLayout) v.findViewById(R.id.frameLevel1);
		frameLevels[1] = (FrameLayout) v.findViewById(R.id.frameLevel2);
		frameLevels[2] = (FrameLayout) v.findViewById(R.id.frameLevel3);

		textKoreans[0][0] = (TextView) v.findViewById(R.id.textKorean_1_1);
		textKoreans[0][1] = (TextView) v.findViewById(R.id.textKorean_1_2);
		textKoreans[0][2] = (TextView) v.findViewById(R.id.textKorean_1_3);
		textKoreans[1][0] = (TextView) v.findViewById(R.id.textKorean_2_1);
		textKoreans[1][1] = (TextView) v.findViewById(R.id.textKorean_2_2);
		textKoreans[1][2] = (TextView) v.findViewById(R.id.textKorean_2_3);
		textKoreans[2][0] = (TextView) v.findViewById(R.id.textKorean_3_1);
		textKoreans[2][1] = (TextView) v.findViewById(R.id.textKorean_3_2);
		textKoreans[2][2] = (TextView) v.findViewById(R.id.textKorean_3_3);

		textEnglishs[0][0] = (TextView) v.findViewById(R.id.textEnglish_1_1);
		textEnglishs[0][1] = (TextView) v.findViewById(R.id.textEnglish_1_2);
		textEnglishs[0][2] = (TextView) v.findViewById(R.id.textEnglish_1_3);
		textEnglishs[1][0] = (TextView) v.findViewById(R.id.textEnglish_2_1);
		textEnglishs[1][1] = (TextView) v.findViewById(R.id.textEnglish_2_2);
		textEnglishs[1][2] = (TextView) v.findViewById(R.id.textEnglish_2_3);
		textEnglishs[2][0] = (TextView) v.findViewById(R.id.textEnglish_3_1);
		textEnglishs[2][1] = (TextView) v.findViewById(R.id.textEnglish_3_2);
		textEnglishs[2][2] = (TextView) v.findViewById(R.id.textEnglish_3_3);

		imgCorrect[0][0] = (ImageView) v.findViewById(R.id.imgCorrect_1_1);
		imgCorrect[0][1] = (ImageView) v.findViewById(R.id.imgCorrect_1_2);
		imgCorrect[0][2] = (ImageView) v.findViewById(R.id.imgCorrect_1_3);
		imgCorrect[1][0] = (ImageView) v.findViewById(R.id.imgCorrect_2_1);
		imgCorrect[1][1] = (ImageView) v.findViewById(R.id.imgCorrect_2_2);
		imgCorrect[1][2] = (ImageView) v.findViewById(R.id.imgCorrect_2_3);
		imgCorrect[2][0] = (ImageView) v.findViewById(R.id.imgCorrect_3_1);
		imgCorrect[2][1] = (ImageView) v.findViewById(R.id.imgCorrect_3_2);
		imgCorrect[2][2] = (ImageView) v.findViewById(R.id.imgCorrect_3_3);


		for(int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				textKoreans[i][j].setSingleLine(true);
				textEnglishs[i][j].setSingleLine(true);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textKoreans[i][j].getLayoutParams();
				params.setMargins(25, 5, 25, 0);
				textKoreans[i][j].setLayoutParams(params);

				params = (LinearLayout.LayoutParams) textEnglishs[i][j].getLayoutParams();
				params.setMargins(25, 0, 25, 0);
				textEnglishs[i][j].setLayoutParams(params);

				ESUtils.setTextViewTypeFaceByRes(m_context, textKoreans[i][j], Define.getMainFont());
				ESUtils.setTextViewTypeFaceByRes(m_context, textEnglishs[i][j], Define.getEnglishFont());

				textKoreans[i][j].setTextSize(20);
				textEnglishs[i][j].setTextSize(20);
			}
			ESUtils.setTextViewTypeFaceByRes(m_context, textLevels[i], Define.getMainFont());
		}

		ESUtils.setTextViewTypeFaceByRes(m_context, textResult, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, textResultSub, Define.getMainFont());

	}

	int nStudyLevel = -1;

	public void initData(int nStartLevel, int nEndLevel){

		textLevels[0].setText(String.format("%d%s", nStartLevel+1, m_context.getString(R.string.order)));
		textLevels[1].setText(String.format("%d%s", nStartLevel+2, m_context.getString(R.string.order)));

		if(nEndLevel - nStartLevel < 2) {
			frameLevels[2].setVisibility(View.INVISIBLE);
		}else{
			textLevels[2].setText(String.format("%d%s", nStartLevel+3, m_context.getString(R.string.order)));
		}


		for(int i = nStartLevel; i <= nEndLevel; i++){
			ArrayList<WordData> lvlData = getWordDataList(i+1);
			int nCompleteCount = 0;
			if(lvlData.size() == 3){
				for(int j = 0; j < 3; j++){
					WordData data = lvlData.get(j);
					textEnglishs[i - nStartLevel][j].setText(data.getMeanEn());
					textKoreans[i - nStartLevel][j].setText(data.getMeanKr());

					if(data.getComplete() == 1) {
						nCompleteCount++;
						imgCorrect[i - nStartLevel][j].setVisibility(View.VISIBLE);
					}else{
						imgCorrect[i - nStartLevel][j].setVisibility(View.INVISIBLE);
					}
				}
			}

			if(nCompleteCount <= 1 && nStudyLevel == -1) nStudyLevel = i;
		}

		if(nStudyLevel == -1) nStudyLevel = nEndLevel;

		imgMasks[nStudyLevel-nStartLevel].setBackgroundResource(R.drawable.st_leveltest_line_two);
		imgMasks[nStudyLevel-nStartLevel].setVisibility(View.VISIBLE);


		textResult.setText(String.format("%s %d%s", m_context.getString(R.string.string_leveltest_finalresult1_1),
				nStudyLevel + 1, m_context.getString(R.string.string_leveltest_finalresult1_2)));

		int nTempLevel = nStudyLevel;
		if(nTempLevel > 0) nTempLevel --;

		textResultSub.setText(String.format("%d%s", nTempLevel + 1, m_context.getString(R.string.string_leveltest_finalresult2)));


	}

	public ArrayList<WordData> getWordDataList(int nLevel){
		ArrayList<WordData> rDataList = new ArrayList<WordData>();
		for(int i = 0; i < m_arrAllWordData.size(); i++){
			WordData data = m_arrAllWordData.get(i);
			if(data.getLevel() == nLevel){
				rDataList.add(data);
			}
		}

		for(int i = 0; i< rDataList.size(); i++){
			for(int j = 0; j > rDataList.size(); j++){
				WordData dataI = rDataList.get(i);
				WordData dataJ = rDataList.get(j);

				if(dataI.getComplete() == 0 && dataJ.getComplete() == 1){
					WordData tmp = dataI;
					dataI.setComplete(dataJ.getComplete());
					dataI.setId(dataJ.getId());
					dataI.setMeanEn(dataJ.getMeanEn());
					dataI.setMeanKr(dataJ.getMeanKr());

					dataJ.setComplete(tmp.getComplete());
					dataJ.setId(tmp.getId());
					dataJ.setMeanEn(tmp.getMeanEn());
					dataJ.setMeanKr(tmp.getMeanKr());
				}
			}
		}
		return rDataList;
	}

	public void setUserDbMana(UserDBManager dbMana) {
		m_userDbMana = dbMana;
	}
	
	public void setListener(OnLevelTestResultFinishListner listener) {
		m_listener = listener;
	}

	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(m_context);

		if(v.getId() == R.id.btnNextStep){
			int nTempLevel = nStudyLevel;
			if(nTempLevel > 0) nTempLevel --;
			m_listener.onAfter(nTempLevel);
		}
	}

	public abstract interface OnLevelTestResultFinishListner {
		public abstract void onAfter(int nOrder);
	}
}
