package com.sforeignpad.sekword.ui.studding.study;


import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.PAKDBManager;
import com.sforeignpad.sekword.database.ScheduleDBManager;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.StudyDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.ui.studding.test.StudyTestActivity;
import com.sforeignpad.sekword.utils.ESUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class StudyActivity extends Activity implements View.OnTouchListener {

	private TextView m_tvOrder;
	private TextView m_tvTargetWordCount;
	private TextView m_tvCurWordCount;
	
	private StudyWordTextView m_tvStudyWord1;
	private StudyWordTextView m_tvStudyWord2;
	private ImageView m_ivStudyPic;
	
	private Button m_btnBack;
	private Button m_btnSave;
	private Button m_btnPrev;
	private Button m_btnPause;
	private Button m_btnNext;
	private Button m_btnRemeber;
	
	private int m_nOrder = 0;		//급수
	private int m_nTargetStudyWordCount = 0;	//목표단어개수
	private int m_nCurrentCase = 1;		//학습단계
	private int m_nCurrentWordCount = 1;	//현재 학습한 단어개수
	private int[] m_nTotalRepeat = new int[2];			//단계별 총학습회수
	private int m_nCurrentCaseStudy = 0;	//현재 단계별 학습회수
	private int m_nStudySpeed = 0;			//학습속도
	
	SoundPool m_soundPool = null;
	HashMap<String, Integer> m_soundMap;
	float m_fVolume = 0.0f;
	
	private int m_nEngWordBgColor = 0;
	private int m_nKorWordBgColor = 0;
	
	private Thread m_thread = null;
	private Handler m_handlerStudy = null;	
	private boolean m_bStudyStop = false;	//학습이 완전히 끝났는가 판단
	private boolean m_bPause = false;		//학습정지상태 판단
	private boolean m_bOneWordStudy = true; //한개 단어학습이 다 끝났는가를 판단
	private boolean m_bArrowBtnPressed = false;	//이전 단추, 다음단추가 눌리웠다면 true;
	private boolean m_bShowNoticeDlg = false;	//학습단계대화창 현시했댔는가 판단
	
	private long m_nStartStudyTime = 0;		//학습시작시간
	
	private int m_nUserId = -1;			//사용자아이디	
	private int m_nScheduleId = -1;		//일정아이디
	
	private int[] m_arrStudyTime = {2000, 3000, 3000};
	
	private StudyDBManager m_studyDbMana = null;
	private ScheduleDBManager m_scheduleDbMana = null;
	private WordpadDBManager m_wordpadDbMana = null;
	private SettingDBManager mSettingDbMana = null;
	private ScheduleDateData m_scheduleData = null;
	private PAKDBManager m_pakManager = null;

	private ArrayList<WordData> m_studyWordData = new ArrayList<WordData>();
	
	private boolean m_bTouchDown = false;	//터치다운상태인가를 판별
	private float m_nTouchDownPoint = 0;		//터치다운하였을때의 x점
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_study_word);
		
		Intent intent = this.getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);
		
		if(!isConnectDB()){
			finish();
			return;
		}	
		
		getStudySettingInfo();
		
		initViews();
		setStudyInitInfo();
		setStudyCurInfo();
		initSound();
		initStudy();
		initStudyHandler();
		
		//시작시간
		m_nStartStudyTime = ESUtils.getNowTimeSeconds();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				/*if(isCompleteAllRember()) {					
					goStudyTestActivity();
					StudyActivity.this.finish();
				}
				else	*///처음 학습시작시 1단계학습을 시작한다고 알려준다.
					startStudyPhase();
			}
		}, 500);
	}

	private void finishStudyThread() {
		if(!m_tvStudyWord1.getAnimState()) m_tvStudyWord1.finishThread();
		if(!m_tvStudyWord2.getAnimState()) m_tvStudyWord2.finishThread();
		
		m_bArrowBtnPressed = true;
		m_bPause = false;
		m_bStudyStop = true;
		
		if(m_thread.isAlive()) {
			try{
				m_thread.join();
			}
			catch(Exception e){}
		}
		
		m_handlerStudy = null;	
	}
	
	private void finishStudyActivity() {
		if(m_soundPool != null)
			m_soundPool.release();
		m_soundPool = null;
		
		if(m_soundMap != null)
			m_soundMap.clear();
		m_soundMap = null;
		
		finishStudyThread();
		
		saveStudyTimeToDB();
		closeDB();		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishStudyActivity();
		
		super.onDestroy();
		System.gc();
	}

	/******************DB관련***********************/
	
	//DB접속
	private boolean isConnectDB() {
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;
			
		m_scheduleDbMana = new ScheduleDBManager(this, m_nUserId);
		if(m_scheduleDbMana == null)
			return false;
		
		m_wordpadDbMana = new WordpadDBManager(this, m_nUserId);
		if(m_wordpadDbMana == null)
			return false;
		
		mSettingDbMana = new SettingDBManager(this);
		if(mSettingDbMana == null)
			return false;

		m_pakManager = new PAKDBManager(this);
		if(m_pakManager == null)
			return false;

		return true;
	}
		
	private void closeDB() {
		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
	}

	public String getImageFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 0);
	}

	public String getAudioFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 1);
	}


	private void saveStudyTimeToDB() {
		long nEndStudyTime = ESUtils.getNowTimeSeconds();
		long nDiff = nEndStudyTime - m_nStartStudyTime;
		
		m_scheduleData.setTimes(nDiff);
		m_studyDbMana.setTimes(m_scheduleData);
	}
	
	/**************************음성관련***************************/
	
	private void initSound() {
		m_soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);		
		
		AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
		m_fVolume = audioManager.getStreamVolume(3);///audioManager.getStreamMaxVolume(3);
		
		m_soundMap = new HashMap<String, Integer>();
		
		for(int i=0; i < m_studyWordData.size(); i++)
			loadSound(m_studyWordData.get(i).getSound());
	}
	
	private void loadSound(String strWord) {
		AssetFileDescriptor fd = null;
		try {
			//fd = this.getAssets().openFd("data/sound/1-" + strWord + Define.SOUND_TYPE);
			//String soundPath = Define.SOUND_PATH + strWord + Define.SOUND_TYPE;
			String soundPath = getAudioFileName(Integer.parseInt(strWord));
			int nSoundId = m_soundPool.load(soundPath , 1);
			m_soundMap.put(strWord, nSoundId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void playWordVoice(String strWord) {
		m_soundPool.play(m_soundMap.get(strWord).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}
	
	/*********************레이아웃 관련*************************/
	
	private void initViews() {
		FrameLayout layoutParent = (FrameLayout)findViewById(R.id.StudyLayout);
		layoutParent.setOnTouchListener(this);
		
		m_tvOrder = (TextView)findViewById(R.id.tvStudyOrder);
		m_tvTargetWordCount = (TextView)findViewById(R.id.tvTargetWordCount);
		m_tvCurWordCount = (TextView)findViewById(R.id.tvCurrentStudyWordCount);
		
		m_tvStudyWord1 = (StudyWordTextView)findViewById(R.id.tvStudyWord1);
		m_tvStudyWord1.setText("");
		m_tvStudyWord1.setDirect(true);
		m_tvStudyWord1.setAnimSpeed(m_nStudySpeed);
		
		m_tvStudyWord2 = (StudyWordTextView)findViewById(R.id.tvStudyWord2);
		m_tvStudyWord2.setText("");
		m_tvStudyWord2.setDirect(true);
		m_tvStudyWord2.setAnimSpeed(m_nStudySpeed);
		
		setWordTextColor();
		
		m_ivStudyPic = (ImageView)findViewById(R.id.ivStudyPic);
		
		//단추관련
		m_btnBack = (Button)findViewById(R.id.btnBack);
		m_btnBack.setOnClickListener(m_clickListener);
		
		m_btnSave = (Button)findViewById(R.id.btnSave);
		m_btnSave.setOnClickListener(m_clickListener);
		
		m_btnPrev = (Button)findViewById(R.id.btnPrevWord);
		m_btnPrev.setOnClickListener(m_clickListener);
		
		m_btnPause = (Button)findViewById(R.id.btnPause);
		m_btnPause.setOnClickListener(m_clickListener);
		
		m_btnNext = (Button)findViewById(R.id.btnNextWord);
		m_btnNext.setOnClickListener(m_clickListener);
		
		m_btnRemeber = (Button)findViewById(R.id.btnRemeber);
		m_btnRemeber.setOnClickListener(m_clickListener);
	}
	
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(v.getId()) {
			case R.id.btnBack:
				StudyActivity.this.finish();
				break;
			case R.id.btnSave:
				saveWord();
				break;
			case R.id.btnPrevWord:
				doArrowBtnPreessPerform(false);
				break;
			case R.id.btnNextWord:
				doArrowBtnPreessPerform(true);
				break;
			case R.id.btnPause:
				doPausePerformed();
				break;
			case R.id.btnRemeber:
				doRemeberWord();
			}
		}
	};
	
	private void setWordTextColor() {
		int rColor = ESUtils.getRedColor(m_nEngWordBgColor);
		int gColor = ESUtils.getGreenColor(m_nEngWordBgColor);
		int bColor = ESUtils.getBlueColor(m_nEngWordBgColor);
			
		if(m_nEngWordBgColor != 0)
			m_tvStudyWord1.setColorOfView(rColor, gColor, bColor);
		
		rColor = ESUtils.getRedColor(m_nKorWordBgColor);
		gColor = ESUtils.getGreenColor(m_nKorWordBgColor);
		bColor = ESUtils.getBlueColor(m_nKorWordBgColor);
			
		if(m_nKorWordBgColor != 0)
			m_tvStudyWord2.setColorOfView(rColor, gColor, bColor);
	}
	
	private void setStudyInitInfo() {
		m_scheduleData = m_scheduleDbMana.getScheduleDateData(m_nScheduleId); 
		m_nOrder = m_scheduleDbMana.getLevel(m_nScheduleId);
		
		m_scheduleDbMana.createScheduleWord(m_scheduleData.getId());
		m_studyWordData = m_studyDbMana.getWordList(m_scheduleData);
		
		m_nTargetStudyWordCount = m_studyWordData.size();
		
		m_tvOrder.setText(Integer.toString(m_nOrder) + getString(R.string.order));
		m_tvTargetWordCount.setText(Integer.toString(m_nTargetStudyWordCount) + getString(R.string.sum));	
	}
	
	private void getStudySettingInfo() {
		m_nTotalRepeat[0] = mSettingDbMana.getStudy1Value();
		m_nTotalRepeat[1] = mSettingDbMana.getStudy2Value();
		
		m_nEngWordBgColor = mSettingDbMana.getWordColorValue();
		m_nKorWordBgColor = mSettingDbMana.getMeanColorValue();
		
		m_nStudySpeed = mSettingDbMana.getStudySpeedIndex();
		
		switch(m_nStudySpeed) {
		case 0:
			m_arrStudyTime = new int[] {5500, 3000, 10000};
			break;
		case 1:
			m_arrStudyTime = new int[] {4500, 2000, 7000};
			break;
		case 2:
			m_arrStudyTime = new int[] {2500, 1500, 5000};
			break;
		case 3:
			m_arrStudyTime = new int[] {2000, 1000, 2000};
			break;
		case 4:
			m_arrStudyTime = new int[] {800, 800, 800};
			break;
		}
	}
	
	private void setStudyCurInfo() {
		
		
		String strInfo = Integer.toString(m_nCurrentCase) + getString(R.string.phase) + ":";
		strInfo += " " + Integer.toString(m_nCurrentWordCount) + "/" + Integer.toString(m_nTargetStudyWordCount);
		strInfo += "  " + "회수 : " + Integer.toString(m_nCurrentCaseStudy+1);	//Test용
		m_tvCurWordCount.setText(strInfo);
	}



	private void setStudyWordPic(String strWord) {
		try {
			//String imgPath = Define.IMAGE_PATH + strWord + ".png";
			String imgPath = getImageFileName(Integer.parseInt(strWord));
			FileInputStream fis = new FileInputStream(imgPath);
			Bitmap wordPic = BitmapFactory.decodeStream(fis);
			m_ivStudyPic.setImageBitmap(wordPic);
			fis.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initStudy() {
		m_tvStudyWord1.setText("");
		m_tvStudyWord2.setText("");
		
		m_tvStudyWord1.setVisibility(View.GONE);
		m_tvStudyWord1.finishThread();
		m_tvStudyWord1.init();
		
		m_tvStudyWord2.setVisibility(View.GONE);
		m_tvStudyWord2.finishThread();
		m_tvStudyWord2.init();
		
		m_ivStudyPic.setImageDrawable(null);
	}
	
	//단어장에 단어 보관
	private void saveWord() {
		m_wordpadDbMana.InsertWordpad(Define.KIND_IMPORT_WORD, m_studyWordData.get(m_nCurrentWordCount-1).getId());
	}
	
	//Prev, Next단추를 눌렀을때의 처리
	private void doArrowBtnPreessPerform(boolean bNext) {
		m_bArrowBtnPressed = true;	//방향단추가 눌리웠음		
		m_bOneWordStudy = true;		//한개 단어 학습 끝남		
				
		m_soundPool.pause(m_soundMap.get(m_studyWordData.get(m_nCurrentWordCount-1).getSound()).intValue());	//음성이 플레이된다면 정지
		m_soundPool.stop(m_soundMap.get(m_studyWordData.get(m_nCurrentWordCount-1).getSound()).intValue());	//음성이 플레이된다면 정지
		
		finishStudyThread();
		
		if(bNext) {		
			m_nCurrentWordCount ++;		//다음단어
			
			if(m_nCurrentWordCount > m_studyWordData.size()) {
				m_nCurrentWordCount = 1;	//처음 단어부터 학습				
				m_nCurrentCaseStudy++;		//학습회수 증가
				
				if(m_nCurrentCaseStudy > m_nTotalRepeat[m_nCurrentCase-1]-1){		//학습회수가 다 되였다면
					m_nCurrentCase ++;		//다음 단계 학습
					m_nCurrentCaseStudy = 0;	//학습회수 0으로 설정
					
					if(m_nCurrentCase > 2) {		//학습단계가 2보다 크다면 학습완료
						m_bStudyStop = true;		//학습끝내기
						goStudyTestActivity();
						StudyActivity.this.finish();
					}					
				}
			}
		}
		else {
			m_nCurrentWordCount --;		//이전단어
			m_nCurrentWordCount = (m_nCurrentWordCount <= 1) ? 1 : m_nCurrentWordCount;
		}
		
		m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
		
		initStudy();			
		initStudyHandler();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				startStudyPhase();
			}
		}, 300);
	}
	
	//정지단추 누를때의 처리
	private void doPausePerformed() {
		m_bPause = !m_bPause;
		if(m_bPause) {
			m_btnPause.setBackgroundResource(R.drawable.btn_study_play_right);		
			m_soundPool.pause(m_soundMap.get(m_studyWordData.get(m_nCurrentWordCount-1).getSound()).intValue());
			
			if(!m_tvStudyWord1.getAnimState())	m_tvStudyWord1.setPauseAnim();
			if(!m_tvStudyWord2.getAnimState())	m_tvStudyWord2.setPauseAnim();
		}
		else {
			m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
			m_soundPool.resume(m_soundMap.get(m_studyWordData.get(m_nCurrentWordCount-1).getSound()).intValue());
			
			if(!m_tvStudyWord1.getAnimState())	m_tvStudyWord1.setResumeAnim();
			if(!m_tvStudyWord2.getAnimState())	m_tvStudyWord2.setResumeAnim();
		}
	}
	
	//암기완료 단어 설정
	private void doRemeberWord() {
		if(!m_btnRemeber.isSelected()) {
			m_btnRemeber.setSelected(true);
			m_studyWordData.get(m_nCurrentWordCount-1).setComplete(1);
			m_studyDbMana.setWordComplete(m_scheduleData.getId(), m_studyWordData.get(m_nCurrentWordCount-1).getId());
		}
		else {
			m_btnRemeber.setSelected(false);
			m_studyWordData.get(m_nCurrentWordCount-1).setComplete(0);
			m_studyDbMana.setWordComplete(m_scheduleData.getId(), m_studyWordData.get(m_nCurrentWordCount-1).getId());
		}
	}
	
	//학습할 단어, 그림 표시하는 핸들러
	@SuppressLint("HandlerLeak")
	private void initStudyHandler() {
		m_handlerStudy = new Handler() {
			public void handleMessage(Message msg) {
				switch(msg.what) {
				case 1:	//왼쪽단어 현시
					
					setStudyCurInfo();
					if(m_studyWordData.get(m_nCurrentWordCount-1).getComplete() == 1)
						m_btnRemeber.setSelected(true);						
					else
						m_btnRemeber.setSelected(false);
					
					if(m_nCurrentCase == 1) {
						m_tvStudyWord1.setText(m_studyWordData.get(m_nCurrentWordCount-1).getMeanEn());
						m_tvStudyWord1.setVisibility(View.VISIBLE);
						m_tvStudyWord1.startAnimBg();
					}
					else {
						m_tvStudyWord2.setText(m_studyWordData.get(m_nCurrentWordCount-1).getMeanKr());
						m_tvStudyWord2.setVisibility(View.VISIBLE);
						m_tvStudyWord2.startAnimBg();
					}
					
					playWordVoice(m_studyWordData.get(m_nCurrentWordCount-1).getSound());
					
					break;
				case 2:	//그림 및 음성
					setStudyWordPic(m_studyWordData.get(m_nCurrentWordCount-1).getImage());
					
					break;
				case 3:	//오른쪽단어
					if(m_nCurrentCase == 1) {
						m_tvStudyWord2.setText(m_studyWordData.get(m_nCurrentWordCount-1).getMeanKr());
						m_tvStudyWord2.setVisibility(View.VISIBLE);
						m_tvStudyWord2.startAnimBg();
					}
					else {
						m_tvStudyWord1.setText(m_studyWordData.get(m_nCurrentWordCount-1).getMeanEn());
						m_tvStudyWord1.setVisibility(View.VISIBLE);
						m_tvStudyWord1.startAnimBg();
					}
					
					break;
				case 4:	//다음단어 현시준비
					m_bOneWordStudy = true;					
					initStudy();					
					break;
				case 5:	//다음 단계학습 알림 대화창 
					setWordTextColor();	//단어배경색 설정
			
					if(!m_bShowNoticeDlg) {
						m_bShowNoticeDlg = true;
						showStartStudyDialog();
					}
					
					break;
				case 6:	//학습완료
					goStudyTestActivity();
					StudyActivity.this.finish();
					break;
				}
			}
		};
	}
	
	//학습진행
	private void startStudyPhase() {
		m_thread = new Thread() {
			public void run() {
				while(!m_bStudyStop) {
					if(!m_bOneWordStudy)	//한개 단어를 완전히 학습하면 다음 단어로 이행
						continue;
					
					if(m_nCurrentWordCount-1 > m_studyWordData.size()-1) { //한단계 학습을 한회 진행하였다면
						m_nCurrentCaseStudy ++;			//학습회수 증가
						m_nCurrentWordCount = 1;		//첫 단어부터 다시 학습
						
						if(m_nCurrentCaseStudy > m_nTotalRepeat[m_nCurrentCase-1]-1) {	//총학습회수만큼 학습하였다면
							m_nCurrentCase ++;	//2단계 학습 진행
							m_nCurrentCaseStudy = 0;	//학습회수 0으로 설정
							m_bShowNoticeDlg = false;
							
							if(m_nCurrentCase > 2) {		//학습단계가 2보다 크다면
							//	if(isCompleteAllRember()) {	//모든 단어를 암기완료하였다면 학습완료
									m_bStudyStop = true;
									m_handlerStudy.sendEmptyMessage(6);	
									break;
							//	}
							//	else	//1단계부터 학습시작
							//		m_nCurrentCase = 1;
							}
						}
					}
					
					//학습단계 시작이라면 대화창 현시
					if((m_nCurrentCase == 1 || m_nCurrentCase == 2) &&
							m_nCurrentWordCount == 1 && m_nCurrentCaseStudy == 0) {	
						
						/*if(isCompleteAllRember()) {	//모든 단어를 암기완료하였다면 학습완료
							m_bStudyStop = true;
							m_handlerStudy.sendEmptyMessage(6);	
							break;
						}*/
						if(!m_bShowNoticeDlg) m_bPause = true;
						m_handlerStudy.sendEmptyMessage(5);
					}
					
					m_bOneWordStudy = false;
					
					/*if(m_studyWordData.get(m_nCurrentWordCount-1).getComplete() == 1) {	//암기완료된 단어는 학습하지 않는다.  
						m_nCurrentWordCount ++;
						continue;
					}*/
						
					//단어표시
					if(sleepThread(0)) continue;
					if(m_handlerStudy != null) m_handlerStudy.sendEmptyMessage(1);
					
					//그림 및 음성
					if(sleepThread(m_arrStudyTime[0])) continue;
					if(m_handlerStudy != null)	m_handlerStudy.sendEmptyMessage(2);
					
					//단어표시
					if(sleepThread(m_arrStudyTime[1])) continue;
					if(m_handlerStudy != null)	m_handlerStudy.sendEmptyMessage(3);
					
					//다음 단어 현시준비
					if(sleepThread(m_arrStudyTime[2])) continue;
					if(m_handlerStudy != null) m_handlerStudy.sendEmptyMessage(4);
					
					m_nCurrentWordCount ++;
					
					try {
						Thread.sleep(100);
					}
					catch(Exception e)
					{}					
				}
			}
		};
		
		m_thread.setName("StudyDaemon");
		m_thread.setDaemon(true);
		m_bStudyStop = false;
		m_bArrowBtnPressed = false;
		m_bPause = false;
		
		m_thread.start();		
	}
	
	//학습진행상태가 정지상태인가 검사
	private void pauseStudding() {
		while(m_bPause) {
			try{
				Thread.sleep(200);
			}
			catch(Exception e) 
			{}
		}
	}
	
	private boolean sleepThread(int nTime) {
		int nCount = 0;
		boolean bRes = false;
		
		while(true) {
			pauseStudding();
			
			if(m_bArrowBtnPressed) {
				bRes = true;
				break;
			}
			
			if(nCount * 200 >= nTime)
				break;
			
			try {
				Thread.sleep(200);
			}catch(Exception e) {}
			
			nCount ++;
		}
		
		return bRes;
	}
	
	//모든 단어를 다 암기완료했는가 검사
	private boolean isCompleteAllRember() {
		boolean bComplete = true;
		
		for(int i=0; i<m_studyWordData.size(); i++) {
			if(m_studyWordData.get(i).getComplete() == 0) {
				bComplete = false;
				break;
			}
		}
		
		return bComplete;
	}
	
	//학습단계 알림 대화창
	private void showStartStudyDialog () {
		String strMsg = this.getString(R.string.start_study_phase1);
		if(m_nCurrentCase == 2)
			strMsg = this.getString(R.string.start_study_phase2);
		
		new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(strMsg)
		.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(StudyActivity.this);

				m_bPause = false;
				dialog.dismiss();
			}
		})
		.show();		
	}
	
	private void goStudyTestActivity() {
		Intent intent = new Intent(this, StudyTestActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.SCHEDULE_ID, m_scheduleData.getId());
		startActivity(intent);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			m_bTouchDown = true;
			m_nTouchDownPoint = event.getX();
			return true;
		case MotionEvent.ACTION_UP:
			if(m_bTouchDown) {
				float endPoint = event.getX();
				if(m_nTouchDownPoint-endPoint > 200.0f) 
					doArrowBtnPreessPerform(true);
				
				m_bTouchDown = false;
			}
			break;
		}
		return false;
	}
}
