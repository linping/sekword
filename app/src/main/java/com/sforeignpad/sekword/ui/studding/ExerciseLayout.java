package com.sforeignpad.sekword.ui.studding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.ui.studding.test.TestLayout;
import com.sforeignpad.sekword.ui.studding.test.TestProgressBar;
import com.sforeignpad.sekword.utils.ESUtils;

import java.util.ArrayList;

public class ExerciseLayout extends TestLayout {

	private Context m_context;
	boolean is_enable_thread = false;

	private TextView m_tvEngProblem;	//조선어문제

	private ImageView[] m_arrIvTrueFalseMsg = new ImageView[4];		//합격 불합격
	private Button[] m_arrBtnResult = new Button[4];				//결과단어
	private TestProgressBar m_progressbar = null;					//ProgressBar
	private TestProgressBar m_progressbarBack = null;

	public OnEngKorTestFinishListener m_listener = null;

	private String[] m_strEngRes = new String[4];
	private int m_nTrueIndex = -1;

	StudyExerciseActivity m_Parent;

	public ExerciseLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		initLayout();
		initKorEngTestLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.exercise_layout, null);
		this.addView(v);
	}

	private void initKorEngTestLayout() {

		m_tvEngProblem = (TextView)findViewById(R.id.tvKorProblem);

		ImageView ivTrueFalseMsg1 = (ImageView)findViewById(R.id.ivTrueFalseMsg1);
		ivTrueFalseMsg1.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg2 = (ImageView)findViewById(R.id.ivTrueFalseMsg2);
		ivTrueFalseMsg2.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg3 = (ImageView)findViewById(R.id.ivTrueFalseMsg3);
		ivTrueFalseMsg3.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg4 = (ImageView)findViewById(R.id.ivTrueFalseMsg4);
		ivTrueFalseMsg4.setVisibility(View.GONE);

		m_progressbar = (TestProgressBar)findViewById(R.id.progressbar);
		m_progressbar.setMax(1000);
		m_progressbar.initProgressBarManullay(true);

		m_progressbarBack = (TestProgressBar) findViewById(R.id.progressbar_back);
		m_progressbarBack.setMax(1000);
		m_progressbarBack.initProgressBarManullay(false);
		m_progressbarBack.setProgress(1000);

		m_arrIvTrueFalseMsg[0] = ivTrueFalseMsg1;
		m_arrIvTrueFalseMsg[1] = ivTrueFalseMsg2;
		m_arrIvTrueFalseMsg[2] = ivTrueFalseMsg3;
		m_arrIvTrueFalseMsg[3] = ivTrueFalseMsg4;


		Button btnResultEngKor1 = (Button)findViewById(R.id.btnTestKorWord1);
		btnResultEngKor1.setOnClickListener(m_clickListener);

		Button btnResultEngKor2 = (Button)findViewById(R.id.btnTestKorWord2);
		btnResultEngKor2.setOnClickListener(m_clickListener);

		Button btnResultEngKor3 = (Button)findViewById(R.id.btnTestKorWord3);
		btnResultEngKor3.setOnClickListener(m_clickListener);

		Button btnResultEngKor4 = (Button)findViewById(R.id.btnTestKorWord4);
		btnResultEngKor4.setOnClickListener(m_clickListener);

		m_arrBtnResult[0] = btnResultEngKor1;
		m_arrBtnResult[1] = btnResultEngKor2;
		m_arrBtnResult[2] = btnResultEngKor3;
		m_arrBtnResult[3] = btnResultEngKor4;

		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvEngProblem, Define.getEnglishFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, (TextView) findViewById(R.id.tvengkortest_description), Define.getMainFont());

		for(int i = 0; i < m_arrBtnResult.length; i++){
			ESUtils.setButtonTypeFaceByRes(m_context, m_arrBtnResult[i], Define.getMainFont());
		}
	}

	public void setParent(StudyExerciseActivity pParent){
		m_Parent = pParent;
	}

	private OnClickListener m_clickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//ESUtils.playBtnSound(m_context);

			// TODO Auto-generated method stub
			if(m_bResultPressed || !m_bProblemView) return;
			
			switch(v.getId()) {
			case R.id.btnTestKorWord1:
				checkResult(1);
				break;
			case R.id.btnTestKorWord2:
				checkResult(2);
				break;
			case R.id.btnTestKorWord3:
				checkResult(3);
				break;
			case R.id.btnTestKorWord4:
				checkResult(4);
				break;
			}
		}
	};
	
	@SuppressLint("HandlerLeak")
	private void checkResult(int nIndex) {
		m_bResultPressed = true;
		
		m_arrIvTrueFalseMsg[nIndex-1].setVisibility(View.VISIBLE);
		
		if(isTrueWord(nIndex-1)) {	//합격
			is_successProblem[m_nCurrentTestProblemCount-1] = true;		

			m_nTrueResCount ++;
			m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.test_word_true);
			m_arrIvTrueFalseMsg[nIndex-1].setSelected(true);
			StudyExerciseActivity.playWordVoice("success");
		}
		else {	//불합격
			is_successProblem[m_nCurrentTestProblemCount-1] = false;

			m_nFalseResCount ++;
			m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.test_word_false);
			m_arrIvTrueFalseMsg[nIndex-1].setSelected(false);
			StudyExerciseActivity.playWordVoice("fail");
			animSelectTrueWord();
			
			//틀린 단어 단어장 추가
			if(m_nTestCount != 0)
				insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());
		}
		
		setTestResult();		
		
		new Handler() {
			public void handleMessage(Message msg) {
				m_nTestTime = 0;
				m_bResultPressed = false;
				m_bProblemView = false;
			}
		}.sendEmptyMessageDelayed(0, 1200);
	}
	
	//옳은 단어 지정 애니메이션
	private void animSelectTrueWord() {
		Thread thread = new Thread() {
			private boolean m_bVisible = false;
			private int m_nCount = 0;
			private boolean m_bStop = false;
			
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_nTrueIndex == -1) return;
					
					m_bVisible = !m_bVisible;
					if(m_bVisible) {						
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.VISIBLE);
						m_nCount ++;
					}
					else
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.INVISIBLE);
					
					if(m_nCount > 4) {
						m_bStop = true;
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.VISIBLE);
					}
				}
			};
			
			public void run() {
				while(!m_bStop && !m_bTestStop) {
					try{
						Thread.sleep(150);
					}
					catch(Exception e) {}
					
					m_nCount ++;
					m_handler.sendEmptyMessage(0);
				}
			}
		};
		
		if(m_nTrueIndex != -1) m_arrIvTrueFalseMsg[m_nTrueIndex].setImageResource(R.drawable.true_msg2);
		thread.start();
	}
		
	//지정한 단어가 맞는가 검사
	private boolean isTrueWord(int nResIndex) {
		boolean bTrue = false;
			
		if(nResIndex == m_nTrueIndex)
			bTrue = true;
			
		return bTrue;
	}
		
	//시험결과 현시
	private void setTestResult() {
		m_nTestCurrentProblemIndex++;
	}
	
	//시험전 초기화
	private void setTestInit() {
		for(int i=0; i<4; i++) {
			m_arrIvTrueFalseMsg[i].setVisibility(View.GONE);
			m_arrIvTrueFalseMsg[i].setImageResource(R.drawable.word_true_false);
			m_arrBtnResult[i].setBackgroundResource(R.drawable.test_word_false);
		}
	}
	
	protected void makeResWords() {
		////옳은 답의 단추순서
		ArrayList<Integer> arrResIndex = new ArrayList<Integer>();
		arrResIndex.add(-1);
		
		int nRes = ESUtils.getRandomValue(4, arrResIndex);	
		m_strEngRes[nRes] = m_arrWordData.get(m_nTestCount).getMeanKr();
		arrResIndex.add(nRes);
		
		m_nTrueIndex = nRes;
		
		ArrayList<Integer> arrIndex = new ArrayList<Integer>();
		arrIndex.add(m_nTestCount);
		
		for(int i=0; i<3; i++) {
			int nWrong = ESUtils.getRandomValue(m_arrWordData.size(), arrIndex);	//틀린 답의 인덱스
			nRes = ESUtils.getRandomValue(4, arrResIndex);	//틀린 답의 단추순서
			
			m_strEngRes[nRes] = m_arrWordData.get(nWrong).getMeanKr();
			
			arrResIndex.add(nRes);
			arrIndex.add(nWrong);
		}


		m_tvEngProblem.setText(m_arrWordData.get(m_nTestCount).getMeanEn());

		for(int i=0; i<4; i++){
			String text_ = getadjustedText(m_strEngRes[i]);
			m_arrBtnResult[i].setText(text_);
		}
	}

	
	public String getadjustedText(String textstring){
		String temp_string1 = "";
		String temp_string2 = "";
		if (textstring.length()>20) {
			temp_string2 = textstring.substring(0, 20);
			textstring    =textstring.replace(temp_string2, temp_string2+"\n");			
			temp_string1 = textstring.substring(0, 10);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n");			
			textstring = temp_string2;
		}else if (textstring.length()>10) {
			temp_string1 = textstring.substring(0, 10);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n");
			textstring = temp_string2;
		}			
		return textstring;				
	}
	//조선어-영어 시험
	@SuppressLint({ "HandlerLeak", "NewApi" })
	public void performEngKorTest() {
		m_threadTest = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_arrWordData == null) return;
					
					switch(msg.what) {
					case 0:
						if(m_nTestCount >m_tvTotalProblems -1) {	//모든 시험을 다 쳤다면
							m_bTestStop = true;		
							m_bResultPressed = true;
							
							new Handler() {
								public void handleMessage(Message msg) {
									m_bResultPressed = false;
									m_listener.onAfter(m_nCurrentTestProblemCount, m_nTrueResCount, m_nFalseResCount,m_nTestCurrentProblemIndex,is_successProblem, m_arrWordData);
								}
							}.sendEmptyMessageDelayed(0, 500);
							break;
						}
						else {
							is_enable_thread = false;

							//문제 제시
							makeResWords();
							
							m_nTestCount ++;
							m_nCurrentTestProblemCount++;
							
							setTestInit();
							
							m_bProblemView = true;
						}
						
						setTestResult();
						break;
					case 1:
						
						if (!is_enable_thread) {
							is_enable_thread = true;
							
							
							is_successProblem[m_nCurrentTestProblemCount-1] = false;
							m_nFalseResCount ++;
							
							//틀린 단어 단어장 추가
							StudyExerciseActivity.playWordVoice("fail");
							
							if(m_nTestCount != 0) insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());
							animSelectTrueWord();
							
							m_bResultPressed = true;
							
							new Handler() {
								public void handleMessage(Message msg) {
									m_nTestTime = 0;
									m_bResultPressed = false;
									m_bProblemView = false;
								}
							}.sendEmptyMessageDelayed(0, 1200);
						}
						break;
					}
					
				}
			};
			
			public void run() {
				while(!m_bTestStop) {
					pauseThread();
					sleepResultPressed();
					
					if(m_nTestTime == 0) m_handler.sendEmptyMessage(0);	//시험문제 제시
					
					m_nTestTime += 20;
									
					if(m_nTestTime >= m_nTestLimitTime*1000) {
						m_nTestTime = m_nTestLimitTime*1000;
						
						m_handler.sendEmptyMessage(1);
					}
					
		
					m_progressbar.setProgress(100 * m_nTestTime / (m_nTestLimitTime *1000));

					try {
						Thread.sleep(20);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		getTestProblems(Define.KIND_TEST_ENGKOR);
		m_threadTest.setName("KorEngTestThread");
		m_threadTest.setDaemon(true);
		m_threadTest.start();
	}
	
	//답을 선택한후 1초있다가 다음 문제를 제시
	private void sleepResultPressed() {
		while(m_bResultPressed) {
			try {
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void finishTest() {
		if(m_threadTest != null && m_threadTest.isAlive()) {
			m_bTestStop = true;
			try {
				m_threadTest.join();
			}
			catch(Exception e) {} 
		}
		m_arrBtnResult = null;
		m_arrIvTrueFalseMsg = null;
		m_strEngRes = null;
		super.finishTest();
	}
	
	public void setListener(OnEngKorTestFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnEngKorTestFinishListener {
		public abstract void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount, int m_nTestCurrentProblemIndex_, Boolean[] is_successProblem_, ArrayList<WordData> m_arrAllWordData_);

	}
}
