package com.sforeignpad.sekword.ui.studding;

import java.util.ArrayList;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.ScheduleDBManager;
import com.sforeignpad.sekword.database.StudyDBManager;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.effect.ScaleAnimEffect;
import com.sforeignpad.sekword.structs.ScheduleData;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.ui.studding.test.StudyLevelTestActivity;
import com.sforeignpad.sekword.ui.studyresult.EsStudyResultActivity;
import com.sforeignpad.sekword.utils.ESUtils;
import com.sforeignpad.sekword.ui.studding.SekWordPopup.OnSEKLaunchListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class EsStuddingActivity extends Activity implements View.OnClickListener {

	private final int TAB_SELECT_ORDER = 3;
	private final int TAB_VIEW_SCHEDULE = 2;
	private final int TAB_RESULT_VIEW = 1;
	private final int TAB_BACK = 0;
	
	private int m_nTabIndex = TAB_SELECT_ORDER;
	
	private RelativeLayout m_layoutTab;
	private FrameLayout[] m_layoutTabMenus = new FrameLayout[4];
	
	private Button[] m_btnSelectTabs = new Button[4];
	private Button[] m_btnDefaultTabs = new Button[4];
	
	private FrameLayout m_layoutSelectOrder;
	private FrameLayout m_layoutSchedule;
	
	private ScaleAnimEffect m_animEffect = new ScaleAnimEffect();
	
	private ScheduleMakeDialog m_dlgScheduleMake;
	
	private Button m_btnPrevDays;
	private Button m_btnNextDays;
	private int m_nArrowCount = 0;
	
	private ImageView m_ivStartOrder;
	
	private TextView m_tvPerioDate;		
	private TextView m_tvStudyStartDate;
	private TextView m_tvStudyEndDate;
	private	TextView m_tvStudyWordCount;
	private CustomProgressBar m_progressbar;
	
	private OneDayScheduleView[] m_schedules = new OneDayScheduleView[8];
	
	private int m_nOrder = 0;
	private int m_nTempOrder = 0;
	private int m_nUserId = -1;	
	
	private UserDBManager m_userDbMana = null;
	private StudyDBManager m_studyDbMana = null;
	private ScheduleDBManager m_scheduleDbMana = null;
	private ArrayList<ScheduleDateData> m_arrSchdeuleDateData = new ArrayList<ScheduleDateData>();
	private ScheduleData m_scheduleData = null;
	
	private int m_nTotalWordsOfOrder = 0;
	
	private ScheduleViewReceiver m_receiver = null;
	
	private StudyLauncherPopup launcherPopup = null;
	
	//private ProgressBar m_progressLoading;
	private FrameLayout m_frameLoading;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_studding);
		
		Intent intent = this.getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		
		//m_progressLoading = (ProgressBar) findViewById(R.id.progressLoading);
		//m_progressLoading.setVisibility(View.VISIBLE);
		m_frameLoading = (FrameLayout) findViewById(R.id.frameLoading);
		m_frameLoading.setVisibility(View.VISIBLE);
		new BackgroundTask().execute();

	}
	
	private class BackgroundTask extends AsyncTask<Void, Void, Void>
	{

		private boolean isConnected = true;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!isConnectDB()){
				isConnected = false;
				finish();
			}


			return null;
		}
		
	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);

			if(isConnected == false)
				return;

			new Handler(getMainLooper()).post(new Runnable() {
				@Override
				public void run() {


					int value = m_userDbMana.getIsUseStudyHelp(m_nUserId);

					initTextViews();
					initTabBar();
					initTabLayouts();
					initOrderButtons();
					initTestButton();
					initScheduleMakeDlg();
					initScheduleLayout();

					if(!setViewSchedule()) {
						m_nTabIndex = TAB_SELECT_ORDER;
						setTabLayout(m_nTabIndex);
					}

					registerReceiver();
				}
			});

	    }

		
	}
	private void finishActivity() {
		for(int i=0; i<4; i++) {
			m_layoutTabMenus[i] = null;
			m_btnSelectTabs[i] = null;
			m_btnDefaultTabs[i] = null;
		}
		
		for(int i=0; i<8; i++) 
			m_schedules[i] = null;
		
		m_layoutTabMenus = null;
		m_btnSelectTabs = null;
		m_btnDefaultTabs = null;
		m_schedules = null;
		
		unregisterReceiver();
		
		closeDB();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishActivity();		
		super.onDestroy();
		System.gc();
	}

	private void initTextViews(){
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textLevelTest1), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textLevelTest2), Define.getMainFont());

		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSelect1), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSelect2), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSelect3), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textSelect4), Define.getMainFont());

		/*TextView tv =  (TextView) findViewById(R.id.textSelect3);
		tv.setTextScaleX(0.9f);
		tv.settextscal*/

		//((TextView) findViewById(R.id.textLevelTest1)).setLetterSpacing(-0.1f);

	}

	/**************************Tab ê´€ë ¨******************************/
	
	private void initTabBar() {
		m_layoutTab = (RelativeLayout)findViewById(R.id.TabLayout);
		
		m_layoutTabMenus[3] = (FrameLayout)findViewById(R.id.TabSelectOrderLayout);
		m_layoutTabMenus[2] = (FrameLayout)findViewById(R.id.TabScheduleLayout);
		m_layoutTabMenus[1] = (FrameLayout)findViewById(R.id.TabResultLayout);
		m_layoutTabMenus[0] = (FrameLayout)findViewById(R.id.TabBackLayout);
		
		m_btnSelectTabs[3] = (Button)findViewById(R.id.ivTabSelectOrderOff);
		m_btnSelectTabs[2] = (Button)findViewById(R.id.ivTabScheduleOff);
		m_btnSelectTabs[1] = (Button)findViewById(R.id.ivTabResultOff);
		m_btnSelectTabs[0] = (Button)findViewById(R.id.ivTabBackOff);
		
		m_btnDefaultTabs[3] = (Button)findViewById(R.id.ivTabSelectOrderOn);
		m_btnDefaultTabs[2] = (Button)findViewById(R.id.ivTabScheduleOn);
		m_btnDefaultTabs[1] = (Button)findViewById(R.id.ivTabResultOn);
		m_btnDefaultTabs[0] = (Button)findViewById(R.id.ivTabBackOn);
		
		for(int i=0; i<4; i++) {
			m_btnSelectTabs[i].setVisibility(View.GONE);			
			m_btnDefaultTabs[i].setOnClickListener(this);
		}
		
		reAddAllTab();
		
		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				showOnFocusAnimation(m_nTabIndex, 10);
			}
		};
		handler.sendEmptyMessageDelayed(0, 10);
	}

	private void initTabLayouts() {
		m_layoutSelectOrder = (FrameLayout)findViewById(R.id.select_order_layout);
		m_layoutSchedule = (FrameLayout)findViewById(R.id.SchduleViewLayout);
		
		//setTabLayout(m_nTabIndex);
	}
	
	private void reAddAllTab() {		
		RelativeLayout.LayoutParams[] layoutParams = new RelativeLayout.LayoutParams[4];
		for(int i=0; i<4; i++)
			layoutParams[i] = (RelativeLayout.LayoutParams)m_layoutTabMenus[i].getLayoutParams();
		
		m_layoutTab.removeAllViews();
		
		for(int i=0; i<4; i++)
			m_layoutTabMenus[i].setLayoutParams(layoutParams[i]);
		
		m_layoutTab.addView(m_layoutTabMenus[0]);
		m_layoutTab.addView(m_layoutTabMenus[1]);
		m_layoutTab.addView(m_layoutTabMenus[2]);
		m_layoutTab.addView(m_layoutTabMenus[3]);
	}
	
	private void showLooseFocusAinimation(final int index) {
		reAddAllTab();
		
	    this.m_animEffect.setAttributs(1.15F, 1.0F, 1.15F, 1.0F, 200L);
	    
	    Animation anim = this.m_animEffect.createAnimation();
	    anim.setAnimationListener(new Animation.AnimationListener() {
	    	public void onAnimationEnd(Animation paramAnimation) {
	    		m_btnSelectTabs[index].setVisibility(View.INVISIBLE);
	    		m_btnDefaultTabs[index].setVisibility(View.VISIBLE);
				m_btnSelectTabs[index].clearAnimation();
	    	}

	    	public void onAnimationRepeat(Animation paramAnimation) { }
	    	public void onAnimationStart(Animation paramAnimation) { }
	    });

	    m_btnSelectTabs[index].startAnimation(anim);
	}
	
	private void showOnFocusAnimation(final int index, int duration) {
	    this.m_layoutTabMenus[index].bringToFront();
	    this.m_animEffect.setAttributs(1.0F, 1.15F, 1.0F, 1.15F, duration);
	    
	    Animation anim = this.m_animEffect.createAnimation();
	    anim.setAnimationListener(new Animation.AnimationListener() {
	    	public void onAnimationEnd(Animation paramAnimation) {}

	    	public void onAnimationRepeat(Animation paramAnimation) { }
	    	public void onAnimationStart(Animation paramAnimation) { }
	    });
	    
	    m_btnDefaultTabs[index].setVisibility(View.GONE);
	    m_btnSelectTabs[index].setVisibility(View.VISIBLE);
	    m_btnSelectTabs[index].startAnimation(anim);
	}
	
	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(this);
		boolean is_enabled_animation = true;
		
		switch(v.getId()) {
		case R.id.ivTabSelectOrderOn:
			break;
		case R.id.ivTabScheduleOn:
			break;
		case R.id.ivTabResultOn:
			is_enabled_animation = false;
			goResultActivity();
			break;
		case R.id.ivTabBackOn:
			is_enabled_animation = false;
			finish();
			break;
		}	
		
		if (is_enabled_animation) {
			// TODO Auto-generated method stub
			showLooseFocusAinimation(m_nTabIndex);
			
			switch(v.getId()) {
			case R.id.ivTabSelectOrderOn:
				m_nTabIndex = TAB_SELECT_ORDER;
				break;
			case R.id.ivTabScheduleOn:
				m_nTabIndex = TAB_VIEW_SCHEDULE;
				break;
			case R.id.ivTabResultOn:
				m_nTabIndex = TAB_RESULT_VIEW;
				goResultActivity();
				break;
			case R.id.ivTabBackOn:
				m_nTabIndex = TAB_BACK;
				this.finish();
				break;
			}		
			
			showOnFocusAnimation(m_nTabIndex, 150);
			setTabLayout(m_nTabIndex);
		}
	}
	
	public void setTabLayout(int nIndex) {
		if(nIndex == TAB_SELECT_ORDER) {
			m_layoutSelectOrder.setVisibility(View.VISIBLE);
			m_layoutSchedule.setVisibility(View.GONE);
            m_frameLoading.setVisibility(View.INVISIBLE);
		}
		else if(nIndex == TAB_VIEW_SCHEDULE){
			if(m_nOrder == 0) {		
				m_scheduleData = m_scheduleDbMana.getScheduleData();
				if(m_scheduleData == null) {
					showDlgNoSchedule();
					return;
				}
				
				m_nOrder = m_scheduleData.getLevel();
				m_arrSchdeuleDateData = m_scheduleDbMana.getScheduleListFromLevel(m_nOrder);
				m_nTotalWordsOfOrder = m_studyDbMana.getWordCount(m_nOrder);				
				
				goMakeScheduleScreen(m_scheduleData.getDays());
			}
			
			m_layoutSelectOrder.setVisibility(View.GONE);
			m_layoutSchedule.setVisibility(View.VISIBLE);
		}
	}
	
	private boolean setViewSchedule() {
		boolean bHaveSchedule = false;
		m_scheduleData = m_scheduleDbMana.getScheduleData();
		if(m_scheduleData != null) {	
			m_nTabIndex = TAB_VIEW_SCHEDULE;
			m_nOrder = m_scheduleData.getLevel();
			m_arrSchdeuleDateData = m_scheduleDbMana.getScheduleListFromLevel(m_nOrder);
			m_nTotalWordsOfOrder = m_studyDbMana.getWordCount(m_nOrder);
			
			goMakeScheduleScreen(m_scheduleData.getDays());
			
			m_layoutSelectOrder.setVisibility(View.GONE);
			m_layoutSchedule.setVisibility(View.VISIBLE);
			
			bHaveSchedule = true;
		}
		
		return bHaveSchedule;
	}
	
	private void showDlgNoSchedule() {
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(this.getString(R.string.noscheduledata))
		.setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				showLooseFocusAinimation(m_nTabIndex);
				m_nTabIndex = TAB_SELECT_ORDER;
				showOnFocusAnimation(m_nTabIndex, 150);
				
				dialog.cancel();
			}
		})
		.show();*/
		SekWordPopupOneButton popup;		
		popup = new SekWordPopupOneButton(this);
		popup.setMessage(this.getString(R.string.noscheduledata));
		popup.setListener(new com.sforeignpad.sekword.ui.studding.SekWordPopupOneButton.OnSEKLaunchListener() {
			
			@Override
			public void onOK() {
				// TODO Auto-generated method stub
				showLooseFocusAinimation(m_nTabIndex);
				m_nTabIndex = TAB_SELECT_ORDER;
				showOnFocusAnimation(m_nTabIndex, 150);
			}
		});
		
		popup.show();
	}
	
	
	private void initOrderButtons() {
		Button btnOrder1 = (Button)findViewById(R.id.btnOrder1);
		btnOrder1.setOnClickListener(m_orederBtnClickListener);
		
		Button btnOrder2 = (Button)findViewById(R.id.btnOrder2);
		btnOrder2.setOnClickListener(m_orederBtnClickListener);
				
		Button btnOrder3 = (Button)findViewById(R.id.btnOrder3);
		btnOrder3.setOnClickListener(m_orederBtnClickListener);
		
		
		Button btnOrder4 = (Button)findViewById(R.id.btnOrder4);
		btnOrder4.setOnClickListener(m_orederBtnClickListener);
		
		Button btnOrder5 = (Button)findViewById(R.id.btnOrder5);
		btnOrder5.setOnClickListener(m_orederBtnClickListener);
	}
	
	private View.OnClickListener m_orederBtnClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {

			ESUtils.playBtnSound(EsStuddingActivity.this);

			// TODO Auto-generated method stub
			switch(v.getId()) {
			case R.id.btnOrder1:
				showSelectOrderDlg(1);
				break;
			case R.id.btnOrder2:
				showSelectOrderDlg(2);
				break;
			case R.id.btnOrder3:
				showSelectOrderDlg(3);
				break;
			case R.id.btnOrder4:
				showSelectOrderDlg(4);
				break;
			case R.id.btnOrder5:
				showSelectOrderDlg(5);
				break;
			}
		}
	};
	
	//ì„ íƒ�í•œ ê¸‰ìˆ˜ì—� í•´ë‹¹í•œ í•™ìŠµì�„ ì‹œìž‘í•œë‹¤ëŠ” í†µë³´ë¬¸ ë³´ì—¬ì¤Œ
	private void showSelectOrderDlg(final int nOrder) {
		String strMsg = String.valueOf(nOrder) + this.getString(R.string.start_study_order);
		
		SekWordPopup popup;
		popup = new SekWordPopup(EsStuddingActivity.this);
		popup.setMessage(strMsg);
		popup.setListener(new OnSEKLaunchListener() {
			
			@Override
			public void onOK() {
				// TODO Auto-generated method stub
				setStudyOrderOfUser(nOrder);				
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub

			}
		});

			
		popup.show();
		
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(strMsg)
		.setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				setStudyOrderOfUser(nOrder);				
				dialog.cancel();
			}
		})
		.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.show();*/
		
	}
	
	//ê¸‰ìˆ˜ì—� ë”°ë¥´ëŠ” ì�¼ì •ìž‘ì„±í™”ë©´ìœ¼ë¡œ ì�´í–‰
	private void setStudyOrderOfUser(int nOrder) {
		m_nTempOrder = m_nOrder;
		m_nOrder = nOrder;
		
		m_scheduleData = m_scheduleDbMana.getScheduleData(m_nOrder);				//ê¸‰ìˆ˜ì—� í•´ë‹¹í•œ ì�¼ì •ì •ë³´ ì–»ê¸°
		if(m_scheduleData == null) m_scheduleData = new ScheduleData();
		
		m_arrSchdeuleDateData = m_scheduleDbMana.getScheduleListFromLevel(m_nOrder);	//ê¸‰ìˆ˜ì—� í•´ë‹¹í•œ ë§¤ ì�¼ì •ìž�ë£Œ	
		m_nTotalWordsOfOrder = m_studyDbMana.getWordCount(m_nOrder);				//ê¸‰ìˆ˜ì—� í•´ë‹¹í•œ í•™ìŠµí•  ì´�ë‹¨ì–´ìˆ˜
		
		if(!m_scheduleDbMana.IsExistScheduleData())	//ì²˜ì�Œ ì‚¬ìš©ìž�ì�¸ ê²½ìš° ì¦‰ ì�¼ì •ìž�ë£Œê°€ í•˜ë‚˜ë�„ ì—†ëŠ” ê²½ìš° 
			showScheduleDlg();						//ì�¼ì •ìž‘ì„±ëŒ€í™”ì°½ í˜„ì‹œ
		else {
			if(m_arrSchdeuleDateData.isEmpty()) 	//ì�´ë¯¸ ìž‘ì„±í•œ ë‹¤ë¥¸ ì�¼ì •ìž�ë£Œê°€ ìžˆë‹¤ë©´ ê¸°ì¡´ìž�ë£Œë¥¼ ì§€ìš´ë‹¤ëŠ” 
				showNewScheduleDlg();				//ëŒ€í™”ì°½ í˜„ì‹œ
			else 									//ì�¼ì •ìž�ë£Œê°€ ìžˆë‹¤ë©´ ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
				goMakeScheduleScreen(m_scheduleData.getDays());
		}
	}
	
	private void showNewScheduleDlg() {
		SekWordPopup popup;
		popup = new SekWordPopup(EsStuddingActivity.this);
		popup.setMessage(this.getString(R.string.new_schedule));
		//popup.setMessageSize(15);
		popup.setListener(new OnSEKLaunchListener() {
			
			@Override
			public void onOK() {
				// TODO Auto-generated method stub
				sendStudyComplete();
				showScheduleDlg();				
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				
			}
		});

			
		popup.show();
		
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(this.getString(R.string.new_schedule))
		.setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				//m_scheduleDbMana.deleteSchedule();
				sendStudyComplete();
				showScheduleDlg();				
				dialog.cancel();
			}
		})
		.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.show();*/
		
	}
	
	//ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
	private void goMakeScheduleScreen(int nDays) {
		if(m_nTabIndex != TAB_VIEW_SCHEDULE) {
			showLooseFocusAinimation(m_nTabIndex);
			m_nTabIndex = TAB_VIEW_SCHEDULE;
			showOnFocusAnimation(m_nTabIndex, 150);
			setTabLayout(m_nTabIndex);
		}
		
		setStudyOrder();
		setStudyInfo();
		setScheduleMake(nDays);
	}
	
	//ê¸‰ìˆ˜ì„ íƒ�ë‹¨ì¶”
	private void initTestButton() {
		Button btnTest = (Button)findViewById(R.id.btnStartTest);
		btnTest.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsStuddingActivity.this);

				goStudyTestActivity();
			}
		});
	}
	
	private void initScheduleMakeDlg() {
		m_dlgScheduleMake = new ScheduleMakeDialog(this);
		m_dlgScheduleMake.setScheduleMakeListener(new ScheduleMakeDialog.OnScheduleMakeListener() {

			@Override
			public void onOK() {
				// TODO Auto-generated method stub
				//show proress bar
				if(m_nTabIndex != TAB_VIEW_SCHEDULE) {
					showLooseFocusAinimation(m_nTabIndex);
					m_nTabIndex = TAB_VIEW_SCHEDULE;
					showOnFocusAnimation(m_nTabIndex, 150);
					setTabLayout(m_nTabIndex);
				}
				
				m_frameLoading.setVisibility(View.VISIBLE);

				new BackScheduleMakeTask().execute();
			}
			
			public void onCancel() {
				m_nOrder = m_nTempOrder;
			}
			
		});
	}
	
	private class BackScheduleMakeTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			/*try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/

			createScheduleData(m_dlgScheduleMake.getTotalDays(), m_dlgScheduleMake.getWordCountOfDay());

			return null;
		}
		
	    @Override
	    protected void onPostExecute(Void result) {
	        super.onPostExecute(result);
			goMakeScheduleScreen(m_dlgScheduleMake.getTotalDays());	
			sendStudyComplete();

			//m_frameLoading.setVisibility(View.INVISIBLE);

	    }
		
	}
	private void showScheduleDlg() {
		
		m_dlgScheduleMake.setWordCount(m_scheduleData.getWordCount());
//		m_dlgScheduleMake.initWordCount();
		m_dlgScheduleMake.setTotalWordCount(m_nTotalWordsOfOrder);
		m_dlgScheduleMake.setStartDate(m_scheduleData.getStartDate());
		m_dlgScheduleMake.setStudiedDaysAndWords(m_scheduleDbMana.getStudyDays(m_scheduleData), m_scheduleDbMana.getWordCount(m_scheduleData));
		m_dlgScheduleMake.show();
	}

	/**************************ì�¼ì •ë³´ê¸° ê´€ë ¨*********************************/
	
	private void initScheduleLayout() {

		m_schedules[0] = (OneDayScheduleView)findViewById(R.id.schedule1);
		m_schedules[1] = (OneDayScheduleView)findViewById(R.id.schedule2);
		m_schedules[2] = (OneDayScheduleView)findViewById(R.id.schedule3);
		m_schedules[3] = (OneDayScheduleView)findViewById(R.id.schedule4);
		m_schedules[4] = (OneDayScheduleView)findViewById(R.id.schedule5);
		m_schedules[5] = (OneDayScheduleView)findViewById(R.id.schedule6);
		m_schedules[6] = (OneDayScheduleView)findViewById(R.id.schedule7);
		m_schedules[7] = (OneDayScheduleView)findViewById(R.id.schedule8);

		for(int i=0; i<8; i++) {
			m_schedules[i].setDate("");
			m_schedules[i].setScheduleState(Define.SCHEDULE_NO);
		}
		
		m_ivStartOrder = (ImageView)findViewById(R.id.ivStartOrder);
		
		m_tvPerioDate = (TextView)findViewById(R.id.tvPerioDate);
		m_tvStudyStartDate = (TextView)findViewById(R.id.tvStartDate);
		m_tvStudyEndDate = (TextView)findViewById(R.id.tvEndDate);
		m_tvStudyWordCount = (TextView)findViewById(R.id.tvCount);

		ESUtils.setTextViewTypeFaceByRes(this, m_tvPerioDate, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, m_tvStudyStartDate, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, m_tvStudyEndDate, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, m_tvStudyWordCount, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.tvPerioDesc), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.tvStartDesc), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.tvEndDesc), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.tvCountDesc), Define.getMainFont());

		m_progressbar = (CustomProgressBar)findViewById(R.id.progressBar1);
		
		m_btnPrevDays = (Button)findViewById(R.id.btnPrev);
		m_btnPrevDays.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsStuddingActivity.this);

				doPrevButtonPeform();
			}
		});
		
		m_btnNextDays = (Button)findViewById(R.id.btnNext);
		m_btnNextDays.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsStuddingActivity.this);

				doNextButtonPeform();
			}
		});
		
		Button btnScheduleChange = (Button)findViewById(R.id.btnScheduleChange);
		btnScheduleChange.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsStuddingActivity.this);

				// ì�¼ì •ë³€ê²½
				showScheduleDlg();
			}
		});
	}
	
	//ì�¼ì •ë³´ê¸° í™”ì‚´í‘œë‹¨ì¶”ë¥¼ ëˆŒë €ì�„ë•Œì�˜ ì²˜ë¦¬
	private void doPrevButtonPeform() {
		m_nArrowCount --;
		
		if(m_nArrowCount <= 0) {
			m_nArrowCount = 0;
			m_btnPrevDays.setBackgroundResource(R.drawable.btn_left_invisible);
			m_btnPrevDays.setEnabled(false);
		}
		else {
			m_btnPrevDays.setBackgroundResource(R.drawable.btn_studding_day_left);
			m_btnPrevDays.setEnabled(true);
		}
		
		m_btnNextDays.setBackgroundResource(R.drawable.btn_studding_day_right);
		m_btnNextDays.setEnabled(true);
//		makeScheduleDate(m_dlgScheduleMake.getTotalDays(), m_nArrowCount);

		int nTotalDays = m_arrSchdeuleDateData.size();
		makeScheduleDate(nTotalDays, m_nArrowCount);
	}
		
	private void doNextButtonPeform() {
		m_nArrowCount ++;

		int nTotalDays = m_arrSchdeuleDateData.size();
		makeScheduleDate(nTotalDays, m_nArrowCount);

		int nTotal = nTotalDays /* m_dlgScheduleMake.getTotalDays()*//8;
		boolean bNoRemain = (/*m_dlgScheduleMake.getTotalDays()*/nTotalDays % 8 == 0) ? true : false;
		
		if(m_nArrowCount > nTotal-1) {
			m_nArrowCount = nTotal;
			
			m_btnNextDays.setBackgroundResource(R.drawable.btn_right_invisible);
			m_btnNextDays.setEnabled(false);
		}
		else if(m_nArrowCount == nTotal-1 && bNoRemain) {
			m_btnNextDays.setBackgroundResource(R.drawable.btn_right_invisible);
			m_btnNextDays.setEnabled(false);
		}
		else {
			m_btnNextDays.setBackgroundResource(R.drawable.btn_studding_day_right);
			m_btnNextDays.setEnabled(true);
		}
		
		m_btnPrevDays.setBackgroundResource(R.drawable.btn_studding_day_left);
		m_btnPrevDays.setEnabled(true);
		
		makeScheduleDate(/*m_dlgScheduleMake.getTotalDays()*/nTotalDays, m_nArrowCount);
	}

	int m_nDays;
	//í•™ìŠµì�¼ì • ìž‘ì„±
	private void setScheduleMake(int nDays) {	
		/*if((nDays / 8) > 0) {	//í•™ìŠµê¸°ì�¼ì�´ 8ì�¼ ì�´ìƒ�ì�´ë©´
			m_btnPrevDays.setBackgroundResource(R.drawable.btn_left_invisible);
			m_btnPrevDays.setEnabled(true);
			m_btnNextDays.setBackgroundResource(R.drawable.btn_studding_day_right);
			m_btnNextDays.setEnabled(true);
		}
		else {
			m_btnPrevDays.setBackgroundResource(R.drawable.btn_left_invisible);
			m_btnPrevDays.setEnabled(false);
			m_btnNextDays.setBackgroundResource(R.drawable.btn_right_invisible);
			m_btnNextDays.setEnabled(false);
		}*/
		
		getCurrentArrowcount();
		if(nDays > (m_nArrowCount + 1) * 8){
			m_btnNextDays.setBackgroundResource(R.drawable.btn_studding_day_right);
			m_btnNextDays.setEnabled(true);
		}else {
			m_btnNextDays.setBackgroundResource(R.drawable.btn_right_invisible);
			m_btnNextDays.setEnabled(false);
		}

		if(m_nArrowCount > 0){
			m_btnPrevDays.setBackgroundResource(R.drawable.btn_studding_day_left);
			m_btnPrevDays.setEnabled(true);
		} else {
			m_btnPrevDays.setBackgroundResource(R.drawable.btn_left_invisible);
			m_btnPrevDays.setEnabled(false);
		}
		m_nDays = nDays;
		setSchdeuleDate();
		//makeScheduleDate(nDays, m_nArrowCount);


	}


	private void makeScheduleDate(int nTotalDays, int nCount) {
		nTotalDays -= 1;
		
		for(int i=0; i<8; i++) {
			if(nTotalDays - 8*nCount - i < 0) { //ì„ íƒ�ë�œ ì�¼ì •ì�¼ì�´ í•™ìŠµê¸°ì�¼ë³´ë‹¤ í�¬ë‹¤ë©´ ì„¤ì •í•˜ì§€ ì•ŠëŠ”ë‹¤.
				m_schedules[i].setDate("");
			}
			else {
				m_schedules[i].setUserId(m_nUserId);
				m_schedules[i].setScheduleData(m_arrSchdeuleDateData.get(i + 8*nCount));
				if( i > 0 || nCount > 0){
					m_schedules[i].setPrevScheduleData(m_arrSchdeuleDateData.get(i - 1 + 8*nCount));
				}else{
					m_schedules[i].setPrevScheduleData(null);
				}
			}
		}
	}
	
	//ë§¤ ì�¼ì •ìž�ë£Œì—� ë‚ ìž� ì„¤ì •
	private SetScheduleDateTask gScheduleDateTask;

	private void setSchdeuleDate() {
		gScheduleDateTask = new SetScheduleDateTask();
		gScheduleDateTask.execute();
	}

	private class SetScheduleDateTask extends AsyncTask<Void, Void, Void>
	{

		private boolean isConnected = true;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			int days = 0;
			for(int i=0; i<m_arrSchdeuleDateData.size(); i++) {
				if(m_arrSchdeuleDateData.get(i).getMarks() < 70) {
					long value = ESUtils.getNowDateMilliseconds()+days*Define.ONE_DAY_TIME;
					m_arrSchdeuleDateData.get(i).setDate(value);
					m_studyDbMana.setDate(m_arrSchdeuleDateData.get(i));

					days++;
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			makeScheduleDate(m_nDays, m_nArrowCount);
			m_frameLoading.setVisibility(View.INVISIBLE);
		}
	}


	private void getCurrentArrowcount(){
		for(int i=0; i<m_arrSchdeuleDateData.size(); i++) {
			if(m_arrSchdeuleDateData.get(i).getMarks() < 70) {
				m_nArrowCount = i / 8;
				break;
			}
		}
	}
	
	//ì„ íƒ�ë�œ í•™ìŠµê¸‰ìˆ˜ í˜„ì‹œ
	private void setStudyOrder() {
		m_ivStartOrder.setImageResource(R.drawable.start_order1 + m_nOrder - 1); 
	}
	
	//ì‹œìž‘ë‚ ìž�, ë‹¨ì–´ê°œìˆ˜, ì™„ë£Œë‚ ìž�
	private void setStudyInfo() {
		String strStartDate = ESUtils.getDateString(m_scheduleData.getStartDate());
		String strEndDate = ESUtils.getDateString(m_scheduleDbMana.getLastDate(m_scheduleData));
		
		m_tvPerioDate.setText(strStartDate + "-" + strEndDate);
		m_tvStudyStartDate.setText(ESUtils.getDateStringOtherType(strStartDate));
		m_tvStudyEndDate.setText(ESUtils.getDateStringOtherType(strEndDate));
		m_tvStudyWordCount.setText(Integer.toString(m_scheduleDbMana.getWordCount(m_scheduleData)) + getString(R.string.sum));
		
		int nProgress = m_scheduleDbMana.getWordCount(m_scheduleData)*100/m_nTotalWordsOfOrder;
		m_progressbar.setProgress(nProgress);
	}
	
	/**************************DBê´€ë ¨******************************/
	//DBì ‘ì†�
	private boolean isConnectDB() {
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;
			
		m_scheduleDbMana = new ScheduleDBManager(this, m_nUserId);
		if(m_scheduleDbMana == null)
			return false;
		
		m_userDbMana = new UserDBManager(this);
		if (m_userDbMana == null)
			return false;
		
		return true;
	}
		
	private void closeDB() {
		if(gScheduleDateTask != null){
			gScheduleDateTask.cancel(true);
			gScheduleDateTask = null;
		}

		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
		
		if (m_scheduleDbMana != null) {
			m_scheduleDbMana.close();
			m_scheduleDbMana = null;
		}
		
		if (m_userDbMana != null) {
			m_userDbMana.close();
			m_userDbMana = null;
		}
	}
	
	//ì�¼ì •ìž�ë£Œ ì°½ì¡°
	private void createScheduleData(int nDays, int nWordCountOfDay) {
		if(m_scheduleData.getId() == -1) {			//ìƒˆë¡œìš´ ì�¼ì •ìž�ë£Œ ì°½ì¡°
			m_scheduleDbMana.deleteSchedule();
			
			m_scheduleData.setLevel(m_nOrder);
			m_scheduleData.setStartDate(ESUtils.getNowDateMilliseconds());
			m_scheduleData.setDays(nDays);
			m_scheduleData.setWordCount(nWordCountOfDay);
			
			m_scheduleDbMana.createSchedule(m_scheduleData);
		}
		else {		//ì�¼ì •ìž�ë£Œ ë³€ê²½
			m_scheduleData.setDays(nDays);
			m_scheduleData.setWordCount(nWordCountOfDay);
			
			m_scheduleDbMana.updateSchedule(m_scheduleData);
		}
		
		m_arrSchdeuleDateData = m_scheduleDbMana.getScheduleListFromLevel(m_nOrder);
	}
	
	//ì‹œí—˜í™”ë©´ìœ¼ë¡œ ì�´í–‰
	private void goStudyTestActivity() {
		Intent intent = new Intent(this, StudyLevelTestActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.IS_ORDERTEST, true);	
		intent.putExtra(Define.SCHEDULE_ID, -1);
		startActivity(intent);
	}
	
	//ê²°ê³¼ë³´ê¸° í™”ë©´ìœ¼ë¡œ ì�´í–‰
	private void goResultActivity() {
		Intent intent = new Intent(this, EsStudyResultActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		startActivity(intent);
	}
	
	private void sendStudyComplete() {
		Intent intent = new Intent(Define.USER_ORDER_ACTION);
		this.sendBroadcast(intent);
	}
	
	//ë¦¬ì‹œë²„ ë“±ë¡� ë°� í•´ì œ
	private void registerReceiver() {
		IntentFilter filter = new IntentFilter(Define.SCHEDULE_VIEW_ACTION);
		
		m_receiver = new ScheduleViewReceiver();
		registerReceiver(m_receiver, filter);
	}
	
	private void unregisterReceiver() {
		if(m_receiver != null)
			unregisterReceiver(m_receiver);
	}
	
	private class ScheduleViewReceiver extends BroadcastReceiver {
	   public void onReceive(Context context, Intent intent) {
	    	if (intent.getAction().equals(Define.SCHEDULE_VIEW_ACTION)) {
	    		m_nOrder = intent.getIntExtra(Define.STUDY_ORDER, 0);
	    		
	    		setStudyOrderOfUser(m_nOrder);
	    	}
	    }
	}
}