package com.sforeignpad.sekword.ui.words;

import java.util.ArrayList;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.structs.WordpadData;
import com.sforeignpad.sekword.ui.studding.SekWordPopup;
import com.sforeignpad.sekword.ui.studding.SekWordPopup.OnSEKLaunchListener;

import com.sforeignpad.sekword.ui.words.WordLauncherPopup.OnWordLauncherListener;
import com.sforeignpad.sekword.utils.ESUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * 
 * @author RWI
 * 2015.6.28
 * 단어장
 */

public class EsWordsActivity extends Activity implements View.OnClickListener, View.OnTouchListener {
	
	private int mTabIndex = 0; // 0:전체단어, 1:중요단어 2:틀린단어
	private int mSort = 0;     // 0:철자순 1:등록순
	private int mSort3 = 0;    // 0:영조 1:영어 2:조선어
	private int mUId = -1;     // 사용자ID
	
	private int mPageNum = 0;  // 페지번호
	private final int mBlockCount = 8;
	private int mStartNum = 0; 
	private int mEndPageNum = 0;
	private boolean mIsDelSelected = false;
	
	private Button[] mTabs;
	private Button btnSort1; // 철자순단추
	private Button btnSort2; // 등록순단추
	private Button btnSort3; // 현시단추
	private Button btnDel;   // 삭제단추
	private Button btnNext;  // 다음
	private Button btnPrev;  // 이전
	private TextView txtPageNum;
	private FrameLayout[] mItemLayouts;
	private TextView[] mItemEns;
	private TextView[] mItemKrs;
	private ImageButton[] btnItemDels;
	
	private WordLauncherPopup launcherPopup;
	
	private UserDBManager mUserDbMana;
	private WordpadDBManager mDbMana;
	private ArrayList<WordpadData> mDataList;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_word);
		
		Intent intent = this.getIntent();
		mUId = intent.getIntExtra(Define.USER_ID, -1);
		
		initViews();
	} 
	
	private void initViews() {
		initButtons();
		initLayout();
		setSelectedTab();
		setSortButton();
		setSort3Button();
		showLancher();
	}
	
	private void initButtons() {
		mTabs = new Button[4];
		
		mTabs[3] = (Button)findViewById(R.id.btnWordExit);
		mTabs[2] = (Button)findViewById(R.id.btnWordWrong);
		mTabs[1] = (Button)findViewById(R.id.btnWordImport);
		mTabs[0] = (Button)findViewById(R.id.btnWordAll);
		btnSort1 = (Button)findViewById(R.id.btnWordSort1);
		btnSort2 = (Button)findViewById(R.id.btnWordSort2);
		btnSort3 = (Button)findViewById(R.id.btnWordSort3);
		btnDel = (Button)findViewById(R.id.btnWordDel);
		
		btnItemDels = new ImageButton[8];
		
		btnItemDels[0] = (ImageButton)findViewById(R.id.btnDel1);
		btnItemDels[1] = (ImageButton)findViewById(R.id.btnDel2);
		btnItemDels[2] = (ImageButton)findViewById(R.id.btnDel3);
		btnItemDels[3] = (ImageButton)findViewById(R.id.btnDel4);
		btnItemDels[4] = (ImageButton)findViewById(R.id.btnDel5);
		btnItemDels[5] = (ImageButton)findViewById(R.id.btnDel6);
		btnItemDels[6] = (ImageButton)findViewById(R.id.btnDel7);
		btnItemDels[7] = (ImageButton)findViewById(R.id.btnDel8);
		
		btnNext = (Button)findViewById(R.id.btnWordLeft);
		btnPrev = (Button)findViewById(R.id.btnWordRight);
		
		for (int i=0; i<mTabs.length; i++) {
			mTabs[i].setOnClickListener(this);
		}
		
		for (int i=0; i<btnItemDels.length; i++) {
			btnItemDels[i].setOnClickListener(this);
			btnItemDels[i].setVisibility(View.GONE);
		}
		
		btnSort1.setOnClickListener(this);
		btnSort2.setOnClickListener(this);
		btnSort3.setOnClickListener(this);
		btnSort3.setOnTouchListener(this);
		btnDel.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnPrev.setOnClickListener(this);
	}
	
	private void initLayout() {
		mItemLayouts = new FrameLayout[8];
		
		mItemLayouts[0] = (FrameLayout)findViewById(R.id.itemLayout1);
		mItemLayouts[1] = (FrameLayout)findViewById(R.id.itemLayout2);
		mItemLayouts[2] = (FrameLayout)findViewById(R.id.itemLayout3);
		mItemLayouts[3] = (FrameLayout)findViewById(R.id.itemLayout4);
		mItemLayouts[4] = (FrameLayout)findViewById(R.id.itemLayout5);
		mItemLayouts[5] = (FrameLayout)findViewById(R.id.itemLayout6);
		mItemLayouts[6] = (FrameLayout)findViewById(R.id.itemLayout7);
		mItemLayouts[7] = (FrameLayout)findViewById(R.id.itemLayout8);
		
		for (int i=0; i<mItemLayouts.length; i++) {
			mItemLayouts[i].setOnClickListener(this);
		}
		
		mItemEns = new TextView[8];
		
		mItemEns[0] = (TextView)findViewById(R.id.txtEn1);
		mItemEns[1] = (TextView)findViewById(R.id.txtEn2);
		mItemEns[2] = (TextView)findViewById(R.id.txtEn3);
		mItemEns[3] = (TextView)findViewById(R.id.txtEn4);
		mItemEns[4] = (TextView)findViewById(R.id.txtEn5);
		mItemEns[5] = (TextView)findViewById(R.id.txtEn6);
		mItemEns[6] = (TextView)findViewById(R.id.txtEn7);
		mItemEns[7] = (TextView)findViewById(R.id.txtEn8);
		
		mItemKrs = new TextView[8];
		
		mItemKrs[0] = (TextView)findViewById(R.id.txtKr1);
		mItemKrs[1] = (TextView)findViewById(R.id.txtKr2);
		mItemKrs[2] = (TextView)findViewById(R.id.txtKr3);
		mItemKrs[3] = (TextView)findViewById(R.id.txtKr4);
		mItemKrs[4] = (TextView)findViewById(R.id.txtKr5);
		mItemKrs[5] = (TextView)findViewById(R.id.txtKr6);
		mItemKrs[6] = (TextView)findViewById(R.id.txtKr7);
		mItemKrs[7] = (TextView)findViewById(R.id.txtKr8);
		
		txtPageNum = (TextView)findViewById(R.id.txtPage);

		for(int i = 0; i < 8; i++){
			ESUtils.setTextViewTypeFaceByRes(this, mItemEns[i], Define.getEnglishFont());
			ESUtils.setTextViewTypeFaceByRes(this, mItemKrs[i], Define.getMainFont());
		}
		ESUtils.setTextViewTypeFaceByRes(this, txtPageNum, Define.getMainFont());
	}
	
	private void initListData() {
		if (openDB()) {
			if (mSort == 0)
				mDataList = mDbMana.getWordListBySort1(mUId, mSort3, mTabIndex);
			else
				mDataList = mDbMana.getWordListBySort2(mUId, mTabIndex);
			
			mEndPageNum = (mDataList.size() % mBlockCount == 0) ? mDataList.size() / mBlockCount : mDataList.size() / mBlockCount + 1;
			
			if (mPageNum > mEndPageNum) {
				mPageNum = 0;
				mStartNum = mPageNum * mBlockCount;
			}
			
			setDataList();
		}
	}
	
	// 단어자료얻기
	private void setDataList() {
		if (mPageNum == mEndPageNum-1)
			btnPrev.setBackgroundResource(R.drawable.word_right_disable);
		else
			btnPrev.setBackgroundResource(R.drawable.btn_word_right);

		if (mPageNum == 0)
			btnNext.setBackgroundResource(R.drawable.word_left_disable);
		else
			btnNext.setBackgroundResource(R.drawable.btn_word_left);
		
		for (int i=0; i<mItemLayouts.length; i++) {
			mItemLayouts[i].setVisibility(View.GONE);
			btnItemDels[i].setVisibility((mIsDelSelected) ? View.VISIBLE : View.GONE);
		}
		
		if (mEndPageNum > 0) {
			txtPageNum.setText(String.format("%s / %s", mPageNum+1, mEndPageNum));
		} else {
			btnPrev.setBackgroundResource(R.drawable.word_right_disable);
			btnNext.setBackgroundResource(R.drawable.word_left_disable);
			txtPageNum.setText(String.format("1 / 1"));
		}
		
		for (int i=mStartNum; i<(mStartNum+mBlockCount); i++) {
			if (i >= mDataList.size())
				break;
			
			WordData item = (WordData)mDataList.get(i).getWord();
			mItemEns[i-mStartNum].setText(item.getMeanEn());
			mItemKrs[i-mStartNum].setText(item.getMeanKr());			
			mItemLayouts[i-mStartNum].setVisibility(View.VISIBLE);
		}
	}
	
	// 탭설정(틀린단어, 중요단어, 전체단어)
	private void setSelectedTab() { 
		for (int i=0; i<mTabs.length; i++) {
			mTabs[i].setSelected(false);
		}
		
		mTabs[mTabIndex].setSelected(true);
		
		initListData();
	}
	
	// 단어목록에서 삭제단추처리
	private void setSelectedItemDel(final int index) { 
		if (openDB()) {
			/*new AlertDialog.Builder(this)
			.setTitle(R.string.word_del_title)
			.setMessage(R.string.word_del_msg)
			.setCancelable(false)
			.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			
				public void onClick(DialogInterface dialog, int which) {	
					WordpadData item = (WordpadData)mDataList.get(index+mStartNum);
					mDbMana.Delete(item);
					initListData();
				}
			})
			.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			})
			.show();*/
			SekWordPopup popup;

			popup = new SekWordPopup(EsWordsActivity.this);
			popup.setMessage(this.getString(R.string.word_del_msg));
			popup.setListener(new OnSEKLaunchListener() {

				@Override
				public void onOK() {
					WordpadData item = (WordpadData)mDataList.get(index+mStartNum);
					mDbMana.Delete(item);
					initListData();
				}

				@Override
				public void onCancel() {
					// TODO Auto-generated method stub
					
				}

			});
			
			popup.show();
		}
	}
	
	private void setItemDelButton(boolean value) {
		for (int i=0; i<btnItemDels.length; i++) {
			if (value)
				btnItemDels[i].setVisibility(View.VISIBLE);
			else
				btnItemDels[i].setVisibility(View.GONE);
		}
	}
	
	// 철자순/등록순 단추설정
	private void setSortButton() { 
		if (mSort == 0) {
			btnSort1.setSelected(true);
			btnSort2.setSelected(false);
		} else {
			btnSort1.setSelected(false);
			btnSort2.setSelected(true);
		}
		
		initListData();
	}
	
	// 현시단추설정
	private void setSort3Button() { 
		switch (mSort3) {
			case 0: // 영조
				btnSort3.setBackgroundResource(R.drawable.word_sort3_all);
				
				for (int i=0; i<mItemEns.length; i++) {
					mItemEns[i].setVisibility(View.VISIBLE);
					mItemKrs[i].setVisibility(View.VISIBLE);
				}
				break;
			case 1: //영
				btnSort3.setBackgroundResource(R.drawable.word_sort3_en);
				for (int i=0; i<mItemKrs.length; i++) {
					mItemEns[i].setVisibility(View.VISIBLE);
					mItemKrs[i].setVisibility(View.GONE);
				}
				break;
			case 2: //조
				btnSort3.setBackgroundResource(R.drawable.word_sort3_kr);
				for (int i=0; i<mItemEns.length; i++) {
					mItemEns[i].setVisibility(View.GONE);
					mItemKrs[i].setVisibility(View.VISIBLE);
				}
				break;
		}
	}
	
	private void showLancher() {

		/*if (openDB()) {
			int value = mUserDbMana.getIsUseWordHelp(mUId);
			if (value == 1) {
				if (launcherPopup == null) {
					launcherPopup = new WordLauncherPopup(this);
					launcherPopup.setListener(new OnWordLauncherListener() {
						
						@Override
						public void onOK() {
							// TODO Auto-generated method stub
							mUserDbMana.setNoUseWordHelp(mUId);
						}
					});
				}
				launcherPopup.show();
			}
		}*/
	}
	
	private void nextPage() {
		if (mPageNum < mEndPageNum-1) {
			mPageNum ++;
			mStartNum = mPageNum * mBlockCount;
			
			setDataList();
		}
	}
	
	private void prePage() {
		if (mPageNum > 0) {
			mPageNum--;
			mStartNum = mPageNum * mBlockCount;
			
			setDataList();
		} 
	}
	
	// 삭제단추처리
	private void setDeleteButton() {
		if (!btnDel.isSelected()) {
			btnDel.setSelected(true);
			setItemDelButton(true);
		} else {
			btnDel.setSelected(false);
			setItemDelButton(false);
		}
		
		mIsDelSelected = btnDel.isSelected();
	}
	
	
	// 단어목록에서 단어를 선택하였을때 
	private void playWord(int index) {
	//	Intent intent = new Intent(EsWordsActivity.this, WordPlayActivity.class);
		/*Intent intent = new Intent(EsWordsActivity.this, WordStudyActivity.class);
		
		WordpadData item = (WordpadData)mDataList.get(index+mPageNum * mBlockCount);
		intent.putExtra(WordPlayActivity.WORD_ID, item.getWid());
		
		EsWordsActivity.this.startActivity(intent);*/
		
		playWordList(index);
	}
	
	private void playWordList(int index){
		Intent intent = new Intent(EsWordsActivity.this, WordStudyActivity.class);
		
		ArrayList<Integer> listWordIds = new ArrayList<Integer> ();
		String strIds="";
		
		for( int i = index + mPageNum * mBlockCount; i < mDataList.size(); i++ ){
			WordpadData item = mDataList.get(i);
			if(strIds.length() == 0){
				strIds += String.format("%d", item.getWid());
			}else{
				strIds += String.format(":%d", item.getWid());
			}
		}
		
		for(int i = 0; i < index + mPageNum * mBlockCount; i++){
			WordpadData item = mDataList.get(i);
			strIds += String.format(":%d", item.getWid());
		}
		//WordpadData item = (WordpadData)mDataList.get(index+mPageNum * mBlockCount);
		//intent.putExtra(WordPlayActivity.WORD_ID, item.getWid());
		
		intent.putExtra(WordPlayActivity.WORD_ID, strIds);
		EsWordsActivity.this.startActivity(intent);
	}
	
	// 단어목록에서 전체보기를 선택하였을때 
	private void playWords() {
	//	Intent intent = new Intent(EsWordsActivity.this, WordPlayActivity.class);
		Intent intent = new Intent(EsWordsActivity.this, WordStudyActivity.class);
		
		intent.putExtra(WordPlayActivity.KIND_ID, mTabIndex);
		intent.putExtra(WordPlayActivity.USER_ID, mUId);
		intent.putExtra(WordPlayActivity.SORT_ID, mSort3);
		intent.putExtra(WordPlayActivity.SORT_ID1, mSort);
		
		EsWordsActivity.this.startActivity(intent);
	}
	
	private boolean openDB() {
		if (mDbMana == null) 
			mDbMana = new WordpadDBManager(this);
		
		if (mDbMana == null)
			return false;
		
		if (mUserDbMana == null)
			mUserDbMana = new UserDBManager(this);
		
		if (mUserDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
		
		if (mUserDbMana != null) {
			mUserDbMana.close();
			mUserDbMana = null;
		}
	}

	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(this);

		// TODO Auto-generated method stub
		switch(v.getId()) {
			case R.id.btnWordWrong: // 틀린단어
				mTabIndex = 2;
				setSelectedTab();
				break;
			case R.id.btnWordImport: // 중요단어
				mTabIndex = 1;
				setSelectedTab();
				break;
			case R.id.btnWordAll: // 전체단어
				mTabIndex = 0;
				setSelectedTab();
				break;
			case R.id.btnWordExit: // 나가기
				this.finish();
				break;
			case R.id.btnWordSort1: // 철자순
				mSort = 0;
				setSortButton();
				break;
			case R.id.btnWordSort2: // 등록순
				mSort = 1;
				setSortButton();
				break;
			case R.id.btnWordDel: // 삭제단추
				setDeleteButton();
				break;
			case R.id.btnDel1: // 단어목록에서 삭제단추
				setSelectedItemDel(0);
				break;
			case R.id.btnDel2:
				setSelectedItemDel(1);
				break;
			case R.id.btnDel3:
				setSelectedItemDel(2);
				break;
			case R.id.btnDel4:
				setSelectedItemDel(3);
				break;
			case R.id.btnDel5:
				setSelectedItemDel(4);
				break;
			case R.id.btnDel6:
				setSelectedItemDel(5);
				break;
			case R.id.btnDel7:
				setSelectedItemDel(6);
				break;
			case R.id.btnDel8:
				setSelectedItemDel(7);
				break;
			case R.id.btnWordLeft: // 이전
				prePage();
				break;
			case R.id.btnWordRight: // 다음
				nextPage();
				break;
			case R.id.itemLayout1: // 단어목록을 선택
				playWord(0);
				//playWords();
				break;
			case R.id.itemLayout2:
				playWord(1);
				break;
			case R.id.itemLayout3:
				playWord(2);
				break;
			case R.id.itemLayout4:
				playWord(3);
				break;
			case R.id.itemLayout5:
				playWord(4);
				break;
			case R.id.itemLayout6:
				playWord(5);
				break;
			case R.id.itemLayout7:
				playWord(6);
				break;
			case R.id.itemLayout8:
				playWord(7);
				break;
		}		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
			case R.id.btnWordSort3: // 현시단추처리
				int x = (int)event.getX();
				if (x <= 41) {
					mSort3 = 1;
				} else if (x > 41 && x <= 82) {
					mSort3 = 0;
				} else {
					mSort3 = 2;
				}
				setSort3Button();
				break;
		}
		
		return true;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		//initListData();
		
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		closeDB();
		
		super.onDestroy();
	}
}
