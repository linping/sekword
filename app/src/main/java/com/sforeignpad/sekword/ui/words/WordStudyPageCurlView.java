package com.sforeignpad.sekword.ui.words;

import java.io.FileInputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.PAKDBManager;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.structs.WordpadData;
import com.sforeignpad.sekword.ui.studding.study.CurlRenderer;

/**
 * 
 * @author Moritz 'Moss' Wundke (b.thax.dcg@gmail.com)
 *
 */
public class WordStudyPageCurlView extends View {
	
	/** Our Log tag */
	private final static String TAG = "PageCurlView";
	
	// Debug text paint stuff
	private Paint mTextPaint;
	private TextPaint mTextPaintShadow;
	
	/** Px / Draw call */
	private int mCurlSpeed;		//ì�´ë�™ê±°ë¦¬
	
	/** Fixed update time used to create a smooth curl animation */
	private int mUpdateRate;	//ì• ë‹ˆë©”ì�´ì…˜ íƒ€ìž„ì‹œê°„
	
	/** The initial offset for x and y axis movements */
	private int mInitialEdgeOffset;	//ì´ˆê¸° ì¢…ì�´ë��ë³€ë‘�ë¦¬í�¬ê¸°
	
	/** The mode we will use */
	private int mCurlMode;
	
	/** Simple curl mode. Curl target will move only in one axis. */
	public static final int CURLMODE_SIMPLE = 0;
	
	/** Dynamic curl mode. Curl target will move on both X and Y axis. */
	public static final int CURLMODE_DYNAMIC = 1;
	
	/** Enable/Disable debug mode */
	private boolean bEnableDebugMode = false;
	
	/** The context which owns us */
	private WeakReference<Context> mContext;
	
	/** Handler used to auto flip time based */
	private FlipAnimationHandler mAnimationHandler;
	
	/** Maximum radius a page can be flipped, by default it's the width of the view */
	private float mFlipRadius;
	
	/** Point used to move */
	private Vector2D mMovement;
	
	/** The finger position */
	private Vector2D mFinger;
	
	/** Movement point form the last frame */
	private Vector2D mOldMovement;
	
	/** Page curl edge */
	private Paint mCurlEdgePaint;
	
	/** Our points used to define the current clipping paths in our draw call */
	private Vector2D mA, mB, mC, mD, mE, mF, mOldF, mOrigin;
	
	/** Left and top offset to be applied when drawing */
	private int mCurrentLeft, mCurrentTop;
	
	/** If false no draw call has been done */
	private boolean bViewDrawn;
	
	/** Defines the flip direction that is currently considered */
	private boolean bFlipRight;		//íŽ¼ì¹˜ëŠ” ë°©í–¥ì�´ ì˜¤ë¥¸ìª½ì�´ë�¼ë©´ true, ì™¼ìª½ì�´ë�¼ë©´ false
	
	/** If TRUE we are currently auto-flipping */
	private boolean bFlipping;
	
	/** TRUE if the user moves the pages */
	private boolean bUserMoves;

	/** Used to control touch input blocking */
	private boolean bBlockTouchInput = false;	//í„°ì¹˜ì‚¬ê±´ì�„ ë°›ì�„ìˆ˜ ìžˆë‹¤ë©´ true, ì¦‰ í•œë²ˆì—� í•˜ë‚˜ì�˜ ì¢…ì�´ìž¥ë§Œ ë„˜ê¸¸ìˆ˜ ìžˆê²Œ í•˜ì˜€ë‹¤.
												//í˜„ìž¬ëŠ” í•œë²ˆì—� í•˜ë‚˜ì�˜ ì¢…ì�´ë§Œ ë„˜ê¸¸ìˆ˜ ìžˆê²Œ í•˜ì˜€ë‹¤.
	
	/** Enable input after the next draw event */
	private boolean bEnableInputAfterDraw = false;
	
	/** LAGACY The current foreground */
	private Bitmap mForeground;
	
	/** LAGACY The current background */
	private Bitmap mBackground;
	
	/** LAGACY List of pages, this is just temporal */
	private ArrayList<Bitmap> mPages;
	
	/** LAGACY Current selected page */
	private int mIndex = 0;
	
	////////////////////LJG////////////////////////
	//private final Point EN_WORD_POINT = new Point(140, 180);
	//private final Point KR_WORD_POINT = new Point(910, 180);

	private final Point EN_WORD_POINT = new Point(160, 200);
	private final Point EN_PRON_POINT = new Point(160, 230);
	private final Point KR_WORD_POINT = new Point(910 - 1024 + 1280 - 20, 215);
	
	private Context m_context = null;
	private AssetManager m_assetMgr = null;
	
	private int m_nWordBgColorEn = 0;	//ì˜�ì–´ë‹¨ì–´ë°°ê²½ìƒ‰
	private int m_nWordBgColorKr = 0;	//ì¡°ì„ ì–´ë‹¨ì–´ë°°ê²½ìƒ‰
	
	private int m_nCurrentWordCount = 0;	//í˜„ìž¬ í•™ìŠµí•˜ëŠ” ë‹¨ì–´ê°œìˆ˜
	private int m_nStudySpeed = 0;			//ì„¤ì •ì—�ì„œ ì„ íƒ�í•œ í•™ìŠµì†�ë�„

	private Paint m_paintLeftText, m_paintRightText, m_paintPronText;
	
	private ArrayList<WordpadData> m_arrStudyWords = null;		//í•™ìŠµë‹¨ì–´ë°°ë ¬
	
	private SoundPool m_soundPool = null;				//ì�Œì„±
	private HashMap<String, Integer> m_soundMap = new HashMap<String, Integer>();
	private float m_fVolume = 0.0f;
	
	private ArrayList<Bitmap> m_arrStudyBmps = new ArrayList<Bitmap>();	//í•™ìŠµë‹¨ì–´ê·¸ë¦¼
	
	private Thread m_threadStudy = null;
	private boolean m_bStudyStop = false;
	private boolean m_bWordEnDraw = false;
	private boolean m_bWordKrDraw = false;
	private boolean m_bWordPicDraw = false;
	
	private Thread m_threadTextBg = null;
	private boolean m_bDrawBgStopEn = false, m_bDrawBgStopKr = false;
	private int m_nTextBgRightEn = 0;
	private int m_nTextBgRightKr = 0;
	
	private boolean m_bStudyPause = false;
	private boolean m_bArrowBtnPressed = false;
	
	private int[] m_studyTimeList = {};
	private int m_nBgTimeSleep = 0;
	private int m_nBgWidthCount = 2;
	
	private WordStudyListener m_listener;
	/**
	 * Inner class used to represent a 2D point.
	 */
	private class Vector2D
	{
		public float x,y;
		public Vector2D(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
		
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "("+this.x+","+this.y+")";
		}
		
		 public float length() {
             return (float) Math.sqrt(x * x + y * y);
	     }
	
	     public float lengthSquared() {
	             return (x * x) + (y * y);
	     }
		
		public boolean equals(Object o) {
			if (o instanceof Vector2D) {
				Vector2D p = (Vector2D) o;
				return p.x == x && p.y == y;
	        }
	        return false;
		}
		
		public Vector2D reverse() {
			return new Vector2D(-x,-y);
		}
		
		public Vector2D sum(Vector2D b) {
            return new Vector2D(x+b.x,y+b.y);
		}
		
		public Vector2D sub(Vector2D b) {
            return new Vector2D(x-b.x,y-b.y);
		}		

		public float dot(Vector2D vec) {
            return (x * vec.x) + (y * vec.y);
		}

	    public float cross(Vector2D a, Vector2D b) {
	            return a.cross(b);
	    }
	
	    public float cross(Vector2D vec) {
	            return x * vec.y - y * vec.x;
	    }
	    
	    public float distanceSquared(Vector2D other) {
	    	float dx = other.x - x;
	    	float dy = other.y - y;

            return (dx * dx) + (dy * dy);
	    }
	
	    public float distance(Vector2D other) {
	            return (float) Math.sqrt(distanceSquared(other));
	    }
	    
	    public float dotProduct(Vector2D other) {
            return other.x * x + other.y * y;
	    }
		
		public Vector2D normalize() {
			float magnitude = (float) Math.sqrt(dotProduct(this));
            return new Vector2D(x / magnitude, y / magnitude);
		}
		
		public Vector2D mult(float scalar) {
	            return new Vector2D(x*scalar,y*scalar);
	    }
	}

	/**
	 * Inner class used to make a fixed timed animation of the curl effect.
	 */
	class FlipAnimationHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			WordStudyPageCurlView.this.FlipAnimationStep();
		}

		public void sleep(long millis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), millis);
		}
	}
    
	/**
	 * Base
	 * @param context
	 */
	public WordStudyPageCurlView(Context context) {
		super(context);
		init(context);
		initStudyView(context);
		ResetClipEdge();
	}
	
	/**
	 * Construct the object from an XML file. Valid Attributes:
	 * 
	 * @see android.view.View#View(android.content.Context, android.util.AttributeSet)
	 */
	public WordStudyPageCurlView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
		initStudyView(context);
		
		// Get the data from the XML AttributeSet
		{
			TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StudyPageCurlView);
	
			// Get data
			bEnableDebugMode = a.getBoolean(R.styleable.StudyPageCurlView_enableDebugMode, bEnableDebugMode);
			mCurlSpeed = a.getInt(R.styleable.StudyPageCurlView_curlSpeed, mCurlSpeed);
			mUpdateRate = a.getInt(R.styleable.StudyPageCurlView_updateRate, mUpdateRate);
			mInitialEdgeOffset = a.getInt(R.styleable.StudyPageCurlView_initialEdgeOffset, mInitialEdgeOffset);
			mCurlMode = a.getInt(R.styleable.StudyPageCurlView_curlMode, mCurlMode);
			
			Log.i(TAG, "mCurlSpeed: " + mCurlSpeed);
			Log.i(TAG, "mUpdateRate: " + mUpdateRate);
			Log.i(TAG, "mInitialEdgeOffset: " + mInitialEdgeOffset);
			Log.i(TAG, "mCurlMode: " + mCurlMode);
			
			// recycle object (so it can be used by others)
			a.recycle();
		}
		
		ResetClipEdge();
	}
	
	/**
	 * Initialize the view
	 */
	private final void init(Context context) {
		// Foreground text paint
		mTextPaint = new Paint();
		mTextPaint.setAntiAlias(true);
		mTextPaint.setTextSize(16);
		mTextPaint.setColor(0xFF000000);
		
		// The shadow
		mTextPaintShadow = new TextPaint();
		mTextPaintShadow.setAntiAlias(true);
		mTextPaintShadow.setTextSize(16);
		mTextPaintShadow.setColor(0x00000000);
		
		// Cache the context
		mContext = new WeakReference<Context>(context);
		
		// Base padding
		setPadding(3, 3, 3, 3);
		
		// The focus flags are needed
		setFocusable(true);
		setFocusableInTouchMode(true);
		
		mMovement =  new Vector2D(0,0);
		mFinger = new Vector2D(0,0);
		mOldMovement = new Vector2D(0,0);
		
		// Create our curl animation handler
		mAnimationHandler = new FlipAnimationHandler();
		
		// Create our edge paint
		mCurlEdgePaint = new Paint();
		mCurlEdgePaint.setColor(Color.WHITE);
		mCurlEdgePaint.setAntiAlias(true);
		mCurlEdgePaint.setStyle(Paint.Style.FILL);
		mCurlEdgePaint.setShadowLayer(10, -5, 5, 0x99000000);
		
		// Set the default props, those come from an XML :D
		mCurlSpeed = 30;
		mUpdateRate = 33;
		mInitialEdgeOffset = 20;
		mCurlMode = 1;
		
		// LEGACY PAGE HANDLING!
		
		// Create pages
		mPages = new ArrayList<Bitmap>();
		
	}
	
	public void setPageBitmap(int nResId) {
		mPages.add(BitmapFactory.decodeResource(getResources(), nResId));
		mPages.add(BitmapFactory.decodeResource(getResources(), nResId));
		
		// Create some sample images
		mForeground = mPages.get(0);
		mBackground = mPages.get(1);
	}
	
	/**
	 * Reset points to it's initial clip edge state
	 */
	public void ResetClipEdge()
	{
		// Set our base movement
		mMovement.x = mInitialEdgeOffset;
		mMovement.y = mInitialEdgeOffset;		
		mOldMovement.x = 0;
		mOldMovement.y = 0;		
		
		// Now set the points
		// TODO: OK, those points MUST come from our measures and
		// the actual bounds of the view!
		mA = new Vector2D(mInitialEdgeOffset, 0);
		mB = new Vector2D(this.getWidth(), this.getHeight());
		mC = new Vector2D(this.getWidth(), 0);
		mD = new Vector2D(0, 0);
		mE = new Vector2D(0, 0);
		mF = new Vector2D(0, 0);		
		mOldF = new Vector2D(0, 0);
		
		// The movement origin point
		mOrigin = new Vector2D(this.getWidth(), 0);
	}
	
	/**
	 * Return the context which created use. Can return null if the
	 * context has been erased.
	 */
	private Context GetContext() {
		return mContext.get();
	}
	
	/**
	 * See if the current curl mode is dynamic
	 * @return TRUE if the mode is CURLMODE_DYNAMIC, FALSE otherwise
	 */
	public boolean IsCurlModeDynamic()
	{
		return mCurlMode == CURLMODE_DYNAMIC;
	}
	
	/**
	 * Set the curl speed.
	 * @param curlSpeed - New speed in px/frame
	 * @throws IllegalArgumentException if curlspeed < 1
	 */
	public void SetCurlSpeed(int curlSpeed)
	{
		if ( curlSpeed < 1 )
			throw new IllegalArgumentException("curlSpeed must be greated than 0");
		mCurlSpeed = curlSpeed;
	}
	
	/**
	 * Get the current curl speed
	 * @return int - Curl speed in px/frame
	 */
	public int GetCurlSpeed()
	{
		return mCurlSpeed;
	}
	
	/**
	 * Set the update rate for the curl animation
	 * @param updateRate - Fixed animation update rate in fps
	 * @throws IllegalArgumentException if updateRate < 1
	 */
	public void SetUpdateRate(int updateRate)
	{
		if ( updateRate < 1 )
			throw new IllegalArgumentException("updateRate must be greated than 0");
		mUpdateRate = updateRate;
	}
	
	/**
	 * Get the current animation update rate
	 * @return int - Fixed animation update rate in fps
	 */
	public int GetUpdateRate()
	{
		return mUpdateRate;
	}
	
	/**
	 * Set the initial pixel offset for the curl edge
	 * @param initialEdgeOffset - px offset for curl edge
	 * @throws IllegalArgumentException if initialEdgeOffset < 0
	 */
	public void SetInitialEdgeOffset(int initialEdgeOffset)
	{
		if ( initialEdgeOffset < 0 )
			throw new IllegalArgumentException("initialEdgeOffset can not negative");
		mInitialEdgeOffset = initialEdgeOffset;
	}
	
	/**
	 * Get the initial pixel offset for the curl edge
	 * @return int - px
	 */
	public int GetInitialEdgeOffset()
	{
		return mInitialEdgeOffset;
	}
	
	/**
	 * Set the curl mode.
	 * <p>Can be one of the following values:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Value</th><th>Description</th></tr>
     * <tr><td><code>{@link #CURLMODE_SIMPLE com.dcg.pagecurl:CURLMODE_SIMPLE}</code></td><td>Curl target will move only in one axis.</td></tr>
     * <tr><td><code>{@link #CURLMODE_DYNAMIC com.dcg.pagecurl:CURLMODE_DYNAMIC}</code></td><td>Curl target will move on both X and Y axis.</td></tr>
     * </table>
     * @see #CURLMODE_SIMPLE
     * @see #CURLMODE_DYNAMIC
	 * @param curlMode
	 * @throws IllegalArgumentException if curlMode is invalid
	 */
	public void SetCurlMode(int curlMode)
	{
		if ( curlMode != CURLMODE_SIMPLE &&
			 curlMode != CURLMODE_DYNAMIC )
			throw new IllegalArgumentException("Invalid curlMode");
		mCurlMode = curlMode;
	}
	
	/**
	 * Return an integer that represents the current curl mode.
	 * <p>Can be one of the following values:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Value</th><th>Description</th></tr>
     * <tr><td><code>{@link #CURLMODE_SIMPLE com.dcg.pagecurl:CURLMODE_SIMPLE}</code></td><td>Curl target will move only in one axis.</td></tr>
     * <tr><td><code>{@link #CURLMODE_DYNAMIC com.dcg.pagecurl:CURLMODE_DYNAMIC}</code></td><td>Curl target will move on both X and Y axis.</td></tr>
     * </table>
     * @see #CURLMODE_SIMPLE
     * @see #CURLMODE_DYNAMIC
     * @return int - current curl mode
	 */
	public int GetCurlMode()
	{
		return mCurlMode;
	}
	
	/**
	 * Enable debug mode. This will draw a lot of data in the view so you can track what is happening
	 * @param bFlag - boolean flag
	 */
	public void SetEnableDebugMode(boolean bFlag)
	{
		bEnableDebugMode = bFlag;
	}
	
	/**
	 * Check if we are currently in debug mode.
	 * @return boolean - If TRUE debug mode is on, FALSE otherwise.
	 */
	public boolean IsDebugModeEnabled()
	{
		return bEnableDebugMode;
	}

	/**
	 * @see android.view.View#measure(int, int)
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int finalWidth, finalHeight;
		finalWidth = measureWidth(widthMeasureSpec);
		finalHeight = measureHeight(heightMeasureSpec);
		setMeasuredDimension(finalWidth, finalHeight);
	}

	/**
	 * Determines the width of this view
	 * @param measureSpec A measureSpec packed into an int
	 * @return The width of the view, honoring constraints from measureSpec
	 */
	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		
		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text
			result = specSize;
		}
		
		return result;
	}

	/**
	 * Determines the height of this view
	 * @param measureSpec A measureSpec packed into an int
	 * @return The height of the view, honoring constraints from measureSpec
	 */
	private int measureHeight(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		
		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else {
			// Measure the text (beware: ascent is a negative number)
			result = specSize;
		}
		return result;
	}

	/**
	 * Render the text
	 * 
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	//@Override
	//protected void onDraw(Canvas canvas) {
	//	super.onDraw(canvas);
	//	canvas.drawText(mText, getPaddingLeft(), getPaddingTop() - mAscent, mTextPaint);
	//}
	
	//---------------------------------------------------------------
	// Curling. This handles touch events, the actual curling
	// implementations and so on.
	//---------------------------------------------------------------
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!bBlockTouchInput) {
			
			// Get our finger position
			mFinger.x = event.getX();
			mFinger.y = event.getY();
			int width = getWidth();
			
			// Depending on the action do what we need to
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:				
				mOldMovement.x = mFinger.x;
				mOldMovement.y = mFinger.y;
				
				// If we moved over the half of the display flip to next
				if (mOldMovement.x > (width >> 1)) {
					mMovement.x = mInitialEdgeOffset;
					mMovement.y = mInitialEdgeOffset;
					
					// Set the right movement flag
					bFlipRight = true;
				} else {
					// Set the left movement flag
					bFlipRight = false;
					
					// go to next previous page
					previousView();
					
					// Set new movement
					mMovement.x = IsCurlModeDynamic()?width<<1:width;
					mMovement.y = mInitialEdgeOffset;
				}
				
				break;
			case MotionEvent.ACTION_UP:					
				pauseWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getSound());
				WordStudyActivity.playWordVoice("page");
				
				bUserMoves=false;
				bFlipping=true;
				FlipAnimationStep();
				break;
			case MotionEvent.ACTION_MOVE:
				bUserMoves=true;
				
				// Get movement
				mMovement.x -= mFinger.x - mOldMovement.x;
				mMovement.y -= mFinger.y - mOldMovement.y;
				mMovement = CapMovement(mMovement, true);
				
				// Make sure the y value get's locked at a nice level
				if ( mMovement.y  <= 1 )
					mMovement.y = 1;
				
				// Get movement direction
				/*if (mFinger.x < mOldMovement.x ) {
					bFlipRight = true;
				} else {
					bFlipRight = false;
				}*/
				
				// Save old movement values
				mOldMovement.x  = mFinger.x;
				mOldMovement.y  = mFinger.y;
				
				// Force a new draw call
				DoPageCurl();
				this.invalidate();
				break;
			}

			if(bFlipping || bUserMoves) {	//LJG
				m_bStudyPause = true;		//íŽ˜ì§€ê°€ ë„˜ì–´ê°€ëŠ” ë�™ì•ˆì�€ í•™ìŠµì¤‘ì§€
			}
		}
		
		// TODO: Only consume event if we need to.
		return true;
	}
	
	/**
	 * Make sure we never move too much, and make sure that if we 
	 * move too much to add a displacement so that the movement will 
	 * be still in our radius.
	 * @param radius - radius form the flip origin
	 * @param bMaintainMoveDir - Cap movement but do not change the
	 * current movement direction
	 * @return Corrected point
	 */
	private Vector2D CapMovement(Vector2D point, boolean bMaintainMoveDir)
	{
		// Make sure we never ever move too much
		if (point.distance(mOrigin) > mFlipRadius)
		{
			if ( bMaintainMoveDir )
			{
				// Maintain the direction
				point = mOrigin.sum(point.sub(mOrigin).normalize().mult(mFlipRadius));
			}
			else
			{
				// Change direction
				if ( point.x > (mOrigin.x+mFlipRadius))
					point.x = (mOrigin.x+mFlipRadius);
				else if ( point.x < (mOrigin.x-mFlipRadius) )
					point.x = (mOrigin.x-mFlipRadius);
				point.y = (float) (Math.sin(Math.acos(Math.abs(point.x-mOrigin.x)/mFlipRadius))*mFlipRadius);
			}
		}
		return point;
	}
	
	/**
	 * Execute a step of the flip animation
	 */
	public void FlipAnimationStep() {
		if ( !bFlipping )
			return;
		
		int width = getWidth();
			
		// No input when flipping
		bBlockTouchInput = true;
		
		// Handle speed
		float curlSpeed = mCurlSpeed;
		if ( !bFlipRight )
			curlSpeed *= -1;
		
		// Move us
		mMovement.x += curlSpeed;
		mMovement = CapMovement(mMovement, false);
		
		// Create values
		DoPageCurl();
		
		// Check for endings :D
		if (mA.x < 1 || mA.x > width - 1) {
			bFlipping = false;
			if (bFlipRight) {
				//SwapViews();
				nextView();
			} 
			ResetClipEdge();
			
			// Create values
			DoPageCurl();

			// Enable touch input after the next draw event
			bEnableInputAfterDraw = true;
			
			//LJG	íŽ˜ì§€ê°€ ë‹¤ ë„˜ì–´ê°€ë©´ ë‹¤ì‹œ í•™ìŠµ
			if(bFlipRight)
				doNext();
			else
				doPrev();
		}
		else
		{
			mAnimationHandler.sleep(mUpdateRate);
		}
		
		// Force a new draw call
		this.invalidate();
	}
	
	/**
	 * Do the page curl depending on the methods we are using
	 */
	private void DoPageCurl()
	{
		if(bFlipping){
			if ( IsCurlModeDynamic() )
				doDynamicCurl();
			else
				doSimpleCurl();
			
		} else {
			if ( IsCurlModeDynamic() )
				doDynamicCurl();
			else
				doSimpleCurl();
		}
	}
	
	/**
	 * Do a simple page curl effect
	 */
	private void doSimpleCurl() {
		int width = getWidth();
		int height = getHeight();
		
		// Calculate point A
		mA.x = width - mMovement.x;
		mA.y = height;

		// Calculate point D
		mD.x = 0;
		mD.y = 0;
		if (mA.x > width / 2) {
			mD.x = width;
			mD.y = height - (width - mA.x) * height / mA.x;
		} else {
			mD.x = 2 * mA.x;
			mD.y = 0;
		}
		
		// Now calculate E and F taking into account that the line
		// AD is perpendicular to FB and EC. B and C are fixed points.
		double angle = Math.atan((height - mD.y) / (mD.x + mMovement.x - width));
		double _cos = Math.cos(2 * angle);
		double _sin = Math.sin(2 * angle);

		// And get F
		mF.x = (float) (width - mMovement.x + _cos * mMovement.x);
		mF.y = (float) (height - _sin * mMovement.x);
		
		// If the x position of A is above half of the page we are still not
		// folding the upper-right edge and so E and D are equal.
		if (mA.x > width / 2) {
			mE.x = mD.x;
			mE.y = mD.y;
		}
		else
		{
			// So get E
			mE.x = (float) (mD.x + _cos * (width - mD.x));
			mE.y = (float) -(_sin * (width - mD.x));
		}
	}

	/**
	 * Calculate the dynamic effect, that one that follows the users finger
	 */
	private void doDynamicCurl() {
		int width = getWidth();
		int height = getHeight();

		// F will follow the finger, we add a small displacement
		// So that we can see the edge
		mF.x = width - mMovement.x+0.1f;
		mF.y = height - mMovement.y+0.1f;
		
		// Set min points
		if(mA.x==0) {
			mF.x= Math.min(mF.x, mOldF.x);
			mF.y= Math.max(mF.y, mOldF.y);
		}
		
		// Get diffs
		float deltaX = width-mF.x;
		float deltaY = height-mF.y;

		float BH = (float) (Math.sqrt(deltaX * deltaX + deltaY * deltaY) / 2);
		double tangAlpha = deltaY / deltaX;
		double alpha = Math.atan(deltaY / deltaX);
		double _cos = Math.cos(alpha);
		double _sin = Math.sin(alpha);
		
		mA.x = (float) (width - (BH / _cos));
		mA.y = height;
		
		mD.y = (float) (height - (BH / _sin));
		mD.x = width;

		mA.x = Math.max(0,mA.x);
		if(mA.x==0) {
			mOldF.x = mF.x;
			mOldF.y = mF.y;
		}
		
		// Get W
		mE.x = mD.x;
		mE.y = mD.y;
		
		// Correct
		if (mD.y < 0) {
			mD.x = width + (float) (tangAlpha * mD.y);
			mE.y = 0;
			mE.x = width + (float) (Math.tan(2 * alpha) * mD.y);
		}
	}

	/**
	 * Swap between the fore and back-ground.
	 */
	@Deprecated
	private void SwapViews() {
		Bitmap temp = mForeground;
		mForeground = mBackground;
		mBackground = temp;
	}
	
	/**
	 * Swap to next view
	 */
	private void nextView() {
		int foreIndex = mIndex + 1;
		if(foreIndex >= mPages.size()) {
			foreIndex = 0;
		}
		int backIndex = foreIndex + 1;
		if(backIndex >= mPages.size()) {
			backIndex = 0;
		}
		mIndex = foreIndex;
		setViews(foreIndex, backIndex);
	}
	
	/**
	 * Swap to previous view
	 */
	private void previousView() {
		int backIndex = mIndex;
		int foreIndex = backIndex - 1;
		if(foreIndex < 0) {
			foreIndex = mPages.size()-1;
		}
		mIndex = foreIndex;
		setViews(foreIndex, backIndex);
	}
	
	/**
	 * Set current fore and background
	 * @param foreground - Foreground view index
	 * @param background - Background view index
	 */
	private void setViews(int foreground, int background) {
		mForeground = mPages.get(foreground);
		mBackground = mPages.get(background);
	}
	
	//---------------------------------------------------------------
	// Drawing methods
	//---------------------------------------------------------------

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		// Always refresh offsets
		mCurrentLeft = getLeft();
		mCurrentTop = getTop();
		
		// Translate the whole canvas
		//canvas.translate(mCurrentLeft, mCurrentTop);
		
		// We need to initialize all size data when we first draw the view
		if ( !bViewDrawn ) {
			bViewDrawn = true;
			onFirstDrawEvent(canvas);
		}
		
		canvas.drawColor(Color.WHITE);
		
		// Curl pages
		//DoPageCurl();
		
		// TODO: This just scales the views to the current
		// width and height. We should add some logic for:
		//  1) Maintain aspect ratio
		//  2) Uniform scale
		//  3) ...
		Rect rect = new Rect();
		rect.left = 0;
		rect.top = 0;
		rect.bottom = getHeight();
		rect.right = getWidth();
		
		// First Page render
		Paint paint = new Paint();
		
		// Draw our elements
		drawForeground(canvas, rect, paint);
		drawBackground(canvas, rect, paint);	
		drawCurlEdge(canvas);
		
		// Draw any debug info once we are done
		if ( bEnableDebugMode )
			drawDebug(canvas);

		// Check if we can re-enable input
		if ( bEnableInputAfterDraw )
		{
			bBlockTouchInput = false;
			bEnableInputAfterDraw = false;
		}
		
		// Restore canvas
		//canvas.restore();
	}
	
	/**
	 * Called on the first draw event of the view
	 * @param canvas
	 */
	protected void onFirstDrawEvent(Canvas canvas) {
		
		mFlipRadius = getWidth();
		
		ResetClipEdge();
		DoPageCurl();
	}
	
	/**
	 * Draw the foreground
	 * @param canvas
	 * @param rect
	 * @param paint
	 */
	private void drawForeground( Canvas canvas, Rect rect, Paint paint ) {
		canvas.save();
		
		canvas.drawBitmap(mForeground, null, rect, paint);
		
		drawStudyWords(canvas);
		// Draw the page number (first page is 1 in real life :D 
		// there is no page number 0 hehe)
		drawPageNum(canvas, mIndex);
		
		canvas.restore();
	}
	
	/**
	 * Create a Path used as a mask to draw the background page
	 * @return
	 */
	private Path createBackgroundPath() {
		Path path = new Path();
		path.moveTo(mA.x, mA.y);
		path.lineTo(mB.x, mB.y);
		path.lineTo(mC.x, mC.y);
		path.lineTo(mD.x, mD.y);
		path.lineTo(mA.x, mA.y);
		return path;
	}
	
	/**
	 * Draw the background image.
	 * @param canvas
	 * @param rect
	 * @param paint
	 */
	private void drawBackground( Canvas canvas, Rect rect, Paint paint ) {
		Path mask = createBackgroundPath();
		
		// Save current canvas so we do not mess it up
		canvas.save();
		canvas.clipPath(mask);
		canvas.drawBitmap(mBackground, null, rect, paint);
		
		// Draw the page number (first page is 1 in real life :D 
		// there is no page number 0 hehe)
		drawPageNum(canvas, mIndex);
		
		drawStudyInfo(canvas);
		
		canvas.restore();
	}
	
	/**
	 * Creates a path used to draw the curl edge in.
	 * @return
	 */
	private Path createCurlEdgePath() {
		Path path = new Path();
		path.moveTo(mA.x, mA.y);
		path.lineTo(mD.x, mD.y);
		path.lineTo(mE.x, mE.y);
		path.lineTo(mF.x, mF.y);
		path.lineTo(mA.x, mA.y);
		return path;
	}
	
	/**
	 * Draw the curl page edge
	 * @param canvas
	 */
	private void drawCurlEdge( Canvas canvas )
	{
		Path path = createCurlEdgePath();
		canvas.drawPath(path, mCurlEdgePaint);
	}
	
	/**
	 * Draw page num (let this be a bit more custom)
	 * @param canvas
	 * @param pageNum
	 */
	private void drawPageNum(Canvas canvas, int pageNum)
	{
		mTextPaint.setColor(Color.WHITE);
		String pageNumText = "- "+pageNum+" -";
		drawCentered(canvas, pageNumText,canvas.getHeight()-mTextPaint.getTextSize()-5,mTextPaint,mTextPaintShadow);
	}
	
	//---------------------------------------------------------------
	// Debug draw methods
	//---------------------------------------------------------------
	
	/**
	 * Draw a text with a nice shadow
	 */
	public static void drawTextShadowed(Canvas canvas, String text, float x, float y, Paint textPain, Paint shadowPaint) {
    	canvas.drawText(text, x-1, y, shadowPaint);
    	canvas.drawText(text, x, y+1, shadowPaint);
    	canvas.drawText(text, x+1, y, shadowPaint);
    	canvas.drawText(text, x, y-1, shadowPaint);    	
    	canvas.drawText(text, x, y, textPain);
    }
	
	/**
	 * Draw a text with a nice shadow centered in the X axis
	 * @param canvas
	 * @param text
	 * @param y
	 * @param textPain
	 * @param shadowPaint
	 */
	public static void drawCentered(Canvas canvas, String text, float y, Paint textPain, Paint shadowPaint)
	{
		float posx = (canvas.getWidth() - textPain.measureText(text))/2;
		drawTextShadowed(canvas, text, posx, y, textPain, shadowPaint);
	}
	
	/**
	 * Draw debug info
	 * @param canvas
	 */
	private void drawDebug(Canvas canvas)
	{
		float posX = 10;
		float posY = 20;
		
		Paint paint = new Paint();
		paint.setStrokeWidth(5);
		paint.setStyle(Style.STROKE);
		
		paint.setColor(Color.BLACK);		
		canvas.drawCircle(mOrigin.x, mOrigin.y, getWidth(), paint);
		
		paint.setStrokeWidth(3);
		paint.setColor(Color.RED);		
		canvas.drawCircle(mOrigin.x, mOrigin.y, getWidth(), paint);
		
		paint.setStrokeWidth(5);
		paint.setColor(Color.BLACK);
		canvas.drawLine(mOrigin.x, mOrigin.y, mMovement.x, mMovement.y, paint);
		
		paint.setStrokeWidth(3);
		paint.setColor(Color.RED);
		canvas.drawLine(mOrigin.x, mOrigin.y, mMovement.x, mMovement.y, paint);
		
		posY = debugDrawPoint(canvas,"A",mA,Color.RED,posX,posY);
		posY = debugDrawPoint(canvas,"B",mB,Color.GREEN,posX,posY);
		posY = debugDrawPoint(canvas,"C",mC,Color.BLUE,posX,posY);
		posY = debugDrawPoint(canvas,"D",mD,Color.CYAN,posX,posY);
		posY = debugDrawPoint(canvas,"E",mE,Color.YELLOW,posX,posY);
		posY = debugDrawPoint(canvas,"F",mF,Color.LTGRAY,posX,posY);
		posY = debugDrawPoint(canvas,"Mov",mMovement,Color.DKGRAY,posX,posY);
		posY = debugDrawPoint(canvas,"Origin",mOrigin,Color.MAGENTA,posX,posY);
		posY = debugDrawPoint(canvas,"Finger",mFinger,Color.GREEN,posX,posY);
		
		// Draw some curl stuff (Just some test)
		/*
		canvas.save();
		Vector2D center = new Vector2D(getWidth()/2,getHeight()/2);
	    //canvas.rotate(315,center.x,center.y);
	    
	    // Test each lines
		//float radius = mA.distance(mD)/2.f;
	    //float radius = mA.distance(mE)/2.f;
	    float radius = mA.distance(mF)/2.f;
		//float radius = 10;
	    float reduction = 4.f;
		RectF oval = new RectF();
		oval.top = center.y-radius/reduction;
		oval.bottom = center.y+radius/reduction;
		oval.left = center.x-radius;
		oval.right = center.x+radius;
		canvas.drawArc(oval, 0, 360, false, paint);
		canvas.restore();
		/**/
	}
	
	private float debugDrawPoint(Canvas canvas, String name, Vector2D point, int color, float posX, float posY) {	
		return debugDrawPoint(canvas,name+" "+point.toString(),point.x, point.y, color, posX, posY);
	}
	
	private float debugDrawPoint(Canvas canvas, String name, float X, float Y, int color, float posX, float posY) {
		mTextPaint.setColor(color);
		drawTextShadowed(canvas,name,posX , posY, mTextPaint,mTextPaintShadow);
		Paint paint = new Paint();
		paint.setStrokeWidth(5);
		paint.setColor(color);	
		canvas.drawPoint(X, Y, paint);
		return posY+15;
	}

	Typeface tfKorean;
	Typeface tfEnglish;

	///////////LJG//////////////////////////
	public void initStudyView(Context context) {
		m_context = context;
		m_assetMgr = this.getResources().getAssets();

		/*
		m_paintLeftText = new Paint();
		m_paintLeftText.setTextAlign(Align.LEFT);
		m_paintLeftText.setTextSize(35);
		m_paintLeftText.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
		
		m_paintRightText = new Paint();
		m_paintRightText.setTextAlign(Align.RIGHT);
		m_paintRightText.setTextSize(35);
		m_paintRightText.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL));*/
		tfKorean = /*Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);*/Typeface.createFromAsset(context.getAssets(), Define.getMainFont());
		tfEnglish = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);


		m_paintLeftText = new Paint();
		m_paintLeftText.setAntiAlias(true);
		m_paintLeftText.setTextAlign(Align.CENTER);
		m_paintLeftText.setTextSize(28);
		m_paintLeftText.setTypeface(/*Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)*/tfEnglish);

		m_paintPronText = new Paint();
		m_paintPronText.setAntiAlias(true);
		m_paintPronText.setTextAlign(Align.CENTER);
		m_paintPronText.setTextSize(19);
		m_paintPronText.setTypeface(/*Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)*/tfEnglish);

		m_paintRightText = new Paint();
		m_paintRightText.setAntiAlias(true);
		m_paintRightText.setTextAlign(Align.LEFT);
		m_paintRightText.setTextSize(35);
		m_paintRightText.setTypeface(/*Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)*/tfKorean);

		
	}
	
	private void initSound() {
		m_soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		
		AudioManager audioManager = (AudioManager)m_context.getSystemService(Context.AUDIO_SERVICE);
		m_fVolume = /*audioManager.getStreamVolume(3)/*/0.99f;//audioManager.getStreamMaxVolume(3);
		
		m_soundMap = new HashMap<String, Integer>();
		
		for(int i=0; i < m_arrStudyWords.size(); i++)
			loadSound(m_arrStudyWords.get(i).getWord().getSound());
	}
	
	private void loadSound(String strWord) {
		AssetFileDescriptor fd = null;
		try {
			//fd = m_assetMgr.openFd("data/sound/1-" + strWord + Define.SOUND_TYPE);
			PAKDBManager pManager = new PAKDBManager(m_context);
			String soundPath = pManager.getFileName(Integer.parseInt(strWord), 1);
			//String soundPath = Define.SOUND_PATH + strWord + Define.SOUND_TYPE;
			int nSoundId = m_soundPool.load(soundPath , 1);
			m_soundMap.put(strWord, nSoundId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void playWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.play(m_soundMap.get(strWord).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}
	
	private void pauseWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.pause(m_soundMap.get(strWord).intValue());
	}
	
	private void resumeWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.resume(m_soundMap.get(strWord).intValue());
	}
	
	private void drawStudyInfo(Canvas canvas) {
		if(m_nCurrentWordCount > m_arrStudyWords.size()-1)
			return;
		
		drawStudyCurInfo(canvas);		
	}
	
	private void drawStudyWords(Canvas canvas) {
		if(m_nCurrentWordCount > m_arrStudyWords.size()-1)
			return;
		
		drawStudyCurInfo(canvas);		
		
		if(bFlipping || bUserMoves)
			return;
		
		drawStudyWordEn(canvas);
		drawStudyImage(canvas);
		drawStudyWordKr(canvas);
	}
	
	private void drawStudyCurInfo(Canvas canvas) {
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTextSize(18);
		
		String strInfo = Integer.toString(m_nCurrentWordCount+1) + "/" + Integer.toString(m_arrStudyWords.size());
		//canvas.drawText(strInfo, 780.0f, 610.0f, paint);
	}
	
	private void drawStudyWordEn(Canvas canvas) {
		int txtWidth1 = (int)(m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanEn()));
		int txtWidth2 = (int)(m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getPronunciation()));
		int txtWidthBig = txtWidth1 > txtWidth2 ? txtWidth1 : txtWidth2;//(int)(m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn()));
		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(m_bWordEnDraw) {
			if(m_nWordBgColorEn != 0) {
				Paint paint = new Paint();
				paint.setColor(m_nWordBgColorKr);
				paint.setAntiAlias(true);
				paint.setAlpha(255);

				Rect rc = new Rect(EN_WORD_POINT.x-10, (EN_WORD_POINT.y-35), (m_nTextBgRightEn), (EN_WORD_POINT.y+45));
				canvas.drawRect(rc, paint);
			}
			//canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn(), EN_WORD_POINT.x + (txtWidth - txtWidth1) / 2, EN_WORD_POINT.y, m_paintLeftText);
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanEn(), EN_WORD_POINT.x + (txtWidth) / 2, EN_WORD_POINT.y, m_paintLeftText);
			//canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getPronunciation(), EN_PRON_POINT.x + (txtWidth - txtWidth1) / 2, EN_PRON_POINT.y, m_paintPronText);
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getPronunciation(), EN_PRON_POINT.x + (txtWidth) / 2, EN_PRON_POINT.y, m_paintPronText);

		}

		/*if(m_bWordEnDraw) {
			Paint paint = new Paint();
			paint.setColor(m_nWordBgColorEn);
			paint.setAlpha(255);
			Rect rc = new Rect(EN_WORD_POINT.x-10, (EN_WORD_POINT.y-40), (m_nTextBgRightEn), (EN_WORD_POINT.y+10));
			canvas.drawRect(rc, paint);
			
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanEn(), EN_WORD_POINT.x, EN_WORD_POINT.y, m_paintLeftText);
		}*/
	}
	
	private void drawStudyWordKr(Canvas canvas) {

		int txtWidthBig = (int)m_paintRightText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanKr());
		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(m_bWordKrDraw) {
			if(m_nWordBgColorKr != 0) {
				Paint paint = new Paint();
				paint.setColor(m_nWordBgColorKr);
				paint.setAntiAlias(true);
				paint.setAlpha(255);
				//int txtWidth = (int)m_paintRightText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanKr()) + 10;
				Rect rc = new Rect(KR_WORD_POINT.x-txtWidth+5, (KR_WORD_POINT.y-50), (m_nTextBgRightKr), (KR_WORD_POINT.y+30));
				canvas.drawRect(rc, paint);
			}

			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanKr(), KR_WORD_POINT.x -txtWidth + (txtWidth - txtWidthBig) / 2, KR_WORD_POINT.y, m_paintRightText);
		}

/*
		if(m_bWordKrDraw) {
			Paint paint = new Paint();
			paint.setColor(m_nWordBgColorKr);
			paint.setAlpha(255);
			//int txtWidth = (int)m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanKr());
			Rect rc = new Rect(KR_WORD_POINT.x-txtWidth+5, (KR_WORD_POINT.y-40), (m_nTextBgRightKr), (KR_WORD_POINT.y+10));
			canvas.drawRect(rc, paint);
			
			canvas.drawText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanKr(), KR_WORD_POINT.x, KR_WORD_POINT.y, m_paintRightText);
		}*/
	}
	
	private void drawStudyImage(Canvas canvas) {
		if(!m_bWordPicDraw || m_arrStudyBmps.size() < 1)
			return;
		
	    Bitmap img_bitmap = Bitmap.createScaledBitmap( m_arrStudyBmps.get(m_nCurrentWordCount), 500, 320, true );

	    float left = (getWidth()-img_bitmap.getWidth())/2 + 30;
	    float top = (getHeight()-img_bitmap.getHeight())/2 + 30;
	    
	    canvas.drawBitmap(img_bitmap, left, top, new Paint());
	}
	
	public void setWordBgColor(int nColor1, int nColor2) {
		m_nWordBgColorEn = nColor1;
		m_nWordBgColorKr = nColor2;
	}

	public void setStudySpeed(int nSpeed) {
		m_nStudySpeed = nSpeed;
		
		switch(m_nStudySpeed) {
		case 0:
			m_studyTimeList = new int[] {5500, 3000, 10000};
			m_nBgTimeSleep = 80;
			break;
		case 1:
			m_studyTimeList = new int[] {4500, 2000, 7000};
			m_nBgTimeSleep = 60;
			break;
		case 2:
			m_studyTimeList = new int[] {2500, 1500, 5000};
			m_nBgTimeSleep = 40;
			break;
		case 3:
			m_studyTimeList = new int[] {2000, 1000, 2000};
			m_nBgTimeSleep = 20;
			break;
		case 4:
			m_studyTimeList = new int[] {800, 1000, 1000};
			m_nBgTimeSleep = 5;
			break;
		}
	}
	
	public void setWordData(ArrayList<WordpadData> wordData) {
		m_arrStudyWords = wordData;
	
		if(m_arrStudyWords == null)
			return;
		
		try {
			for(int i=0; i<m_arrStudyWords.size(); i++) {
				//String imgPath = Define.IMAGE_PATH + m_arrStudyWords.get(i).getWord().getImage() + ".png";
				PAKDBManager pManager = new PAKDBManager(m_context);
				String imgPath = pManager.getFileName(Integer.parseInt(m_arrStudyWords.get(i).getWord().getImage()), 0);
				FileInputStream fis = new FileInputStream(imgPath);
				Bitmap wordPic = BitmapFactory.decodeStream(fis);
				m_arrStudyBmps.add(wordPic);
				fis.close();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		initSound();
	}
	
	public void finishStudy() {
		if(m_soundPool != null)
			m_soundPool.release();
		m_soundPool = null;
		
		if(m_soundMap != null)
			m_soundMap.clear();
		m_soundMap = null;
		
		m_bStudyPause = false;
		m_bArrowBtnPressed = true;
		m_bStudyStop = true;
		m_bDrawBgStopEn = true;
		m_bDrawBgStopKr = true;
		
		if(m_threadTextBg != null && m_threadTextBg.isAlive()) {
			try{
				m_threadTextBg.join();
			}
			catch(Exception e) {}
		}
		
		if(m_threadStudy != null && m_threadStudy.isAlive()) {
			try{
				m_threadStudy.join();
			}
			catch(Exception e) {}
		}
	}
	
	private void startTextBgDrawEn() {
		//final int txtWidth = (int)m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanEn());

		WordData wData = m_arrStudyWords.get(m_nCurrentWordCount).getWord();

		int txtWidth1 = (int)(m_paintLeftText.measureText(wData.getMeanEn()));
		int txtWidth2 = (int)(m_paintLeftText.measureText(wData.getPronunciation()));

		int txtWidthBig = txtWidth1 > txtWidth2 ? txtWidth1 : txtWidth2;//(int)(m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getMeanEn()));

		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(txtWidth >= 250)
			m_nBgWidthCount = 6;
		else if(txtWidth >= 130 && txtWidth < 250)
			m_nBgWidthCount = 4;
		else
			m_nBgWidthCount = 2;
		
		m_nTextBgRightEn = EN_WORD_POINT.x - 5;
		m_bDrawBgStopEn = false;
		
		m_threadTextBg = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					WordStudyPageCurlView.this.invalidate();
				}
			};
			
			public void run() {
				while(!m_bDrawBgStopEn) {
					pauseThread();
					
					m_nTextBgRightEn += m_nBgWidthCount;
					if(m_nTextBgRightEn >= EN_WORD_POINT.x - 5 + txtWidth + 10) {
						m_nTextBgRightEn = EN_WORD_POINT.x - 5 + txtWidth + 10;
						m_handler.sendEmptyMessage(0);
						m_bDrawBgStopEn = true;
						break;
					}
					
					if(m_bArrowBtnPressed) continue;
					m_handler.sendEmptyMessage(0);
					
					try{
						Thread.sleep(m_nBgTimeSleep);
					}
					catch(Exception e) {}
				}
			}
		};
		m_threadTextBg.setName("TextBgThread");
		m_threadTextBg.start();
	}
	
	private void startTextBgDrawKr() {
		//final int txtWidth = (int)m_paintLeftText.measureText(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getMeanKr());

		WordData wData = m_arrStudyWords.get(m_nCurrentWordCount).getWord();

		int txtWidthBig = (int)m_paintRightText.measureText(wData.getMeanKr());

		final int txtWidth =txtWidthBig > 300 ? txtWidthBig : 300;

		if(txtWidth >= 250)
			m_nBgWidthCount = 6;
		else if(txtWidth >= 130 && txtWidth < 250)
			m_nBgWidthCount = 4;
		else
			m_nBgWidthCount = 2;
		
		m_nTextBgRightKr = KR_WORD_POINT.x - txtWidth + 5;		
		m_bDrawBgStopKr = false;
		
		m_threadTextBg = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					WordStudyPageCurlView.this.invalidate();
				}
			};
			
			public void run() {
				while(!m_bDrawBgStopKr) {
					pauseThread();
					
					m_nTextBgRightKr += m_nBgWidthCount;
					if(m_nTextBgRightKr >= KR_WORD_POINT.x  + 5) {
						m_nTextBgRightKr = KR_WORD_POINT.x  + 5;
						m_handler.sendEmptyMessage(0);
						m_bDrawBgStopKr = true;
						break;
					}
					
					if(m_bArrowBtnPressed) continue;
					m_handler.sendEmptyMessage(0);
					
					try{
						Thread.sleep(m_nBgTimeSleep);
					}
					catch(Exception e) {}
				}
			}
		};
		m_threadTextBg.setName("TextBgThread");
		m_threadTextBg.start();
	}
	
	private void pauseThread() {
		while(m_bStudyPause) {
			try{
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	private void sleepThread(int nTime) {
		int nCount = 0;
		while(!m_bStudyStop) {
			pauseThread();
			
			if(m_bArrowBtnPressed)
				break;
			
			if(nCount * 20 > nTime) 
				break;
			
			nCount ++;
			
			try{
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initStudyDraw() {
		m_bWordEnDraw = false;
		m_bWordKrDraw = false;
		m_bWordPicDraw = false;
	}
	
	private Handler m_handlerStudy = new Handler() {
		public void handleMessage(Message msg) {
			switch(msg.what) {
			case 0:		//ë‹¨ì–´, ì�Œì„±
				m_bWordEnDraw = true;
				startTextBgDrawEn();
				playWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getSound());
				WordStudyPageCurlView.this.invalidate();
				break;
			case 1:		//ê·¸ë¦¼
				m_bWordPicDraw = true;
				WordStudyPageCurlView.this.invalidate();
				break;
			case 2:		//ë‹¨ì–´
				m_bWordKrDraw = true;
				startTextBgDrawKr();
				WordStudyPageCurlView.this.invalidate();
				break;
			case 3:		//ë‹¤ì�Œ ë‹¨ì–´ ì¤€ë¹„
				m_nCurrentWordCount ++;
				if(m_nCurrentWordCount < m_arrStudyWords.size()-1) {
					initStudyDraw();
					WordStudyPageCurlView.this.invalidate();
				}
				break;
			case 5:
				if(m_listener != null) 
					m_listener.onFinishAfter();
				
				break;
			}
			
			
		}
	};
	
	public void startStudy() {
		m_threadStudy = new Thread() {
			public void run() {
				while(!m_bStudyStop) {
					if(m_nCurrentWordCount > m_arrStudyWords.size()-1) { //í•œë‹¨ê³„ í•™ìŠµì�„ í•œíšŒ ì§„í–‰í•˜ì˜€ë‹¤ë©´
						m_nCurrentWordCount = 0;		//ì²« ë‹¨ì–´ë¶€í„° ë‹¤ì‹œ í•™ìŠµ
						m_bStudyStop = true;
						m_handlerStudy.sendEmptyMessage(5);
						break;
					}
					
					sleepThread(0);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(0);		//ë‹¨ì–´í‘œì‹œ
					
					sleepThread(m_studyTimeList[0]);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(1);		//ê·¸ë¦¼í‘œì‹œ
					
					sleepThread(m_studyTimeList[1]);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					m_handlerStudy.sendEmptyMessage(2);		//ë‹¨ì–´í‘œì‹œ
					
					sleepThread(m_studyTimeList[2]);
					if(m_bArrowBtnPressed) {
						m_bArrowBtnPressed = false;
						continue;
					}
					
					m_handlerStudy.sendEmptyMessage(3);		//ë‹¤ì�Œ ë‹¨ì–´ì¤€ë¹„
					
					try{
						Thread.sleep(100);
					}
					catch(Exception e) {}
				}
			}
		};
		m_threadStudy.setDaemon(true);
		m_threadStudy.setName("WordStudyThread");
		m_threadStudy.start();
	}
	
	@SuppressLint("NewApi")
	public void doNext() {
		m_bArrowBtnPressed = true;
		m_bStudyPause = false;
		m_bDrawBgStopEn = true;
		m_bDrawBgStopKr = true;
		
		if(m_soundPool != null)
			m_soundPool.autoPause();
		initStudyDraw();
		
		m_nCurrentWordCount ++;
		if(m_nCurrentWordCount > m_arrStudyWords.size()-1)
			m_nCurrentWordCount = m_arrStudyWords.size()-1;
	}
	
	@SuppressLint("NewApi")
	public void doPrev() {
		m_bArrowBtnPressed = true;
		m_bStudyPause = false;
		m_bDrawBgStopEn = true;
		m_bDrawBgStopKr = true;
		
		if(m_soundPool != null)
			m_soundPool.autoPause();
		initStudyDraw();
		
		m_nCurrentWordCount --;
		if(m_nCurrentWordCount < 0)
			m_nCurrentWordCount = 0;
	}
	
	public void doPause() {
		m_bStudyPause = !m_bStudyPause;
		
		if(m_bStudyPause)
			pauseWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getSound());
		else
			resumeWordVoice(m_arrStudyWords.get(m_nCurrentWordCount).getWord().getSound());
	}
	
	public boolean isPause() {
		return m_bStudyPause;
	}
	
	public void setListener(WordStudyListener listener) {
		m_listener = listener;
	}
	
	public abstract interface WordStudyListener {
		public abstract void onFinishAfter();
	}


	private static final int CURL_LEFT = 1;
	private static final int CURL_NONE = 0;
	private static final int CURL_RIGHT = 2;
	private CurlRenderer mRenderer;

}
