package com.sforeignpad.sekword.ui.words;

import java.util.ArrayList;
import java.util.HashMap;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.WordpadData;
import com.sforeignpad.sekword.utils.ESUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;

public class WordStudyActivity extends Activity {

	public final static String WORD_ID = "word_id";
	public final static String KIND_ID = "kind_id";
	public final static String USER_ID = "user_id";
	public final static String SORT_ID = "sort_id";
	public final static String SORT_ID1 = "sort_id1";
	
	private WordStudyPageCurlView m_vwStudyWord = null;
	
	private Button m_btnBack;
	private Button m_btnPrev;
	private Button m_btnPause;
	private Button m_btnNext;

		
	private int m_nWordBgColorEn = 0;	//ì˜�ì–´ë‹¨ì–´ë°°ê²½ìƒ‰
	private int m_nWordBgColorKr = 0;	//ì¡°ì„ ì–´ë‹¨ì–´ë°°ê²½ìƒ‰
	private int m_nStudySpeed = 0;			//ì„¤ì •ì—�ì„œ ì„ íƒ�í•œ í•™ìŠµì†�ë�„
	
	private ArrayList<WordpadData> mDataList;
	
	private WordpadDBManager mWordPadDbMana;
	private SettingDBManager mSettingDBMana;

	private static WordStudyActivity mThis;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_word_study);

		mThis = this;

		Intent intent = this.getIntent();
		/*if ( intent.getIntExtra(WORD_ID, -1) != -1) {
			mDataList = getDataListByWordId(intent.getIntExtra(WORD_ID, -1));
		} else {
			mDataList = getDataListByKindId(intent.getIntExtra(KIND_ID, -1),
											intent.getIntExtra(USER_ID, -1), 
											intent.getIntExtra(SORT_ID, -1), 
											intent.getIntExtra(SORT_ID1, -1));
		}*/
		
		mDataList = new ArrayList<WordpadData>();
		{
			String strExtra = intent.getStringExtra(WORD_ID);
			String[] strIds = strExtra.split(":");
			
			for(int i = 0; i < strIds.length; i++){
				WordpadData data = getDataByWordId( Integer.parseInt(strIds[i]));
				mDataList.add(data);
			}

		}
		
		
		initView();

		getStudySettingInfo();
		setStudySettingInfo();
		setStudyWordData();
		
		new Handler() {
			public void handleMessage(Message msg) {
				startStudy();
			}
		}.sendEmptyMessageDelayed(0, 200);
	}

	private void initView() {
		m_vwStudyWord = (WordStudyPageCurlView)findViewById(R.id.vwStudyWord);
		m_vwStudyWord.setPageBitmap(R.drawable.word_study_paper);
		m_vwStudyWord.SetCurlSpeed(200);
		m_vwStudyWord.SetInitialEdgeOffset(0);
		m_vwStudyWord.SetUpdateRate(20);
		m_vwStudyWord.setListener(new WordStudyPageCurlView.WordStudyListener() {
			
			@Override
			public void onFinishAfter() {
				// TODO Auto-generated method stub
				WordStudyActivity.this.finish();
			}
		});
		
		m_btnBack = (Button)findViewById(R.id.btnBack);
		m_btnBack.setOnClickListener(m_clickListener);
		
		m_btnPrev = (Button)findViewById(R.id.btnPrevWord);
		m_btnPrev.setOnClickListener(m_clickListener);
		
		m_btnPause = (Button)findViewById(R.id.btnPause);
		m_btnPause.setOnClickListener(m_clickListener);
		
		m_btnNext = (Button)findViewById(R.id.btnNextWord);
		m_btnNext.setOnClickListener(m_clickListener);
	}
	
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			ESUtils.playBtnSound(WordStudyActivity.this);

			switch(v.getId()) {
			case R.id.btnBack:
				WordStudyActivity.this.finish();
				break;
			case R.id.btnPrevWord:
				m_vwStudyWord.doPrev();
				m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
				break;
			case R.id.btnNextWord:
				m_vwStudyWord.doNext();
				m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
				break;
			case R.id.btnPause:
				m_vwStudyWord.doPause();
				
				if(m_vwStudyWord.isPause()) 
					m_btnPause.setBackgroundResource(R.drawable.btn_study_play_play);
				else
					m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
				
				break;
			}
		}
	};
	

	public static void playWordVoice(String strVoice) {
		if(strVoice.contains("page")){
			ESUtils.playPageSound(mThis);
		}
	}
	
	private ArrayList<WordpadData> getDataListByWordId(int word_id) {
		return (openDB()) ? mWordPadDbMana.getWordListByWordId(word_id) : new ArrayList<WordpadData>();
	}
	
	private WordpadData getDataByWordId(int word_id){
		return (openDB()) ? mWordPadDbMana.getWordByWordId(word_id) : new WordpadData();
	}
	
	private ArrayList<WordpadData> getDataListByKindId(int kind_id, int uid, int sort, int sort1) {
		if (openDB()) {
			if (sort1 == 0)
				return mWordPadDbMana.getWordListBySort1(uid, sort, kind_id);
			else
				return mWordPadDbMana.getWordListBySort2(uid, kind_id);
			}
		return new ArrayList<WordpadData>();
	}
	
	private void getStudySettingInfo() {
		m_nWordBgColorEn = mSettingDBMana.getWordColorValue();
		m_nWordBgColorKr = mSettingDBMana.getMeanColorValue();
		
		m_nStudySpeed = mSettingDBMana.getStudySpeedIndex();
	}
	
	private boolean openDB() {
		if (mWordPadDbMana == null)
			mWordPadDbMana = new WordpadDBManager(this);
		
		if (mSettingDBMana == null) 
			mSettingDBMana = new SettingDBManager(this);
		
		if (mWordPadDbMana == null) 
			return false;
		
		if (mSettingDBMana == null) 
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mWordPadDbMana != null) {
			mWordPadDbMana.close();
			mWordPadDbMana = null;
		}
		
		if (mSettingDBMana != null) {
			mSettingDBMana.close();
			mSettingDBMana = null;
		}
	}
	
	private void Exit() {
		m_vwStudyWord.finishStudy();
		this.closeDB();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Exit();
		super.onDestroy();
	}
	
	private void setStudySettingInfo() {
		m_vwStudyWord.setStudySpeed(m_nStudySpeed);
		m_vwStudyWord.setWordBgColor(m_nWordBgColorEn, m_nWordBgColorKr);
	}
	
	private void setStudyWordData() {
		m_vwStudyWord.setWordData(mDataList);
	}
	
	private void startStudy() {
		m_vwStudyWord.startStudy();
	}
}
