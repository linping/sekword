package com.sforeignpad.sekword.ui.studding;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

public class StudyLauncherPopup extends Dialog implements View.OnClickListener {
	
	private Context mContext;
	private Button mBtnOK;
	private Button mBtnCancel;
	
	private OnStudyLauncherListener mListener;

	public StudyLauncherPopup(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.study_launcher_popup, null);
		this.setContentView(localView);
		
		mBtnOK = (Button)localView.findViewById(R.id.btnOk);
		mBtnOK.setOnClickListener(this);
		
		mBtnCancel = (Button)localView.findViewById(R.id.btnCancel);
		mBtnCancel.setOnClickListener(this);
	}
	
	public void setListener(OnStudyLauncherListener listener) {
		mListener = listener;
	}
	
	@Override
	public void dismiss() {
	    super.dismiss();
	}

	@Override
	public void onClick(View arg0) {
		ESUtils.playBtnSound(mContext);

		// TODO Auto-generated method stub
		switch (arg0.getId()) {
			case R.id.btnOk:
				mListener.onOK();
				this.dismiss();
				break;
				
			case R.id.btnCancel:
				this.dismiss();
				break;
		}
	}
	
	public abstract interface OnStudyLauncherListener {
		public abstract void onOK();
	}

}
