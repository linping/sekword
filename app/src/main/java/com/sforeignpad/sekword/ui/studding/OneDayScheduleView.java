package com.sforeignpad.sekword.ui.studding;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.ui.studding.study.StudyWordActivity;
import com.sforeignpad.sekword.utils.ESUtils;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class OneDayScheduleView extends FrameLayout {

	private Context m_context = null;
	
	private ImageView m_ivMedal = null;
	private Button m_btnPlayStudy = null;
	private ImageView m_ivKey = null;
	private ScoreStar m_scoreStar = null;
	private TextView m_tvDate = null;
	
	private int m_nScheduleState = Define.SCHEDULE_NO;
	
	private int m_nUserId = -1;
	private ScheduleDateData m_scheduleData = null;
	private ScheduleDateData m_prevData = null;

	public OneDayScheduleView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public OneDayScheduleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		initView(context);
	}

	public OneDayScheduleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		initView(context);
	}

	private void initView(Context context) {
		m_context = context;
		
		View v = LayoutInflater.from(context).inflate(R.layout.one_day_schedule_layout, null);
		this.addView(v);
		
		m_ivMedal = (ImageView)this.findViewById(R.id.ivMedal);
		m_btnPlayStudy = (Button)this.findViewById(R.id.btnPlayDayStudy);
		m_btnPlayStudy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				goStudyActivity();
			}
		});
		m_ivKey = (ImageView)this.findViewById(R.id.ivKey);
		m_tvDate = (TextView)this.findViewById(R.id.tvDate);
		m_scoreStar = (ScoreStar)this.findViewById(R.id.scorestar);

		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvDate, Define.getMainFont());
	}
	
	public void setMedalState(int nState) {
		int	idImg = R.drawable.medal2;
		switch(nState) {
		case Define.MEDAL_BAD:
			idImg = R.drawable.medal2;
			break;
		case Define.MEDAL_NORMAL:
			idImg = R.drawable.medal3;
			break;
		case Define.MEDAL_GOOD:
			idImg = R.drawable.medal4;
			break;
		case Define.MEDAL_EXCELLENT:
			idImg = R.drawable.medal5;
			break;
		}
		m_ivMedal.setImageResource(idImg);
	}
	
	public void setScheduleState(int nState) {
		m_nScheduleState = nState;
		
		m_ivMedal.setVisibility(View.GONE);
		m_btnPlayStudy.setVisibility(View.GONE);
		m_ivKey.setVisibility(View.GONE);
		m_scoreStar.initStar();
		switch(nState) {
		case Define.SCHEDULE_NO:
			m_ivKey.setVisibility(View.VISIBLE);
			break;
		case Define.SCHEDULE_STUDY:
			m_btnPlayStudy.setVisibility(View.VISIBLE);
			break;
		case Define.SCHEDULE_MEDAL:
			m_ivMedal.setVisibility(View.VISIBLE);
			m_btnPlayStudy.setVisibility(View.VISIBLE);
			break;
		}
	}
	
	public int getScheduleState() {
		return m_nScheduleState;
	}
	public void setDate(String strDate) {
		m_tvDate.setText(strDate);
	}
	
	public String getDate() {
		return m_tvDate.getText().toString();
	}
	
	private void setStudyPlayStateSchedule() {
		if(m_scheduleData == null) {
			setScheduleState(Define.SCHEDULE_NO);
		}
		else {
			if(m_scheduleData.getMarks() < 70) {	//학습하지 않은 날이라면
				//현재 날자와 같다면 학습할 상태로 설정
				//아니라면 lock상태
				if(ESUtils.isSameDate(m_scheduleData.getDate(), ESUtils.getNowDateMilliseconds()))
					setScheduleState(Define.SCHEDULE_STUDY);
				else {
					setScheduleState(Define.SCHEDULE_NO);	//lock상태
				}
				//m_scoreStar.setScore(0);

			}
			else {	//학습한 날이라면 메달상태 표시
				int nMedal = ESUtils.getMedalType(m_scheduleData.getMarks());
				setScheduleState(Define.SCHEDULE_MEDAL);
				setMedalState(nMedal);
				
				m_scoreStar.setScore(m_scheduleData.getMarks());
			}
		}
		
		setDate(Integer.toString(ESUtils.getDateOfDay(m_scheduleData.getDate())));
	}
	
	public void setUserId(int nId) {
		m_nUserId = nId;
	}
	
	public void setScheduleData(ScheduleDateData data) {
		m_scheduleData = data;		
		setStudyPlayStateSchedule();
	}

	public void setPrevScheduleData(ScheduleDateData data){
		m_prevData = data;
	}

	public ScheduleDateData getScheduleData() {
		return m_scheduleData;
	}
	
	/*private void goStudyActivity() {
		Intent intent = new Intent(m_context, StudyActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.SCHEDULE_ID, m_scheduleData.getId());
		m_context.startActivity(intent);
	}*/
	
	//Test
	private void goStudyActivity() {
		/*Intent intent = new Intent(m_context, StudyWordActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.SCHEDULE_ID, m_scheduleData.getId());
		m_context.startActivity(intent);*/
/*
		Intent intent = new Intent(m_context, StudyTestActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.SCHEDULE_ID, m_scheduleData.getId());
		m_context.startActivity(intent);
*/
		if(m_prevData != null)
		{
			Intent intent = new Intent(m_context, StudyExerciseActivity.class);
			intent.putExtra(Define.USER_ID, m_nUserId);
			intent.putExtra(Define.SCHEDULE_ID, m_prevData.getId());
			intent.putExtra(Define.STUDY_SCHEDULE_ID , m_scheduleData.getId());
			m_context.startActivity(intent);
		} else {
			Intent intent = new Intent(m_context, StudyWordActivity.class);
			intent.putExtra(Define.USER_ID, m_nUserId);
			intent.putExtra(Define.SCHEDULE_ID, m_scheduleData.getId());
			m_context.startActivity(intent);
		}


	}
}
