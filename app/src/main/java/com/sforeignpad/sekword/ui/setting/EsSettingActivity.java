package com.sforeignpad.sekword.ui.setting;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.ui.studding.SekWordPopupOneButton;
import com.sforeignpad.sekword.ui.studding.SekWordPopupOneButton.OnSEKLaunchListener;
import com.sforeignpad.sekword.utils.ESUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class EsSettingActivity extends Activity {
	
	private String[] mStudy ;
	private String[] mColor ;
	private String[] mTestTime;
	private String[] mStudySpeed ;
	
	private int mUId;
	
	private SettingPopup1 settingPopup1 = null;
	private SettingPopup2 settingPopup2 = null;
	private SettingPopup3 settingPopup3 = null;
	private SettingPopup4 settingPopup4 = null;
	private SettingPopup5 settingPopup5 = null;
	private SettingPopup6 settingPopup6 = null;
	private SettingPopup7 settingPopup7 = null;
	
	private Button btnSetting1;
	private Button btnSetting2;
	private Button btnSetting3;
	private Button btnSetting4;
	private Button btnSetting5;
	private Button btnSetting6;
	private Button btnSetting7;


	private TextView textSettingTitle1;
	private TextView textSettingTitle2;
	private TextView textSettingTitle3;
	private TextView textSettingTitle4;
	private TextView textSettingTitle5;
	private TextView textSettingTitle6;
	private TextView textSettingTitle7;

	private TextView textSettingContent1;
	private TextView textSettingContent2;
	private TextView textSettingContent3;
	private TextView textSettingContent4;
	private TextView textSettingContent5;
	private TextView textSettingContent7;

	private UserDBManager mUserDbMana = null;
	private SettingDBManager mDbMana = null;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_setting);
		
		Intent intent = this.getIntent();
		mUId = intent.getIntExtra(Define.USER_ID, -1);
		
		if (openDB()) {
			int value = (mUId!=-1) ? mUserDbMana.getIsUseSettingHelp(mUId) : 1;
			if (value == 1) {
				/*new AlertDialog.Builder(this)
				.setTitle(R.string.msgTitle)
				.setMessage(R.string.settingMsg)
				.setCancelable(false)
				.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
				
					public void onClick(DialogInterface dialog, int which) {	
						if (mUId!=-1)
							mUserDbMana.setNoUseSettingHelp(mUId);
					}
				})
				.show();*/
				/*SekWordPopupOneButton popup;
					
				popup = new SekWordPopupOneButton(EsSettingActivity.this);
				popup.setMessage(this.getString(R.string.settingMsg));
				popup.setMessageSize(15);
				popup.setListener(new OnSEKLaunchListener() {

					@Override
					public void onOK() {
						if (mUId!=-1)
							mUserDbMana.setNoUseSettingHelp(mUId);
					}
				});
				popup.show();*/
			}
		}
		
		
		initViews();
	} 
	
	private void initViews() {
		initVariables();
    	initButtons();
    	setButtonInfo();
    }
	
	private void initVariables() {
		mStudy = new String[]{ this.getString(R.string.setting_study1), 
				this.getString(R.string.setting_study2), 
				this.getString(R.string.setting_study3), 
				this.getString(R.string.setting_study4), 
				this.getString(R.string.setting_study5)};
		
		mColor = new String[]{ this.getString(R.string.setting_color1), 
				this.getString(R.string.setting_color2), 
				this.getString(R.string.setting_color3), 
				this.getString(R.string.setting_color4), 
				this.getString(R.string.setting_color5)};
		
		mTestTime = new String[]{this.getString(R.string.setting_time1), 
				  this.getString(R.string.setting_time2), 
				  this.getString(R.string.setting_time3)};
		
		mStudySpeed = new String[]{this.getString(R.string.setting_speed1), 
					this.getString(R.string.setting_speed2), 
					this.getString(R.string.setting_speed3), 
					this.getString(R.string.setting_speed4), 
					this.getString(R.string.setting_speed5)};
	}
	
	private void initButtons() { 
		
		Button btnExit = (Button)findViewById(R.id.btnExit);
    	btnExit.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				EsSettingActivity.this.finish();
			}
		});
    	
		btnSetting1 = (Button)findViewById(R.id.btnSetting1);
		btnSetting1.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				// TODO Auto-generated method stub
				if (settingPopup1 == null)
					settingPopup1 = new SettingPopup1(EsSettingActivity.this);
				settingPopup1.initValues();
				settingPopup1.setOkListener(new SettingPopup1.OnOkListener() {					
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopup1.show();
			}
		});
		
		
		btnSetting2 = (Button)findViewById(R.id.btnSetting2);
		btnSetting2.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				// TODO Auto-generated method stub
				if (settingPopup2 == null)
					settingPopup2 = new SettingPopup2(EsSettingActivity.this);
				settingPopup2.initValues();
				settingPopup2.setOkListener(new SettingPopup2.OnOkListener() {					
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopup2.show();
			}
		});
		
		btnSetting3 = (Button)findViewById(R.id.btnSetting3);
		btnSetting3.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(EsSettingActivity.this);

				// TODO Auto-generated method stub
				if (settingPopup3 == null)
					settingPopup3 = new SettingPopup3(EsSettingActivity.this);
				settingPopup3.initValues();
				settingPopup3.setOkListener(new SettingPopup3.OnOkListener() {					
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopup3.show();
			}
		});
		
		btnSetting4 = (Button)findViewById(R.id.btnSetting4);
		btnSetting4.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopup4 == null)
					settingPopup4 = new SettingPopup4(EsSettingActivity.this);
				settingPopup4.initValues();
				settingPopup4.setOkListener(new SettingPopup4.OnOkListener() {					
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopup4.show();
			}
		});
		
		btnSetting5 = (Button)findViewById(R.id.btnSetting5);
		btnSetting5.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopup5 == null)
					settingPopup5 = new SettingPopup5(EsSettingActivity.this);
				settingPopup5.initValues();
				settingPopup5.setOkListener(new SettingPopup5.OnOkListener() {					
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopup5.show();
			}
		});
		
		btnSetting6 = (Button)findViewById(R.id.btnSetting6);
		btnSetting6.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopup6 == null)
					settingPopup6 = new SettingPopup6(EsSettingActivity.this);
				settingPopup6.initValues();
				settingPopup6.show();
			}
		});
		
		btnSetting7 = (Button)findViewById(R.id.btnSetting7);
		btnSetting7.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(EsSettingActivity.this);

				if (settingPopup7 == null)
					settingPopup7 = new SettingPopup7(EsSettingActivity.this);
				settingPopup7.initValues();
				settingPopup7.setOkListener(new SettingPopup7.OnOkListener() {					
					@Override
					public void onOK() {
						setButtonInfo();
					}
				});
				settingPopup7.show();
			}
		});


		textSettingTitle1 = (TextView) findViewById(R.id.textSetting1_Title);
		textSettingTitle2 = (TextView) findViewById(R.id.textSetting2_Title);
		textSettingTitle3 = (TextView) findViewById(R.id.textSetting3_Title);
		textSettingTitle4 = (TextView) findViewById(R.id.textSetting4_Title);
		textSettingTitle5 = (TextView) findViewById(R.id.textSetting5_Title);
		textSettingTitle6 = (TextView) findViewById(R.id.textSetting6_Title);
		textSettingTitle7 = (TextView) findViewById(R.id.textSetting7_Title);

		textSettingContent1 = (TextView) findViewById(R.id.textSetting1_Content);
		textSettingContent2 = (TextView) findViewById(R.id.textSetting2_Content);
		textSettingContent3 = (TextView) findViewById(R.id.textSetting3_Content);
		textSettingContent4 = (TextView) findViewById(R.id.textSetting4_Content);
		textSettingContent5 = (TextView) findViewById(R.id.textSetting5_Content);
		textSettingContent7 = (TextView) findViewById(R.id.textSetting7_Content);

		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle1, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle2, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle3, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle4, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle5, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle6, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingTitle7, Define.getMainFont());

		ESUtils.setTextViewTypeFaceByRes(this, textSettingContent1, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingContent2, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingContent3, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingContent4, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingContent5, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, textSettingContent7, Define.getMainFont());


		/*
		ESUtils.setButtonTypeFaceByRes(this, btnSetting1, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnSetting2, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnSetting3, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnSetting4, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnSetting5, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnSetting6, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnSetting7, Define.getMainFont());*/


	}
	
	private void setButtonInfo() {
		if (openDB()) {
			/*
			btnSetting1.setText(this.getString(R.string.btnsetting_pref1)+mStudy[mDbMana.getStudy1Index()]);
			btnSetting2.setText(this.getString(R.string.btnsetting_pref2)+mStudy[mDbMana.getStudy2Index()]);
			btnSetting3.setText(this.getString(R.string.btnsetting_pref3)+mColor[mDbMana.getWordColorIndex()]);
			btnSetting4.setText(this.getString(R.string.btnsetting_pref4)+mColor[mDbMana.getMeanColorIndex()]);
			btnSetting5.setText(this.getString(R.string.btnsetting_pref5)+mTestTime[mDbMana.getTestTimeIndex()]);
			btnSetting7.setText(this.getString(R.string.btnsetting_pref7)+mStudySpeed[mDbMana.getStudySpeedIndex()]);
			*/
			textSettingContent1.setText(mStudy[mDbMana.getStudy1Index()]);
			textSettingContent2.setText(mStudy[mDbMana.getStudy2Index()]);
			textSettingContent3.setText(mColor[mDbMana.getWordColorIndex()]);
			textSettingContent4.setText(mColor[mDbMana.getMeanColorIndex()]);
			textSettingContent5.setText(mTestTime[mDbMana.getTestTimeIndex()]);
			textSettingContent7.setText(mStudySpeed[mDbMana.getStudySpeedIndex()]);
		}
	}
	
	private boolean openDB() {
		if (mDbMana == null)
			mDbMana = new SettingDBManager(this);
		
		if (mDbMana == null)
			return false;
		
		if (mUserDbMana == null)
			mUserDbMana = new UserDBManager(this);
		
		if (mUserDbMana == null)
			return false;
		
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
		
		if (mUserDbMana != null) {
			mUserDbMana.close();
			mUserDbMana = null;
		}
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		this.closeDB();		
		super.onDestroy();
	}
}
