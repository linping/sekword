package com.sforeignpad.sekword.ui.studding.test;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.ScheduleDBManager;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.StudyDBManager;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.ui.studding.SekWordPopup;
import com.sforeignpad.sekword.ui.studding.SekWordPopup.OnSEKLaunchListener;
import com.sforeignpad.sekword.utils.ESUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class StudyLevelTestActivity extends Activity {

	private FrameLayout m_layoutFirstStart;
	private FrameLayout m_layoutParent;
	
	private TestKorToEngLayout m_layoutKorEngTest;
	private TestEngToKorLayout m_layoutEngKorTest;
	private TestEngVoiceLayout m_layoutEngVoiceTest;
	private TestEngTypingLayout m_layoutEngTypingTest;
	private TestResultChartLayout m_layoutTestResultChart;
	private TestResultMedalLayout m_layoutTestResultMedal;
	private LevelTestResultLayout m_layoutLevelTest;
	private LevelTestResultFinalLayout m_layoutLevelTestFinal;

	private int m_nTrueResCount = 0;
	private int m_nFalseResCount = 0;
	
	private int m_nTestLimitTime = 7;
	private int m_nTestLimitTime_typing = 14;
	private int m_nCurrentTestProblemCount = 0;
	private int m_nTotalTestProblemCount = 0;
	
	protected int m_nTestCurrentProblemIndex = -1;	//단계별 전체 문제개수
	protected Boolean[] is_successProblem = new Boolean[Define.LevelTestProblemCounts];
	
	private SettingDBManager mSettingDbMana = null;
	private StudyDBManager m_studyDbMana = null;
	private ScheduleDBManager m_scheduleDbMana = null;
	private WordpadDBManager m_wordpadDbMana = null;
	private UserDBManager m_userDbMana = null;
	private ScheduleDateData m_scheduleData = null;
	
	private int m_nUserId = -1;
	private int m_nScheduleId = -1;
	
	private boolean m_bIsTestOrder = false;
	private int[] m_arrTestCase = {1, 1, 1, 1, 1};
	private int m_nTestPhase = -1;
	
	
	protected ArrayList<WordData> m_arrAllWordData = new ArrayList<WordData>();	//전체시험문제
	protected WordData[] m_arrAllWordDataArray = new WordData[40];

	private static StudyLevelTestActivity mThis;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_study_leveltest);

		mThis = this;

		Intent intent = getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);
		//m_bIsTestOrder = intent.getBooleanExtra(Define.IS_ORDERTEST, false);
		m_bIsTestOrder = true;
		
		if(!isConnectDB()){
			finish();
			return;
		}	

		m_nTotalTestProblemCount = Define.LevelTestProblemCounts;
		
		getTestTimeData();
		
		initTab();
		initLayouts();
		//initStartButton();

		performKorEngTest();

	}
	
	private void finishActivity() {		
		disableAllTestLayout();
		closeDB();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		doBack();
		//super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishActivity();
		
		super.onDestroy();
		System.gc();
	}

	private void initLayouts() {
		
		m_layoutParent = (FrameLayout)findViewById(R.id.TestParentLayout);
		m_layoutFirstStart = (FrameLayout)findViewById(R.id.FirstStartLayout);
	}
	
	private void initTab() {
		
		Button btnBack = (Button)findViewById(R.id.ivTabBackOn);

		btnBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(StudyLevelTestActivity.this);

				doBack();
			}
		});
		
		Button btnTest = (Button)findViewById(R.id.ivTabTestOn);

		ESUtils.setButtonTypeFaceByRes(this, btnBack, Define.getMainFont());
		ESUtils.setButtonTypeFaceByRes(this, btnTest, Define.getMainFont());

		if(m_bIsTestOrder){
			btnTest.setBackgroundResource(R.drawable.tab_red_on);
			btnTest.setText(R.string.string_study_test);
		}else
		{
			btnTest.setBackgroundResource(R.drawable.tab_red_on);
			btnTest.setText(R.string.string_test);
		}
	}
	
	/*private void initStartButton() {
		Button btnStartTest = (Button)findViewById(R.id.btnTestStart);
		btnStartTest.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ESUtils.playBtnSound(StudyLevelTestActivity.this);

				m_nTestPhase = 0;
				if(!m_bIsTestOrder)
					m_studyDbMana.setFormatTestTmpTable();
				performTest();
			}
		});
	}*/
	
	private void doBack() {
		if(m_nTestPhase >= 0 && m_nTestPhase < 4) { //í˜„ìž¬ ì‹œí—˜ì¤‘ì�´ë�¼ë©´
			switch(m_nTestPhase) {
			case 0:
				m_layoutKorEngTest.setTestPause(true);
				break;
			case 1:
				m_layoutKorEngTest.setTestPause(true);
				break;
			case 2:
				m_layoutEngVoiceTest.setTestPause(true);
				break;
			case 3:
				m_layoutEngTypingTest.setTestPause(true);
				break;
			}
			showTestStopDlg();
		}
		else {
			if(!m_bIsTestOrder)
				sendScheduleInfo(false, m_scheduleDbMana.getLevel(m_nScheduleId));
			
			StudyLevelTestActivity.this.finish();
		}
	}


	public static void playWordVoice(String strVoice) {

		if(strVoice.contains("success")){
			ESUtils.playSuccessSound(mThis);
		}else if(strVoice.contains("fail")){
			ESUtils.playFailSound(mThis);
		}
	}
	
	//DBì ‘ì†�
	private boolean isConnectDB() {
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;
		
		m_scheduleDbMana = new ScheduleDBManager(this, m_nUserId);
		if(m_scheduleDbMana == null)
			return false;
		
		mSettingDbMana = new SettingDBManager(this);
		if(mSettingDbMana == null)
			return false;
		
		m_wordpadDbMana = new WordpadDBManager(this, m_nUserId);
		if(m_wordpadDbMana == null)
			return false;
		
		m_userDbMana = new UserDBManager(this);
		if(m_userDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
		
		if (m_scheduleDbMana != null) {
			m_scheduleDbMana.close();
			m_scheduleDbMana = null;
		}
		
		if (mSettingDbMana != null) {
			mSettingDbMana.close();
			mSettingDbMana = null;
		}
		
		if(m_wordpadDbMana != null) {
			m_wordpadDbMana.close();
			m_wordpadDbMana = null;
		}
		
		if(m_userDbMana != null) {
			m_userDbMana.close();
			m_userDbMana = null;
		}
	}
	
	private void getTestTimeData() {
		m_nTestLimitTime = mSettingDbMana.getTestTimeValue();
	}
	
	private void getScheduleData() {
		m_scheduleData = m_scheduleDbMana.getScheduleDateData(m_nScheduleId);
	}
		
	private int getTestCaseSchedule() {
		String strMode = mSettingDbMana.getTestModeValue();
		String[] strTemp = strMode.split(",");
		String strPhase = "";
		
		for(int i=0; i<4; i++) { 
			m_arrTestCase[i] = Integer.valueOf(strTemp[i]);
			if(m_arrTestCase[i] == 1)
				strPhase += "1";
		}
		
		return strPhase.length() * 10;
	}
	
	private void disableAllTestLayout() {
		m_layoutFirstStart.setVisibility(View.GONE);
		m_layoutParent.removeAllViews();
		
		if(m_layoutKorEngTest != null) {
			m_layoutKorEngTest.finishTest();	
			m_layoutKorEngTest = null;
		}
		
		if(m_layoutEngKorTest != null) {
			m_layoutEngKorTest.finishTest();	
			m_layoutEngKorTest = null;
		}
		
		if(m_layoutEngVoiceTest != null) {
			m_layoutEngVoiceTest.finishTest();	
			m_layoutEngVoiceTest = null;
		}
		
		if(m_layoutEngTypingTest != null) {
			m_layoutEngTypingTest.finishTest();	
			m_layoutEngTypingTest = null;
		}
		
		if(m_layoutTestResultChart != null) {
			m_layoutTestResultChart = null;
		}
		
		if(m_layoutTestResultMedal != null) {
			m_layoutTestResultMedal = null;
		}
	}
	
	private void performTest() {
		disableAllTestLayout();
		
		for(int i=m_nTestPhase; i<m_arrTestCase.length; i++) 
			if(m_arrTestCase[m_nTestPhase] == 0) {
				m_nTestPhase ++;
				if(m_nTestPhase == m_arrTestCase.length-1)
					break;
			}
			else
				break;
		
		switch(m_nTestPhase) {
		case 0:
			performKorEngTest();
			break;
		case 1:
			performEngKorTest();
			break;
		case 2:
			performEngVoiceTest();
			break;
		case 3:
			performEngTypingTest();
			break;
		case 4:
			if(m_bIsTestOrder)
				displayChartResult();
			else
				displayResultMedal();
			break;
		}
	}

	private void displayLevelTestResult(){
		m_layoutLevelTest = new LevelTestResultLayout(this);
		m_layoutLevelTest.setListener(new LevelTestResultLayout.OnLevelTestResultFinishListner() {

			@Override
			public void onAfter(int nStartLevel, int nEndLevel) {
				// TODO Auto-generated method stub
				//performTest();

				performSecondTest(nStartLevel, nEndLevel);

			};
		});
		m_layoutParent.addView(m_layoutLevelTest);

		initTestLayouts(m_layoutLevelTest);
		//initWordData(m_layoutLevelTest);
		m_layoutLevelTest.setAllTestWordData(m_arrAllWordData);
		m_layoutLevelTest.initData();
	}

	private void displayLevelTestFinalResult(){
		m_layoutLevelTestFinal = new LevelTestResultFinalLayout(this);
		m_layoutLevelTestFinal.setListener(new LevelTestResultFinalLayout.OnLevelTestResultFinishListner() {

			@Override
			public void onAfter(int nOrder) {
				// TODO Auto-generated method stub
				//performTest();

				//performSecondTest(nStartLevel, nEndLevel);
				sendScheduleInfo(true, nOrder + 1);
				sendStudyComplete();
				StudyLevelTestActivity.this.finish();

			};
		});
		m_layoutParent.addView(m_layoutLevelTestFinal);

		initTestLayouts(m_layoutLevelTestFinal);
		//initWordData(m_layoutLevelTest);
		m_layoutLevelTestFinal.setAllTestWordData(m_arrAllWordData);
		m_layoutLevelTestFinal.initData(gStartLevel, gEndLevel);
	}

	int gStartLevel = 0 ;
	int gEndLevel = 0;

	private void performSecondTest(int nStartLevel, int nEndLevel){
		m_layoutKorEngTest = new TestKorToEngLayout(this);
		m_layoutKorEngTest.setListener(new TestKorToEngLayout.OnKorEngTestFinishListener() {

			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;


				//m_arrAllWordData =m_arrAllWordData_;
				m_nTestPhase ++;
				//performTest();

				displayLevelTestFinalResult();

			};
		});
		m_layoutKorEngTest.setStepInfo(2);
		m_layoutParent.addView(m_layoutKorEngTest);

		initTestLayouts(m_layoutKorEngTest);
		initSecondWordData(m_layoutKorEngTest, nStartLevel, nEndLevel);

		gStartLevel = nStartLevel;
		gEndLevel = nEndLevel;

		m_layoutKorEngTest.setAllTestWordData(m_arrAllWordData);
		m_layoutKorEngTest.performKorEngTest();
	}
	private void performKorEngTest() {
		m_layoutKorEngTest = new TestKorToEngLayout(this);
		m_layoutKorEngTest.setListener(new TestKorToEngLayout.OnKorEngTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;


				//m_arrAllWordData =m_arrAllWordData_;
				m_nTestPhase ++;
				//performTest();
				displayLevelTestResult();

			};
		});

		m_layoutKorEngTest.setStepInfo(1);
		m_layoutParent.addView(m_layoutKorEngTest);
		
		initTestLayouts(m_layoutKorEngTest);
		initWordData(m_layoutKorEngTest);

		m_layoutKorEngTest.setAllTestWordData(m_arrAllWordData);
		m_layoutKorEngTest.performKorEngTest();
	}
	
	//ì˜�ì–´-ì¡°ì„ ì–´ì‹œí—˜ ì§„í–‰
	private void performEngKorTest() {
		m_layoutEngKorTest = new TestEngToKorLayout(this);
		m_layoutEngKorTest.setListener(new TestEngToKorLayout.OnEngKorTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;
				
//				ArrayList<WordData> m_arrAllWordData__ = new ArrayList<WordData>();
//				m_arrAllWordData__ = m_arrAllWordData_;
				m_arrAllWordData = m_arrAllWordData_;

				m_nTestPhase ++;
				performTest();
			}
		});
		

		
		
		m_layoutParent.addView(m_layoutEngKorTest);
	
		//í•´ë‹¹í•œ ìž�ë£Œ ì„¤ì •
		initTestLayouts(m_layoutEngKorTest);
		initWordData(m_layoutEngKorTest);
		m_layoutEngKorTest.setAllTestWordData(m_arrAllWordData);

		m_layoutEngKorTest.performEngKorTest();
	}
	
	//ì˜�ì–´ë°œì�Œ - ì¡°ì„ ì–´ ì‹œí—˜ ì§„í–‰
	private void performEngVoiceTest() {
		m_layoutEngVoiceTest = new TestEngVoiceLayout(this);
		m_layoutEngVoiceTest.setListener(new TestEngVoiceLayout.OnEngVoiceTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;
				
//				ArrayList<WordData> m_arrAllWordData__ = new ArrayList<WordData>();
//				m_arrAllWordData__ = m_arrAllWordData_;
				m_arrAllWordData =m_arrAllWordData_;

				m_nTestPhase ++;
				performTest();
			}
		});
		

		
		m_layoutParent.addView(m_layoutEngVoiceTest);
	
		
		//í•´ë‹¹í•œ ìž�ë£Œ ì„¤ì •
		initTestLayouts(m_layoutEngVoiceTest);	
		initWordData(m_layoutEngVoiceTest);
		m_layoutEngVoiceTest.setAllTestWordData(m_arrAllWordData);
		m_layoutEngVoiceTest.performEngVoiceTest();
	}
	
	//ì˜�ì–´ì² ìž�ì‹œí—˜ ì§„í–‰
	private void performEngTypingTest() {
		m_layoutEngTypingTest = new TestEngTypingLayout(this);
		m_layoutEngTypingTest.setListener(new TestEngTypingLayout.OnEngTypingTestFinishListener() {
			
			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;
				m_nTestPhase ++;
				performTest();
			}
		});
		

		
		m_layoutParent.addView(m_layoutEngTypingTest);
	
		initTestLayouts(m_layoutEngTypingTest);
		initWordData(m_layoutEngTypingTest);
		m_layoutEngTypingTest.setAllTestWordData(m_arrAllWordData);
		m_layoutEngTypingTest.setTestTimeLimit(m_nTestLimitTime_typing);


		m_layoutEngTypingTest.performEngTypingTest();
	}
	
	//ì‹œí—˜ê²°ê³¼ë¥¼ ë�„í‘œë¡œ ë³´ì—¬ì¤€ë‹¤
	private void displayChartResult() {
		m_layoutTestResultChart = new TestResultChartLayout(this);
		m_layoutTestResultChart.setListener(new TestResultChartLayout.OnTestResultChartFinishListener() {
			
			@Override
			public void onAfter(int nOrder) {
				// TODO Auto-generated method stub
				//ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
				sendScheduleInfo(true, nOrder);
				sendStudyComplete();
				StudyLevelTestActivity.this.finish();
			}
		});
		m_layoutParent.addView(m_layoutTestResultChart);
		
		initTestLayouts(m_layoutTestResultChart);		
		m_layoutTestResultChart.setUserDbMana(m_userDbMana);
		m_layoutTestResultChart.setUserId(m_nUserId);
		m_layoutTestResultChart.setResultChartLayout();
	}
	
	//ì‹œí—˜ê²°ê³¼ë¥¼ ë©”ë‹¬ë¡œ ë³´ì—¬ì¤€ë‹¤
	private void displayResultMedal() {
		m_layoutTestResultMedal = new TestResultMedalLayout(this);
		m_layoutTestResultMedal.setListener(new TestResultMedalLayout.OnTestResultMedalFinishListener() {
			
			@Override
			public void onAfter(int nOrder) {
				// TODO Auto-generated method stub
				//ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
				sendScheduleInfo(false, nOrder);
				StudyLevelTestActivity.this.finish();
			}
		});
		m_layoutParent.addView(m_layoutTestResultMedal);
		
		initTestLayouts(m_layoutTestResultMedal);		
		m_layoutTestResultMedal.setTestResultInfo();
	}
	
	private void initTestLayouts(TestLayout layout) {
		layout.setDbMana(m_studyDbMana, m_scheduleDbMana, m_wordpadDbMana);
		layout.setScheduleData(m_scheduleData);
		layout.setTestTimeLimit(m_nTestLimitTime);
		layout.setCurrentProblemCount(0);
		layout.setTrueFalseWordCount(0, 0);
		layout.setCurrentProblemIndex(0);
		layout.setIs_successProblem(is_successProblem);
		layout.setIsTestOrder(m_bIsTestOrder);
		layout.setTotalProblemCount(m_nTotalTestProblemCount);
		layout.setScheduleId(m_nScheduleId);
		
	}
	
	private void initWordData(TestLayout layout) {
		ArrayList<WordData> m_arrAllWordData_ = new ArrayList<WordData>();
		m_arrAllWordData_ = layout.initWordData();
		this.m_arrAllWordData=m_arrAllWordData_;
	}

	private void initSecondWordData(TestLayout layout, int nStartLevel, int nEndLevel) {
		ArrayList<WordData> m_arrAllWordData_ = new ArrayList<WordData>();
		m_arrAllWordData_ = layout.initSecondWordData(nStartLevel, nEndLevel);
		this.m_arrAllWordData=m_arrAllWordData_;
	}

	private void showTestStopDlg() {
		/*new AlertDialog.Builder(this)
		.setTitle(getString(R.string.test_stop))
		.setCancelable(false)
		.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {	
				setTestPause(false);
				disableAllTestLayout();
				
				m_nTestPhase = -1;
				m_layoutFirstStart.setVisibility(View.VISIBLE);
				
				m_nTrueResCount = 0;	//í•©ê²©í•œ ê°œìˆ˜
				m_nFalseResCount = 0;	//í‹€ë¦° ê°œìˆ˜					
				m_nCurrentTestProblemCount = 0;	//ë¬¸ì œì œì‹œìˆ˜
			}
		})
		.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				setTestPause(false);
				dialog.cancel();
			}
		})
		.show();*/

		SekWordPopup popup;

		popup = new SekWordPopup(StudyLevelTestActivity.this);
		popup.setMessage(getString(R.string.test_stop));
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				setTestPause(false);
				disableAllTestLayout();
				
				m_nTestPhase = -1;
				m_layoutFirstStart.setVisibility(View.VISIBLE);
				
				m_nTrueResCount = 0;	//í•©ê²©í•œ ê°œìˆ˜
				m_nFalseResCount = 0;	//í‹€ë¦° ê°œìˆ˜					
				m_nCurrentTestProblemCount = 0;	//ë¬¸ì œì œì‹œìˆ˜

				StudyLevelTestActivity.this.finish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				setTestPause(false);
			}

		});
		
		popup.show();


	}
	
	private void setTestPause(boolean bPause) {
		switch(m_nTestPhase) {
		case 0:
			m_layoutKorEngTest.setTestPause(bPause);
			break;
		case 1:
			m_layoutEngKorTest.setTestPause(bPause);
			break;
		case 2:
			m_layoutEngVoiceTest.setTestPause(bPause);
			break;
		case 3:
			m_layoutEngTypingTest.setTestPause(bPause);
			break;
		}
	}

	//ì�¼ì •ë³´ê¸°í™”ë©´ìœ¼ë¡œ ì�´í–‰
	private void sendScheduleInfo(boolean bIsOrderTest, int nOrder) {
		Intent intent = new Intent(Define.SCHEDULE_VIEW_ACTION);
		intent.putExtra(Define.STUDY_ORDER, nOrder);
		sendBroadcast(intent);
	}
	
	private void sendStudyComplete() {
		Intent intent = new Intent(Define.USER_ORDER_ACTION);
		this.sendBroadcast(intent);
	}
}
