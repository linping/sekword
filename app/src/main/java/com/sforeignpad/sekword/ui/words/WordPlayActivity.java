package com.sforeignpad.sekword.ui.words;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.PAKDBManager;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.structs.WordpadData;
import com.sforeignpad.sekword.ui.studding.study.StudyWordTextView;
import com.sforeignpad.sekword.utils.ESUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
 
public class WordPlayActivity extends Activity implements View.OnClickListener, View.OnTouchListener {
		
	public final static String WORD_ID = "word_id";   // ë‹¨ì–´ID
	public final static String KIND_ID = "kind_id";   // 0:ì „ì²´ë‹¨ì–´, 1:ì¤‘ìš”ë‹¨ì–´ 2:í‹€ë¦°ë‹¨ì–´ 
	public final static String USER_ID = "user_id";   // ì‚¬ìš©ìž�ID
	public final static String SORT_ID = "sort_id";   // 0:ì˜�ì¡° 1:ì˜�ì–´ 2:ì¡°ì„ ì–´
	public final static String SORT_ID1 = "sort_id1"; // 0:ì² ìž�ìˆœ, 1:ë“±ë¡�ìˆœ
	
	private final int SHOW_WORD_EN = 0;
	private final int SHOW_WORD_KR = 1;
	private final int SHOW_WORD_IMG = 2;
	private final int INIT_VIEW = 3;
	private final int SHOW_EXIT = 4;
	
	private StudyWordTextView mWordEn;
	private StudyWordTextView mWordKr;
	private ImageView mContentPic;
	private Button mPreBtn;
	private Button mPauseBtn;
	private Button mNextBtn;
	private Button mExitBtn;
	
	private int mCurrentIndex;
	private int[] mStudyTime;
	private ArrayList<WordpadData> mDataList;
	
	private WordpadDBManager mWordPadDbMana;
	private SettingDBManager mSettingDBMana;
	
	private float mVolume;
	private HashMap<String, Integer> mSoundMap;
	private SoundPool mSoundPool;
	
	private boolean mIsExit;
	private boolean mIsPlaying;	
	
	private Handler mHandlerStudy;
	private Thread mThreadStudy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_word_play);
		
		Intent intent = this.getIntent();
		if ( intent.getIntExtra(WORD_ID, -1) != -1) {
			mDataList = getDataListByWordId(intent.getIntExtra(WORD_ID, -1));
		} else {
			mDataList = getDataListByKindId(intent.getIntExtra(KIND_ID, -1),
											intent.getIntExtra(USER_ID, -1), 
											intent.getIntExtra(SORT_ID, -1), 
											intent.getIntExtra(SORT_ID1, -1));
		}
		
		initViews();
		initSettingVariables();
		initSound();		
		initStudyHandler();
		
		mCurrentIndex = 0;
		startStudy();
	}
	
	private void initViews() {
		mWordEn = (StudyWordTextView)findViewById(R.id.txtWordEn);
		mWordKr = (StudyWordTextView)findViewById(R.id.txtWordKr);
		mContentPic = (ImageView)findViewById(R.id.imgContentPic);
		
		mWordEn.setText("");
		mWordEn.setDirect(true);
		mWordEn.init();
		
		mWordKr.setText("");
		mWordKr.setDirect(true);
		mWordKr.init();
		
		initButtons();
	}
	
	private void initButtons() {
		mPreBtn = (Button)findViewById(R.id.btnPrev);
		mPauseBtn = (Button)findViewById(R.id.btnPause);
		mNextBtn = (Button)findViewById(R.id.btnNext);
		mExitBtn = (Button)findViewById(R.id.btnBack);
		
		mPreBtn.setOnClickListener(this);
		mPauseBtn.setOnClickListener(this);
		mNextBtn.setOnClickListener(this);
		mExitBtn.setOnClickListener(this);
	}
	
	private void initSettingVariables() {
		if (openDB()) {
			int mEnColor = mSettingDBMana.getWordColorValue();
			int mKrColor = mSettingDBMana.getMeanColorValue();
			
			
			if (mEnColor != 0) {
				int rColor = ESUtils.getRedColor(mEnColor);
				int gColor = ESUtils.getGreenColor(mEnColor);
				int bColor = ESUtils.getBlueColor(mEnColor);
				mWordEn.setColorOfView(rColor, gColor, bColor);
			}
			
			if (mKrColor != 0) {
				int rColor = ESUtils.getRedColor(mKrColor);
				int gColor = ESUtils.getGreenColor(mKrColor);
				int bColor = ESUtils.getBlueColor(mKrColor);
				mWordKr.setColorOfView(rColor, gColor, bColor);
			}
			
			int mStudySpeed = mSettingDBMana.getStudySpeedIndex();
			mWordEn.setAnimSpeed(mStudySpeed);
			mWordKr.setAnimSpeed(mStudySpeed);
		}
	}
	
	private void initSound() {
		mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		
		AudioManager audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
		mVolume = 0.99f; //audioManager.getStreamVolume(3)/audioManager.getStreamMaxVolume(3);
		
		mSoundMap = new HashMap<String, Integer>();
		for (int i=0; i<mDataList.size(); i++) {
			try {
				WordData item = (WordData)mDataList.get(i).getWord();
				//AssetFileDescriptor fd = this.getAssets().openFd("data/sound/1-" + item.getSound() + Define.SOUND_TYPE);
				//String soundPath = Define.SOUND_PATH + item.getSound() + Define.SOUND_TYPE;


				PAKDBManager pManager = new PAKDBManager(this);
				String soundPath = pManager.getFileName(Integer.parseInt(item.getSound()), 1);

				mSoundMap.put(item.getSound(), mSoundPool.load(soundPath , 1));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void initStudy() {
		mWordEn.setVisibility(View.GONE);
		mWordEn.setText("");
		mWordKr.setVisibility(View.GONE);
		mWordKr.setText("");
		
		mWordEn.init();
		mWordEn.finishThread();
		mWordKr.init();
		mWordKr.finishThread();
		
		mContentPic.setImageDrawable(null);
		
		int m_nStudySpeed = mSettingDBMana.getStudySpeedIndex();
		
		switch(m_nStudySpeed) {
			case 0:
				mStudyTime = new int[] {5500, 3000, 10000};
				break;
			case 1:
				mStudyTime = new int[] {4500, 2000, 7000};
				break;
			case 2:
				mStudyTime = new int[] {2500, 1500, 5000};
				break;
			case 3:
				mStudyTime = new int[] {2000, 1000, 2000};
				break;
			case 4:
				mStudyTime = new int[] {800, 800, 800};
				break;
		}
	}
	
	@SuppressLint("HandlerLeak")
	private void initStudyHandler() {
		mHandlerStudy = new Handler() {
			public void handleMessage(Message msg) {
				WordData item = (WordData)mDataList.get(mCurrentIndex).getWord();
				switch(msg.what) {
				
					case SHOW_WORD_EN:
						
						mWordEn.setVisibility(View.VISIBLE);
						mWordEn.setText(item.getMeanEn());
						mWordEn.startAnimBg();
						mSoundPool.play(mSoundMap.get(item.getSound()).intValue(), mVolume, mVolume, 1, 0, 1.0F);
						break;
						
					case SHOW_WORD_KR:
						
						mWordKr.setVisibility(View.VISIBLE);
						mWordKr.setText(item.getMeanKr());	
						mWordKr.startAnimBg();
						break;
						
					case SHOW_WORD_IMG:
						try {

							//String imgPath =Define.IMAGE_PATH + item.getImage() + ".png";
							PAKDBManager pManager = new PAKDBManager(WordPlayActivity.this);
							String imgPath = pManager.getFileName(Integer.parseInt(item.getImage()), 0);

							FileInputStream fis = new FileInputStream(imgPath);
							Bitmap wordPic = BitmapFactory.decodeStream(fis);
							mContentPic.setImageBitmap(wordPic);
							fis.close();
						} catch(Exception e) {
							e.printStackTrace();
						}
						break;
						
					case INIT_VIEW:
						initStudy();
						break;
						
					case SHOW_EXIT:
						mIsPlaying = false;
						mIsExit = true;
						mPauseBtn.setBackgroundResource(R.drawable.btn_study_play_right);
						break;
				}
			}
		};
	}
	
	private ArrayList<WordpadData> getDataListByWordId(int word_id) {
		return (openDB()) ? mWordPadDbMana.getWordListByWordId(word_id) : new ArrayList<WordpadData>();
	}
	
	private ArrayList<WordpadData> getDataListByKindId(int kind_id, int uid, int sort, int sort1) {
		if (openDB()) {
			if (sort1 == 0)
				return mWordPadDbMana.getWordListBySort1(uid, sort, kind_id);
			else
				return mWordPadDbMana.getWordListBySort2(uid, kind_id);
			}
		return new ArrayList<WordpadData>();
	}
	
	private void startStudy() {
		if (mDataList.size() > 0) {
			
			mIsExit = false;
			mIsPlaying = true;
			
			this.mThreadStudy = new Thread() {
				public void run() {
					while(!mIsExit) {
						// ì´ˆê¸°í™”
						mHandlerStudy.sendEmptyMessage(INIT_VIEW);
						if(sleepThread(200)) continue;
						
						// ì˜�ì–´ë‹¨ì–´í‘œì‹œ
						if(sleepThread(mStudyTime[0])) continue;
						if(mHandlerStudy != null) mHandlerStudy.sendEmptyMessage(SHOW_WORD_EN);
						
						// ê·¸ë¦¼í‘œì‹œ
						if(sleepThread(mStudyTime[1])) continue;
						if(mHandlerStudy != null) mHandlerStudy.sendEmptyMessage(SHOW_WORD_IMG);
						
						// ì¡°ì„ ì–´í‘œì‹œ
						if(sleepThread(1400)) continue;
						if(mHandlerStudy != null) mHandlerStudy.sendEmptyMessage(SHOW_WORD_KR);
						
						// ì‹œê°„ì§€ì—°
						//if(sleepThread(1500)) continue;
						
						if (!mIsExit) {
							if (mCurrentIndex < mDataList.size()-1) {
								mCurrentIndex++;
							} else {
								break;
							}
						}
					}
					
					if(mHandlerStudy != null) mHandlerStudy.sendEmptyMessage(SHOW_EXIT);
				}
			};
			
			this.mThreadStudy.setName("kkkk");
			this.mThreadStudy.setDaemon(true);
			this.mThreadStudy.start();
		}
	}
	
	private boolean sleepThread(int nMillSeconds) {
		int nCount = 0;
		boolean result = false;
		
		while(true) {
			if (mIsExit) 
				break;
			
			if(mIsPlaying && (nCount*200) >= nMillSeconds) 
				break;
			
			try {
				Thread.sleep(200);
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			nCount ++;
		}
		
		return result;
	}
	
	private void Pause() {
		WordData item = (WordData)mDataList.get(mCurrentIndex).getWord();
		mIsPlaying = !mIsPlaying;
		
		if (mIsExit) {
			mPauseBtn.setBackgroundResource(R.drawable.btn_study_play_pause);
			closeThread();
			startStudy();
		}else if (!mIsPlaying) {
			mPauseBtn.setBackgroundResource(R.drawable.btn_study_play_right);
			mSoundPool.pause(mSoundMap.get(item.getSound()).intValue());
			
			if(!mWordEn.getAnimState())	mWordEn.setPauseAnim();
			if(!mWordKr.getAnimState())	mWordKr.setPauseAnim();
		} else {
			mPauseBtn.setBackgroundResource(R.drawable.btn_study_play_pause);
			mSoundPool.resume(mSoundMap.get(item.getSound()).intValue());
			
			if(!mWordEn.getAnimState())	mWordEn.setResumeAnim();
			if(!mWordKr.getAnimState())	mWordKr.setResumeAnim();
		}
	}
	
	private void Prev() {
		if (mCurrentIndex > 0) {
			this.closeThread();
			this.closeHandler();
			
			this.mCurrentIndex --;
			this.initStudyHandler();
			this.startStudy();
		}
	}
	
	private void Next() {
		if (mCurrentIndex < mDataList.size()-1) {
			this.closeThread();
			this.closeHandler();
			
			this.mCurrentIndex ++;
			this.initStudyHandler();
			this.startStudy();
		}
	}
	
	private void Exit() {
		this.closeAnimation();
		this.closeThread();
		this.closeHandler();
		this.closeDB();
	}
	
	private boolean openDB() {
		if (mWordPadDbMana == null)
			mWordPadDbMana = new WordpadDBManager(this);
		
		if (mSettingDBMana == null) 
			mSettingDBMana = new SettingDBManager(this);
		
		if (mWordPadDbMana == null) 
			return false;
		
		if (mSettingDBMana == null) 
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mWordPadDbMana != null) {
			mWordPadDbMana.close();
			mWordPadDbMana = null;
		}
		
		if (mSettingDBMana != null) {
			mSettingDBMana.close();
			mSettingDBMana = null;
		}
	}
	
	private void closeThread() {
		if(this.mThreadStudy.isAlive()) {
			try { 
				this.mIsExit = true;
				
				WordData item = (WordData)mDataList.get(mCurrentIndex).getWord();
				mSoundPool.pause(mSoundMap.get(item.getSound()).intValue());
				mSoundPool.stop(mSoundMap.get(item.getSound()).intValue());
				
				this.mThreadStudy.join();
			} catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void closeAnimation() {
		if(!mWordEn.getAnimState()) mWordEn.finishThread();
		if(!mWordKr.getAnimState()) mWordKr.finishThread();
	}
	
	private void closeHandler() {
		if (this.mHandlerStudy != null) {
			this.mHandlerStudy.removeMessages(SHOW_WORD_EN);
			this.mHandlerStudy.removeMessages(SHOW_WORD_IMG);
			this.mHandlerStudy.removeMessages(SHOW_WORD_KR);
			this.mHandlerStudy.removeMessages(INIT_VIEW);
		}
		this.mHandlerStudy = null;
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClick(View v) {
		ESUtils.playBtnSound(this);

		// TODO Auto-generated method stub
		switch(v.getId()) {
		
			case R.id.btnBack:
				this.Exit();
				this.finish();
				break;
				
			case R.id.btnPrev:
				this.Prev();
				break;
				
			case R.id.btnPause:
				this.Pause();
				break;
				
			case R.id.btnNext:
				this.Next();
				break;
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		this.Exit();		
		super.onDestroy();
	}

}
