package com.sforeignpad.sekword.ui.studding.test;

import java.util.ArrayList;
import java.util.HashMap;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.PAKDBManager;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.utils.ESUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TestEngVoiceLayout extends TestLayout {

	private Context m_context;
	
	private TextView m_tvEngProblem;	//ì¡°ì„ ì–´ë¬¸ì œ
	private TextView m_tvTrueWords;		//ë§žížŒ ë‹¨ì–´ìˆ˜
	private TextView m_tvFalseWords;	//í‹€ë¦° ë‹¨ì–´ìˆ˜
	private TextView m_tvTotalWords;	//ì „ì²´ ë‹¨ì–´ìˆ˜
	boolean is_enable_thread = false;
	boolean is_message_enabled = false;

	private TextView tvengvoicekortest_title1;	
	private TextView tvengvoicekortest_title2;		
	private TextView tvengvoicekortest_description;
	
	private ImageView[] m_arrIvTrueFalseMsg = new ImageView[4];		//í•©ê²© ë¶ˆí•©ê²©
	private ImageView[] m_arrIvWordNum = new ImageView[4];			//ê²°ê³¼ë‹¨ì–´ ìˆœì„œë²ˆí˜¸
	private Button[] m_arrBtnResult = new Button[4];				//ê²°ê³¼ë‹¨ì–´
	private TestProgressBar m_progressbar = null;					//ProgressBar
	
	SoundPool m_soundPool = null;
	HashMap<String, Integer> m_soundMap;
	float m_fVolume = 0.0f;
	
	private OnEngVoiceTestFinishListener m_listener = null;
	
	private String[] m_strEngRes = new String[4];
	private int m_nTrueIndex = -1;
	
	public TestEngVoiceLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		initLayout();		
		initKorEngTestLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.engvoicekortest_layout, null);
		this.addView(v);
	}
	
	private void initSound() {
		m_soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);		
		
		AudioManager audioManager = (AudioManager)m_context.getSystemService(Context.AUDIO_SERVICE);
		m_fVolume = 0.99f;/*audioManager.getStreamVolume(3)/*///audioManager.getStreamMaxVolume(3);
		
		m_soundMap = new HashMap<String, Integer>();
		
		for(int i=0; i < m_arrWordData.size(); i++)
			loadSound(m_arrWordData.get(i).getSound());
	}
	
	private void loadSound(String strWord) {
		try {
			//String soundPath = Define.SOUND_PATH + strWord + Define.SOUND_TYPE;

			PAKDBManager pManager = new PAKDBManager(m_context);
			String soundPath = pManager.getFileName(Integer.parseInt(strWord), 1);

			int nSoundId = m_soundPool.load(soundPath , 1);
			m_soundMap.put(strWord, nSoundId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}


	private void playWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.play(m_soundMap.get(strWord).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);
	}
	
	private void pauseWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.pause(m_soundMap.get(strWord).intValue());
	}
	
	private void resumeWordVoice(String strWord) {
		if(m_soundPool != null)
			m_soundPool.resume(m_soundMap.get(strWord).intValue());
	}
	
	private void initKorEngTestLayout() {
		
		//Typeface textTypeFace_gulim = Typeface.createFromAsset(getContext().getAssets(), Define.TEXTFONT_GULIM_PATH);
		
		tvengvoicekortest_title1 = (TextView)findViewById(R.id.tvengvoicekortest_title1);
		tvengvoicekortest_title2 = (TextView)findViewById(R.id.tvengvoicekortest_title2);
		tvengvoicekortest_description = (TextView)findViewById(R.id.tvengvoicekortest_description);
		
		/*tvengvoicekortest_title1.setTypeface(textTypeFace_gulim);
		tvengvoicekortest_title2.setTypeface(textTypeFace_gulim);
		tvengvoicekortest_description.setTypeface(textTypeFace_gulim);*/
		
		m_tvEngProblem = (TextView)findViewById(R.id.tvKorProblem);
		m_tvTrueWords = (TextView)findViewById(R.id.tvTrueWords);
		m_tvFalseWords = (TextView)findViewById(R.id.tvFalseWords);
		m_tvTotalWords = (TextView)findViewById(R.id.tvTotalWords);
		
		ImageView ivTrueFalseMsg1 = (ImageView)findViewById(R.id.ivTrueFalseMsg1);
		ivTrueFalseMsg1.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg2 = (ImageView)findViewById(R.id.ivTrueFalseMsg2);
		ivTrueFalseMsg2.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg3 = (ImageView)findViewById(R.id.ivTrueFalseMsg3);
		ivTrueFalseMsg3.setVisibility(View.GONE);
		ImageView ivTrueFalseMsg4 = (ImageView)findViewById(R.id.ivTrueFalseMsg4);
		ivTrueFalseMsg4.setVisibility(View.GONE);
		m_progressbar = (TestProgressBar)findViewById(R.id.progressbar);
		m_progressbar.setMax(1000);

		m_arrIvTrueFalseMsg[0] = ivTrueFalseMsg1;
		m_arrIvTrueFalseMsg[1] = ivTrueFalseMsg2;
		m_arrIvTrueFalseMsg[2] = ivTrueFalseMsg3;
		m_arrIvTrueFalseMsg[3] = ivTrueFalseMsg4;
		
		ImageView ivWordNum1 = (ImageView)findViewById(R.id.ivTestWordNum1);
		ImageView ivWordNum2 = (ImageView)findViewById(R.id.ivTestWordNum2);
		ImageView ivWordNum3 = (ImageView)findViewById(R.id.ivTestWordNum3);
		ImageView ivWordNum4 = (ImageView)findViewById(R.id.ivTestWordNum4);
		
		m_arrIvWordNum[0] = ivWordNum1;
		m_arrIvWordNum[1] = ivWordNum2;
		m_arrIvWordNum[2] = ivWordNum3;
		m_arrIvWordNum[3] = ivWordNum4;
		
		Button btnResultEngKor1 = (Button)findViewById(R.id.btnTestKorWord1);
		btnResultEngKor1.setOnClickListener(m_clickListener);
		
		Button btnResultEngKor2 = (Button)findViewById(R.id.btnTestKorWord2);
		btnResultEngKor2.setOnClickListener(m_clickListener);
		
		Button btnResultEngKor3 = (Button)findViewById(R.id.btnTestKorWord3);
		btnResultEngKor3.setOnClickListener(m_clickListener);
		
		Button btnResultEngKor4 = (Button)findViewById(R.id.btnTestKorWord4);
		btnResultEngKor4.setOnClickListener(m_clickListener);
		
		m_arrBtnResult[0] = btnResultEngKor1;
		m_arrBtnResult[1] = btnResultEngKor2;
		m_arrBtnResult[2] = btnResultEngKor3;
		m_arrBtnResult[3] = btnResultEngKor4;

		ESUtils.setTextViewTypeFaceByRes(m_context, tvengvoicekortest_title1, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, tvengvoicekortest_title2, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, tvengvoicekortest_description, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvEngProblem, Define.getEnglishFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvTrueWords, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvFalseWords, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvTotalWords, Define.getMainFont());

		for(int i =0; i<4; i++){
			ESUtils.setTextViewTypeFaceByRes(m_context, m_arrBtnResult[i], Define.getMainFont());
		}
	}
	
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//ESUtils.playBtnSound(m_context);

			// TODO Auto-generated method stub
			if(m_bResultPressed || !m_bProblemView) return;
			
			switch(v.getId()) {
			case R.id.btnTestKorWord1:
				checkResult(1);
				break;
			case R.id.btnTestKorWord2:
				checkResult(2);
				break;
			case R.id.btnTestKorWord3:
				checkResult(3);
				break;
			case R.id.btnTestKorWord4:
				checkResult(4);
				break;
			}
		}
	};
	
	@SuppressLint("HandlerLeak")
	private void checkResult(int nIndex) {
		m_bResultPressed = true;	
		
		m_arrIvTrueFalseMsg[nIndex-1].setVisibility(View.VISIBLE);
		
		if(isTrueWord(nIndex-1)) {	//í•©ê²©
			is_successProblem[m_nCurrentTestProblemCount-1] = true;

			m_nTrueResCount ++;
			m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.test_word_true);
			m_arrIvTrueFalseMsg[nIndex-1].setSelected(true);
			m_arrIvWordNum[nIndex-1].setImageResource(R.drawable.true_num1 + nIndex-1);
			StudyTestActivity.playWordVoice("success");
		}
		else {	//ë¶ˆí•©ê²©
			is_successProblem[m_nCurrentTestProblemCount-1] = false;

			m_nFalseResCount ++;
			m_arrBtnResult[nIndex-1].setBackgroundResource(R.drawable.test_word_false);
			m_arrIvTrueFalseMsg[nIndex-1].setSelected(false);
			StudyTestActivity.playWordVoice("fail");
			animSelectTrueWord();
			
			//í‹€ë¦° ë‹¨ì–´ ë‹¨ì–´ìž¥ ì¶”ê°€
			if(m_nTestCount != 0)
				insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());
		}
		
		setTestResult();		
		
		new Handler() {
			public void handleMessage(Message msg) {
				m_nTestTime = 0;
				m_bResultPressed = false;
				m_bProblemView = false;
			}
		}.sendEmptyMessageDelayed(0, 1200);
		
	}
	
	//ì˜³ì�€ ë‹¨ì–´ ì§€ì • ì• ë‹ˆë©”ì�´ì…˜
	private void animSelectTrueWord() {
		Thread thread = new Thread() {
			private boolean m_bVisible = false;
			private int m_nCount = 0;
			private boolean m_bStop = false;
			
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_nTrueIndex == -1) return;
					
					m_bVisible = !m_bVisible;
					if(m_bVisible) {						
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.VISIBLE);
						m_nCount ++;
					}
					else
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.INVISIBLE);
					
					if(m_nCount > 4) {
						m_bStop = true;
						m_arrIvTrueFalseMsg[m_nTrueIndex].setVisibility(View.VISIBLE);
					}
				}
			};
			
			public void run() {
				while(!m_bStop && !m_bTestStop) {
					try{
						Thread.sleep(150);
					}
					catch(Exception e) {}
					
					m_nCount ++;
					m_handler.sendEmptyMessage(0);
				}
			}
		};
		
		if(m_nTrueIndex != -1) m_arrIvTrueFalseMsg[m_nTrueIndex].setImageResource(R.drawable.true_msg2);
		
		thread.start();
	}
		
	//ì§€ì •í•œ ë‹¨ì–´ê°€ ë§žëŠ”ê°€ ê²€ì‚¬
	private boolean isTrueWord(int nResIndex) {
		boolean bTrue = false;
		
		if(nResIndex == m_nTrueIndex)
			bTrue = true;
			
		return bTrue;
	}
	
	//ì‹œí—˜ê²°ê³¼ í˜„ì‹œ
	private void setTestResult() {
		
		m_nTestCurrentProblemIndex++;
		m_tvTrueWords.setText(Integer.toString(m_nTrueResCount));
		m_tvFalseWords.setText(Integer.toString(m_nFalseResCount));
		m_tvTotalWords.setText(Integer.toString(m_nCurrentTestProblemCount) + "/" 
								+ Integer.toString(m_nTotalTestProblemCount));
	}
	
	//ì‹œí—˜ì „ ì´ˆê¸°í™”
	private void setTestInit() {
		for(int i=0; i<4; i++) {
			m_arrIvTrueFalseMsg[i].setVisibility(View.GONE);
			m_arrIvTrueFalseMsg[i].setImageResource(R.drawable.word_true_false);
			m_arrIvWordNum[i].setImageResource(R.drawable.false_num1 + i);
			m_arrBtnResult[i].setBackgroundResource(R.drawable.test_word_false);
		}
	}
	
	protected void makeResWords() {
		////ì˜³ì�€ ë‹µì�˜ ë‹¨ì¶”ìˆœì„œ
		ArrayList<Integer> arrResIndex = new ArrayList<Integer>();
		arrResIndex.add(-1);
		
		int nRes = ESUtils.getRandomValue(4, arrResIndex);

		int nWordIndex;
		if(m_nCurrentTestProblemCount < m_arrWordData.size()){
			nWordIndex = m_nCurrentTestProblemCount;
		}else{
			nWordIndex = m_nTestCount;
		}

		m_strEngRes[nRes] = m_arrWordData.get(nWordIndex).getMeanKr();
		arrResIndex.add(nRes);
		
		m_nTrueIndex = nRes;
		
		ArrayList<Integer> arrIndex = new ArrayList<Integer>();
		arrIndex.add(nWordIndex);
		
		for(int i=0; i<3; i++) {
			int nWrong = ESUtils.getRandomValue(m_arrWordData.size(), arrIndex);	//í‹€ë¦° ë‹µì�˜ ì�¸ë�±ìŠ¤
			nRes = ESUtils.getRandomValue(4, arrResIndex);	//í‹€ë¦° ë‹µì�˜ ë‹¨ì¶”ìˆœì„œ
			
			m_strEngRes[nRes] = m_arrWordData.get(nWrong).getMeanKr();
			
			arrResIndex.add(nRes);
			arrIndex.add(nWrong);
		}
		
		m_tvEngProblem.setText(m_arrWordData.get(nWordIndex).getMeanEn());
		
		for(int i=0; i<4; i++)
		{		
			String text_ = getadjustedText(m_strEngRes[i]);
			m_arrBtnResult[i].setText(text_);
		}
	}

	public String getadjustedText(String textstring){
		String temp_string1 = "";
		String temp_string2 = "";
		if (textstring.length()>20) {
			temp_string2 = textstring.substring(0, 20);
			textstring    =textstring.replace(temp_string2, temp_string2+"\n");
			temp_string1 = textstring.substring(0, 10);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n");
			textstring = temp_string2;
		}else if (textstring.length()>10) {
			temp_string1 = textstring.substring(0, 10);
			temp_string2 = textstring.replace(temp_string1, temp_string1+"\n");
			textstring = temp_string2;
		}
		return textstring;
	}

	@SuppressLint("HandlerLeak")
	public void performEngVoiceTest() {
		m_threadTest = new Thread() {
			private Handler m_handler = new Handler() {
				public void handleMessage(Message msg) {
					if(m_arrWordData == null) return;
					
					switch(msg.what) {
					case 0:
						Log.i("voice: test count:", String.valueOf(m_nTestCount));

						if(m_nTestCount > m_tvTotalProblems-1) {
							
							
							m_bTestStop = true;		
							m_bResultPressed = true;
							
							new Handler() {
								public void handleMessage(Message msg) {
									m_bResultPressed = false;
									m_listener.onAfter(m_nCurrentTestProblemCount, m_nTrueResCount, m_nFalseResCount,m_nTestCurrentProblemIndex,is_successProblem,m_arrAllWordData);
								}
							}.sendEmptyMessageDelayed(0, 500);
						}
						else {
							is_enable_thread = false;

							Log.i("voice: test count:", String.valueOf(m_nTestCount));

							
							makeResWords();


							int nWordIndex;
							if(m_nCurrentTestProblemCount < m_arrWordData.size()){
								nWordIndex = m_nCurrentTestProblemCount;
							}else{
								nWordIndex = m_nTestCount;
							}

							playWordVoice(m_arrWordData.get(nWordIndex).getSound());
							
							m_nTestCount ++;
							m_nCurrentTestProblemCount++;
							
							Log.i("voice", String.valueOf(m_nCurrentTestProblemCount));
							
							setTestInit();
							m_bProblemView = true;
						}
						is_message_enabled = false;
						setTestResult();
						break;
					case 1:
						if (!is_enable_thread) {
							is_enable_thread = true;
							
							m_nFalseResCount ++;
							is_successProblem[m_nCurrentTestProblemCount-1] = false;
							//í‹€ë¦° ë‹¨ì–´ ë‹¨ì–´ìž¥ ì¶”ê°€
							StudyTestActivity.playWordVoice("fail");
							if(m_nTestCount != 0)
								insertWrongWord(m_arrWordData.get(m_nTestCount-1).getId());
							animSelectTrueWord();
							
							m_bResultPressed = true;
							
							new Handler() {
								public void handleMessage(Message msg) {
									m_nTestTime = 0;
									m_bResultPressed = false;
									m_bProblemView = false;
								}
							}.sendEmptyMessageDelayed(0, 1200);
						}
						break;
					}
				}
			};
			
			public void run() {
				while(!m_bTestStop) {
					pauseThread();
					sleepResultPressed();
					if(m_nTestTime == 0) m_handler.sendEmptyMessageDelayed(0, 300);	//ì‹œí—˜ë¬¸ì œ ì œì‹œ
					
					m_nTestTime ++;
									
					m_nTestTime += 20;
					
					if(m_nTestTime >= m_nTestLimitTime*1000) {
						m_nTestTime = m_nTestLimitTime*1000;
						is_message_enabled = true;
						m_handler.sendEmptyMessage(1);
					}
					
					if (is_message_enabled) {
						
					}else{
						m_progressbar.setProgress(100 * m_nTestTime / (m_nTestLimitTime *1000));
					}
					try {
						Thread.sleep(20);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
					
				}
			}
		};

		getTestProblems(Define.KIND_TEST_VOICE);
		initSound();
		
		m_threadTest.setName("EngVoiceTestThread");
		m_threadTest.setDaemon(true);
		m_threadTest.start();
	}
	
	//ë‹µì�„ ì„ íƒ�í•œí›„ 1ì´ˆìžˆë‹¤ê°€ ë‹¤ì�Œ ë¬¸ì œë¥¼ ì œì‹œ
	private void sleepResultPressed() {
		while(m_bResultPressed) {
			try {
				Thread.sleep(20);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setTestPause(boolean bPause) {
		super.setTestPause(bPause);

		if(m_nTestCount > m_arrWordData.size() - 1)
			return;

		if(bPause)
			pauseWordVoice(m_arrWordData.get(m_nTestCount).getSound());
		else
			resumeWordVoice(m_arrWordData.get(m_nTestCount).getSound());
	}	
		
	public void finishTest() {
		if(m_soundPool != null) {
			m_soundPool.release();
			m_soundPool = null;
		}
		
		if(m_soundMap != null) {
			m_soundMap.clear();
			m_soundMap = null;
		}

		if(m_threadTest != null && m_threadTest.isAlive()) {
			m_bTestStop = true;
			try {
				m_threadTest.join();
			}
			catch(Exception e) {} 
		}
		
		m_arrBtnResult = null;
		m_arrIvTrueFalseMsg = null;
		m_arrIvWordNum = null;
		m_strEngRes = null;
		
		super.finishTest();
	}
	
	public void setListener(OnEngVoiceTestFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnEngVoiceTestFinishListener {
		public abstract void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_);

	}
}
