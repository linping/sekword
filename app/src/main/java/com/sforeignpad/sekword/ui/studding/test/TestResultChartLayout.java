package com.sforeignpad.sekword.ui.studding.test;

import java.util.ArrayList;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.structs.UserData;
import com.sforeignpad.sekword.utils.ESUtils;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TestResultChartLayout extends TestLayout {

	private Context m_context = null;
	
	private OnTestResultChartFinishListener m_listener;
	
	private int m_nUserId = -1;		//사용자아이디
	private UserDBManager m_userDbMana = null;
	private int m_nOrder = 0;
	
	
	private LineChart ivStudyChart;
	
	public TestResultChartLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		initLayout();
	}

	private void initLayout() {
		View v = LayoutInflater.from(m_context).inflate(R.layout.testresult_chart_layout, null);
		this.addView(v);
	}
	
	public void setResultChartLayout() {
		String strDesc1 = String.format("%d%s %d%s", m_nTotalTestProblemCount, m_context.getString(R.string.test_msg1), m_nTrueResCount, m_context.getString(R.string.test_msg2));
		TextView tvResultDesc1 = (TextView)findViewById(R.id.tvResultDesc1);
		tvResultDesc1.setText(strDesc1);
		
		
		
//		for (int i = 0; i < 40; i++) {
//			is_successProblem[i] = true;
//		}
		
		
//		for (int i = 10; i < 20; i++) {
//			is_successProblem[i] = false;
//		}
//		
//		for (int i = 10; i < 30; i++) {
//			is_successProblem[i] = true;
//		}
//		
//		for (int i = 10; i < 40; i++) {
//			is_successProblem[i] = false;
//		}
//		
		
		
//		for (int i = 0; i < 40; i++) {
//			if (is_successProblem[i]) {
//				Log.i("test chart result", i+"번쩨 문제:"+"옰음");
//			}else{
//				Log.i("test chart result", i+"번쩨 문제:"+"틒림");
//
//			}
//		}
		
		
		float[] chartScore = new float[41];
		float sum = 0;
		
		chartScore[0]=0;
		
		for (int i = 0; i < is_successProblem.length; i++) {
			if (is_successProblem[i]  ) {
				sum = sum+2.5f;
			}else{
			}
			chartScore[i+1] = sum;			
		}
		
		
		ivStudyChart = (LineChart)findViewById(R.id.ivStudyChart);
		 ArrayList<Entry> entries = new ArrayList<Entry>();
	     ArrayList<String> labels = new ArrayList<String>();

//	     entries.add(new Entry(0f, 0));
//	     labels.add("");

		 for (int i = 0; i < chartScore.length; i++) {			 
			     entries.add(new Entry(Float.valueOf(chartScore[i]), i));
			     //5의 배수미면
			     if ((i%5)==0) {
			    	 
			    	 
			    	 if (i == 0) {
			    	     labels.add("");
					 }else{
		    	     labels.add(String.valueOf(i));
					 }
				}else{
		    	     labels.add("");
				}
		 }
		 
//		 labels.add("");
		 
//	     entries.add(new Entry(Float.valueOf(chartScore[39]),42));
//
//		 labels.add("40"); 
		 
		 
	     LineDataSet dataset = new LineDataSet(entries, null);
	     
	     dataset.setColor(getResources().getColor(R.color.chart_line_color));
//	     dataset.setCircleColor(getResources().getColor(R.color.chart_dot_color));
//	     dataset.setCircleColorHole(getResources().getColor(R.color.chart_dot_color));
	     dataset.setDrawCircleHole(false);
	     dataset.setDrawValues(false);
	     dataset.setCircleSize(5.0f);
	     dataset.setHighlightLineWidth(5f);
	
	     
	     
	     int normalcolor = Color.argb(0, Color.red(0), Color.green(0), Color.blue(0));
   		 int dotcolor = Color.parseColor("#FFFF9000");
   		 
   		 
   		int[] arrColor = new int[41];
		arrColor[0] = normalcolor;
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 4; j++) {
				arrColor[1+j+i*5] = normalcolor;

			}
	   		 arrColor[5+i*5] = dotcolor;

		}
		
	     dataset.setCircleColors(arrColor);
	     
	     
		
	        LineData data = new LineData(labels, dataset);
	        ivStudyChart.setData(data);
	        
	        //X축
	        XAxis xAxis = ivStudyChart.getXAxis();
	       
	        xAxis.setPosition(XAxisPosition.BOTTOM);
	        xAxis.setTextSize(20f);
	        xAxis.setDrawAxisLine(true);
//	        xAxis.setDrawGridLines(false);
	        xAxis.setAxisLineWidth(3.0f);
	        xAxis.setLabelsToSkip(4);
	        
	        //Y축
	        YAxis yAxis = ivStudyChart.getAxisLeft();
	        yAxis.setTextSize(20f);
	        yAxis.setDrawAxisLine(true);


	        yAxis.setAxisMaxValue(100);
	        yAxis.setAxisLineWidth(3.0f);
	        
	        YAxis rightAxis = ivStudyChart.getAxisRight();
	        rightAxis.setAxisMaxValue(100);

	        rightAxis.setDrawLabels(false); 
//	        rightAxis.setEnabled(false);
	        
	        ivStudyChart.setDescription("");
	        ivStudyChart.getLegend().setEnabled(false);
	        ivStudyChart.setDoubleTapToZoomEnabled(false);
		     ivStudyChart.setDragEnabled(false);
		     ivStudyChart.setScaleEnabled(false);
		     ivStudyChart.setTouchEnabled(false);
		     
		     ivStudyChart.animateX(3000);
		     
		//draw chat with chartScore
		
		float nRate = (float)m_nTrueResCount/(float)m_nTotalTestProblemCount * 100.0f;
		if(nRate >= 0.0f && nRate <= 20.0f)
			m_nOrder = 1;
		else if(nRate > 20.0f && nRate <= 40.0f)
			m_nOrder = 2;
		else if(nRate > 40.0f && nRate <= 60.0f)
			m_nOrder = 3;
		else if(nRate > 60.0f && nRate <= 80.0f)
			m_nOrder = 4;
		else if(nRate > 80.0f && nRate <= 100.0f)
			m_nOrder = 5;
		
		String strUserName = m_userDbMana.getUserName(m_nUserId);
		String strDesc2 = String.format("%s%s %d%s", strUserName, m_context.getString(R.string.test_msg3), m_nOrder, m_context.getString(R.string.test_msg4));
		
		TextView tvResultDesc2 = (TextView)findViewById(R.id.tvResultDesc2);
		tvResultDesc2.setText(strDesc2);
		
		TextView tvResultDesc3 = (TextView)findViewById(R.id.tvResultDesc3);
		String strDesc3 = String.format("%d%s", m_nOrder, m_context.getString(R.string.test_msg5));
		tvResultDesc3.setText(strDesc3);
		
		ImageView ivOrder = (ImageView)findViewById(R.id.ivOrder);
		ivOrder.setImageResource(R.drawable.start_order1 + m_nOrder -1);
		
		Button btnOk = (Button)findViewById(R.id.btnResultOk);
		btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//설정된 급수에 따르는 일정보기 화면으로 넘어간다.
				ESUtils.playBtnSound(m_context);

				m_listener.onAfter(m_nOrder);
			}
		});
		
		saveUserOrder();
	}
	
	private void saveUserOrder() {
		UserData data = m_userDbMana.getActiveUser();
		data.setLevel(m_nOrder);
		m_userDbMana.setLevel(data);
	}
	
	//자료기지 해당한 값 얻기
	public void setUserId(int nUserId) {
		m_nUserId = nUserId;
	}
		
	public void setUserDbMana(UserDBManager userDbMana) {
		m_userDbMana = userDbMana;
	}
	
	public void setListener(OnTestResultChartFinishListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnTestResultChartFinishListener {
		public abstract void onAfter(int nOrder);
	}
}
