package com.sforeignpad.sekword.ui.studding.study;

import java.util.ArrayList;
import java.util.HashMap;


import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.PAKDBManager;
import com.sforeignpad.sekword.database.ScheduleDBManager;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.StudyDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.ui.studding.SekWordPopup;
import com.sforeignpad.sekword.ui.studding.SekWordPopup.OnSEKLaunchListener;
import com.sforeignpad.sekword.ui.studding.test.StudyTestActivity;
import com.sforeignpad.sekword.utils.ESUtils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;

public class StudyWordActivity extends PageActivity {

//	private StudyPageCurlView m_vwStudyWord = null;
	private CurlView mCurlView = null;
	
	private FrameLayout FL_StudyWord;

	
	private Button m_btnBack;
	private Button m_btnSave;
	private Button m_btnPrev;
	private Button m_btnPause;
	private Button m_btnNext;
	
	private int m_nUserId = -1;	
	private int m_nScheduleId = -1;
	
	private int m_nOrder = 0;
	private int m_nTargetStudyWordCount = 0;
	
	private int m_nWordBgColorEn = 0;
	private int m_nWordBgColorKr = 0;
	
	private int[] m_nStudyRepeat = new int[2];
	private int m_nStudySpeed = 0;
	
	private long m_nStartStudyTime = 0;
	
	private StudyDBManager m_studyDbMana = null;
	private ScheduleDBManager m_scheduleDbMana = null;
	private WordpadDBManager m_wordpadDbMana = null;
	private SettingDBManager mSettingDbMana = null;
	private ScheduleDateData m_scheduleData = null;
	private PAKDBManager m_pakManager = null;

	private ArrayList<WordData> m_studyWordData = new ArrayList<WordData>();


	private static StudyWordActivity mThis;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_study);

		mThis = this;
		Intent intent = this.getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);
		
		if(!isConnectDB()) {
			finish();
			return;
		}
		
		initView();

		getStudySettingInfo();
		setStudySettingInfo();
		setStudyWordData();
		setStudyListener();
		
		new Handler() {
			public void handleMessage(Message msg) {
				startStudy();
			}
		}.sendEmptyMessageDelayed(0, 200);
		
		
		FL_StudyWord = (FrameLayout)findViewById(R.id.FL_StudyWord);
		
		// This is something somewhat experimental. Before uncommenting next
		// line, please see method comments in CurlView.
		// mCurlView.setEnableTouchPressure(true);
	}

	
	public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
	    Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
	    Canvas canvas = new Canvas(bmOverlay);
	    canvas.drawBitmap(bmp1, new Matrix(), null);
	    canvas.drawBitmap(bmp2, 0, 0, null);
	    return bmOverlay;
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		mCurlView.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		mCurlView.onResume();
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		return mCurlView.getCurrentIndex();
	}

	/**
	 * Bitmap provider.
	 */
	private class PageProvider implements CurlView.PageProvider {

		// Bitmap resources.
		private int[] mBitmapIds = { R.drawable.study_paper_bg3, R.drawable.study_paper_bg3,
				R.drawable.study_paper_bg3, R.drawable.study_paper_bg3 };

		@Override
		public int getPageCount() {
			return 2*m_nTargetStudyWordCount*m_nStudyRepeat[0]*m_nStudyRepeat[1];
		}

		private Bitmap loadBitmap(int width, int height, int index) {
			Bitmap b = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			b.eraseColor(0xFFFFFFFF);
			Canvas c = new Canvas(b);


			
			Drawable d = getResources().getDrawable(mBitmapIds[index]);

			int margin = 0;
			int border = 1;
			Rect r = new Rect(margin, margin, width - margin, height - margin);

			int imageWidth = r.width() - (border * 2);
			int imageHeight = r.height() - (border * 2);
			//int imageHeight = imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth();
			/*if (imageHeight > r.height() - (border * 2)) {
				imageHeight = r.height() - (border * 2);
				imageWidth = imageHeight * d.getIntrinsicWidth()
						/ d.getIntrinsicHeight();
			}*/

			r.left += ((r.width() - imageWidth) / 2) - border;
			r.right = r.left + imageWidth + border + border;
			r.top += ((r.height() - imageHeight) / 2) - border;
			r.bottom = r.top + imageHeight + border + border;

			Paint p = new Paint();
			p.setColor(0xFFC0C0C0);
			c.drawRect(r, p);
			r.left += border;
			r.right -= border;
			r.top += border;
			r.bottom -= border;

			d.setBounds(r);
			d.draw(c);
									
			return b;
		}
		
		private Bitmap loadBitmap1(int width, int height, int index) {
			Bitmap b = Bitmap.createBitmap(width, height,
					Bitmap.Config.ARGB_8888);
			b.eraseColor(0xFFFFFFFF);
			Canvas c = new Canvas(b);


			
			Drawable d = getResources().getDrawable(mBitmapIds[index]);

			int margin = 0;
			int border = 1;
			Rect r = new Rect(margin, margin, width - margin, height - margin);

			int imageWidth = r.width() - (border * 2);
			int imageHeight = r.height() - (border * 2);
			//int imageHeight = imageWidth * d.getIntrinsicHeight() / d.getIntrinsicWidth();
			/*if (imageHeight > r.height() - (border * 2)) {
				imageHeight = r.height() - (border * 2);
				imageWidth = imageHeight * d.getIntrinsicWidth()
						/ d.getIntrinsicHeight();
			}*/

			r.left += ((r.width() - imageWidth) / 2) - border;
			r.right = r.left + imageWidth + border + border;
			r.top += ((r.height() - imageHeight) / 2) - border;
			r.bottom = r.top + imageHeight + border + border;

			Paint p = new Paint();
			p.setColor(0xFFC0C0C0);
			c.drawRect(r, p);
			r.left += border;
			r.right -= border;
			r.top += border;
			r.bottom -= border;

			d.setBounds(r);
			d.draw(c);
			
			Paint paint = new Paint();
			paint.setColor(Color.BLACK);
			paint.setTextSize(30);
			c.drawText("abcdefghi", 1020.0f, 670.0f, paint);
	
			return b;
		}


		@Override
		public void updatePage(CurlPage page, int width, int height, int index) {
//			Bitmap front = loadBitmap(width, height, 0);//			
//			FL_StudyWord.setDrawingCacheEnabled(true);
//			FL_StudyWord.buildDrawingCache();
//			Bitmap front2 = FL_StudyWord.getDrawingCache();				
//			Bitmap front = overlay(front1, front2);			
			
			Bitmap front = loadBitmap(width, height, 0);
			/*if (index == 0)
			{
				front = loadBitmap(width, height, 0);
			}else
			{
				front = loadBitmapFromView(mCurlView);
			}*/
			//front = loadBitmapFromView(FL_StudyWord);
			
			page.setTexture(front, CurlPage.SIDE_BOTH);
			page.setColor(Color.argb(127, 255, 255, 255),
					CurlPage.SIDE_BACK);
			
		}
		
		@Override
		public void updatePage1(CurlPage page, int width, int height, int index) {

			Bitmap front = loadBitmap1(width, height, 0);

			page.setTexture(front, CurlPage.SIDE_BOTH);
			page.setColor(Color.argb(127, 255, 255, 255),
					CurlPage.SIDE_BACK);
			
		}

	}
	
	public static Bitmap loadBitmapFromView(View v) {
		if (v.getMeasuredHeight() <= 0) {
		    v.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		    Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		    Canvas c = new Canvas(b);
		    v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
		    v.draw(c);
		    return b;
		}else {
			Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);                
		    Canvas c = new Canvas(b);
		    v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
		    v.draw(c);
		    return b;
		}
	}
	
	/**
	 * CurlView size changed observer.
	 */
	private class SizeChangedObserver implements CurlView.SizeChangedObserver {
		@Override
		public void onSizeChanged(int w, int h) {
//			if (w > h) {
//				mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
//				mCurlView.setMargins(.1f, .05f, .1f, .05f);
//			} else {
				mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
				mCurlView.setMargins(0.0f, 0.0f, 0.0f, 0.0f);
//			}
		}
	}


	private void initView() {
//		m_vwStudyWord = (StudyPageCurlView)findViewById(R.id.vwStudyWord);
//		m_vwStudyWord.setPageBitmap(R.drawable.study_paper_bg1,R.drawable.study_paper_bg1);
//		m_vwStudyWord.SetCurlSpeed(180);
//		m_vwStudyWord.SetInitialEdgeOffset(0);
//		m_vwStudyWord.SetUpdateRate(5);
		
//		page_curl = (StudyCurlLayout) findViewById(R.id.vwStudyWord_curl);
		
		
		int index = 0;
		if (getLastNonConfigurationInstance() != null) {
			index = (Integer) getLastNonConfigurationInstance();
		}
		mCurlView = (CurlView) findViewById(R.id.curl_vwStudyWord);
		mCurlView.setPageProvider(new PageProvider());
		mCurlView.setSizeChangedObserver(new SizeChangedObserver());
		mCurlView.setCurrentIndex(index);
		mCurlView.setBackgroundColor(0xFFFFFFFF);
		mCurlView.setParent(this);
		
		m_btnBack = (Button)findViewById(R.id.btnBack);
		m_btnBack.setOnClickListener(m_clickListener);
		
		m_btnSave = (Button)findViewById(R.id.btnSave);
		m_btnSave.setOnClickListener(m_clickListener);
		
		m_btnPrev = (Button)findViewById(R.id.btnPrevWord);
		m_btnPrev.setOnClickListener(m_clickListener);
		
		m_btnPause = (Button)findViewById(R.id.btnPause);
		m_btnPause.setOnClickListener(m_clickListener);
		
		m_btnNext = (Button)findViewById(R.id.btnNextWord);
		m_btnNext.setOnClickListener(m_clickListener);
				
	}
	
	private void showBackDlg(){
		/*new AlertDialog.Builder(this)
		.setTitle(this.getString(R.string.notice))
		.setCancelable(false)
		.setMessage(this.getString(R.string.study_back))
		.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
				StudyWordActivity.this.finish();

			}
		})
		.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.show();*/
		
		
		mCurlView.doPause();

		
		SekWordPopup popup;

		popup = new SekWordPopup(StudyWordActivity.this);
		popup.setMessage(this.getString(R.string.study_back));
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				StudyWordActivity.this.finish();
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				mCurlView.doPause();
			}

		});
		
		popup.show();
	}
	private View.OnClickListener m_clickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {

			ESUtils.playBtnSound(StudyWordActivity.this);

			// TODO Auto-generated method stub
			switch(v.getId()) {
			case R.id.btnBack:
				//StudyWordActivity.this.finish();
				showBackDlg();
				break;
			case R.id.btnSave:
				saveStudyWord();
				break;
			case R.id.btnPrevWord:
				doPrevPerform();
				break;
			case R.id.btnNextWord:
				doNextPerform();
				break;
			case R.id.btnPause:
				doPausePeform();				
				break;
			}
		}
	};

	@Override
	public void onBackPressed() {
		// your code.
		showBackDlg();

	}

	private void doPrevPerform() {
		
		/*if(mCurlView.getFlippingState())
			return;
		*/
		if(mCurlView.getCurrentWordCount() == 0) 
			return;
		
		sendMouseEventPrev();
		m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
	}
	
	private void doNextPerform() {

		/*if(mCurlView.getFlippingState())
			return;*/
		
		int nWordCount = mCurlView.getCurrentWordCount();
		int nRound = mCurlView.getCurrentRound();
		int nPhase = mCurlView.getCurrentPhase();
		
		if(nWordCount == m_studyWordData.size()-1 && nRound == m_nStudyRepeat[nPhase-1]-1 &&
				nPhase == 2) 
			return;		

		sendMouseEventNext();				
		m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
	}
	
	private void doPausePeform() {
		if(mCurlView.getFlippingState())
			return;
		
		mCurlView.doPause();
		
		if(mCurlView.isPause()) 
			m_btnPause.setBackgroundResource(R.drawable.btn_study_play_play);
		else
			m_btnPause.setBackgroundResource(R.drawable.btn_study_play_pause);
	}
	
	//DB
	private boolean isConnectDB() {
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;
			
		m_scheduleDbMana = new ScheduleDBManager(this, m_nUserId);
		if(m_scheduleDbMana == null)
			return false;
		
		m_wordpadDbMana = new WordpadDBManager(this, m_nUserId);
		if(m_wordpadDbMana == null)
			return false;
		
		mSettingDbMana = new SettingDBManager(this);
		if(mSettingDbMana == null)
			return false;

		m_pakManager = new PAKDBManager(this);
		if(m_pakManager == null)
			return false;

		return true;
	}
		
	private void closeDB() {
		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
	}
	
	public void saveStudyTimeToDB() {
		long nEndStudyTime = ESUtils.getNowTimeSeconds();
		long nDiff = nEndStudyTime - m_nStartStudyTime;
		m_nStartStudyTime = nEndStudyTime;

		if(m_scheduleData != null && m_studyDbMana != null) {
			m_scheduleData.setTimes(nDiff);
			m_studyDbMana.setTimes(m_scheduleData);
		}
	}
	
	private void saveStudyWord() {
		WordData wd = mCurlView.getCurrentWord();
		if(wd == null)
			return;
		
		m_wordpadDbMana.InsertWordpad(Define.KIND_IMPORT_WORD,	wd.getId());
	}
	
	private void getStudySettingInfo() {
		m_nStudyRepeat[0] = mSettingDbMana.getStudy1Value();
		m_nStudyRepeat[1] = mSettingDbMana.getStudy2Value();
		
		m_nWordBgColorEn = mSettingDbMana.getWordColorValue();
		m_nWordBgColorKr = mSettingDbMana.getMeanColorValue();
		
		m_nStudySpeed = mSettingDbMana.getStudySpeedIndex();
	}
	
	private void setStudySettingInfo() {
		mCurlView.setStudyRepeatCount(m_nStudyRepeat);
		mCurlView.setStudySpeed(m_nStudySpeed);
		mCurlView.setWordBgColor(m_nWordBgColorEn, m_nWordBgColorKr);
	}
	
	private void setStudyWordData() {
		m_scheduleData = m_scheduleDbMana.getScheduleDateData(m_nScheduleId); 
		m_nOrder = m_scheduleDbMana.getLevel(m_nScheduleId);
		
		m_scheduleDbMana.createScheduleWord(m_scheduleData.getId());
		m_studyWordData = m_studyDbMana.getWordList(m_scheduleData);
		
		m_nTargetStudyWordCount = m_studyWordData.size();

		mCurlView.setWordData(m_studyWordData);
		mCurlView.setStudyOrderAndWordCount(m_nOrder, m_nTargetStudyWordCount);
	}
	
	private void startStudy() {
		getStudyStartTime();
		mCurlView.startStudy();
	}
	
	private void getStudyStartTime() {
		m_nStartStudyTime = ESUtils.getNowTimeSeconds();
	}
	
	private void setStudyListener() {
		mCurlView.setListener(new CurlView.StudyWordListener() {
			
			@Override
			public void onRember(WordData wd) {
				// TODO Auto-generated method stub
				if(m_studyDbMana != null)
					m_studyDbMana.setWordComplete(m_scheduleData.getId(), wd.getId());
			}
			
			@Override
			public void onFinishAfter() {
				// TODO Auto-generated method stub
				goStudyTestActivity();
				StudyWordActivity.this.finish();
			}
		});
	}

	
	public static void playWordVoice(String strVoice) {
		/*if(m_soundPool != null)
			m_soundPool.play(m_soundMap.get(strVoice).intValue(), m_fVolume, m_fVolume, 1, 0, 1.0F);*/

		if(strVoice.contains("page")){
			ESUtils.playPageSound(mThis);
		}
	}

	public String getImageFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 0);
	}

	public String getAudioFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 1);
	}

	private void finishStudy() {
		
		mCurlView.finishStudy();
		saveStudyTimeToDB();
		closeDB();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishStudy();
		super.onDestroy();
	}
	
	private void goStudyTestActivity() {
		Intent intent = new Intent(this, StudyTestActivity.class);
		intent.putExtra(Define.USER_ID, m_nUserId);
		intent.putExtra(Define.SCHEDULE_ID, m_scheduleData.getId());
		startActivity(intent);
	}
	
	private void sendMouseEventPrev() {

		runOnUiThread(new Runnable() {
			public void run() {
				MotionEvent event = null;


				event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 10f, 660f, 1);
				mCurlView.dispatchTouchEvent(event);


				/*for (int i = 0; i < 19; i++) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*i, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,(1230- i*60), 700-i*34f, 1);
					mCurlView.dispatchTouchEvent(event);
				}

				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*20, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,10, 10, 1);
				mCurlView.dispatchTouchEvent(event);*/
			}
		});
		/*
		MotionEvent event = null;
			
		event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 10f, 660f, 1);
		mCurlView.dispatchTouchEvent(event);
		
		
		for (int i = 0; i < 19; i++) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}                                                                                                                    
			event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*i, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE, i*60, 700f-i*34f, 1);
			mCurlView.dispatchTouchEvent(event);
		}
		
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                                                                                                                    
		event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*20, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,1250f, 10f, 1);
		mCurlView.dispatchTouchEvent(event);*/

	}
	

	private void sendMouseEventNext() {

		
		runOnUiThread(new Runnable() {
		    public void run() {
		    	MotionEvent event = null;
				
				event = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 1230, 700f, 1);
				mCurlView.dispatchTouchEvent(event);
				
				
				/*for (int i = 0; i < 19; i++) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}                                                                                                                    
					event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*i, SystemClock.uptimeMillis(), MotionEvent.ACTION_MOVE,(1230- i*60), 700-i*34f, 1);
					mCurlView.dispatchTouchEvent(event);
				}
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}                                                                                                                    
				event = MotionEvent.obtain(SystemClock.uptimeMillis()+15*20, SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,10, 10, 1);
				mCurlView.dispatchTouchEvent(event);*/
		    }
		});

				
	}
	
}
