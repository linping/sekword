package com.sforeignpad.sekword.ui.setting;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;


public class SettingPopup6 extends Dialog implements View.OnClickListener {
	
	private Context mContext;
	private Button mBtnOk;
	private Button mBtnCancel;
	private Button[] mItems;
	
	private SettingDBManager mDbMana = null;
	
	public SettingPopup6(Context context) {
		super(context, R.style.MyDialog);
		
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initViews();
	}
	
	private void initViews() {
		View localView = LayoutInflater.from(this.mContext).inflate(R.layout.setting_popup6, null);
		this.setContentView(localView);
		
		mItems = new Button[4];
		mItems[0] = (Button)localView.findViewById(R.id.btnItem1);
		mItems[1] = (Button)localView.findViewById(R.id.btnItem2);
		mItems[2] = (Button)localView.findViewById(R.id.btnItem3);
		mItems[3] = (Button)localView.findViewById(R.id.btnItem4);
		mBtnOk = (Button)localView.findViewById(R.id.btnSettingOk);
		mBtnCancel = (Button)localView.findViewById(R.id.btnSettingCancel);		
		
		for (int i=0; i<4; i++) {
			mItems[i].setOnClickListener(this);
			ESUtils.setButtonTypeFaceByRes(mContext, mItems[i], Define.getMainFont());
		}
		mBtnOk.setOnClickListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
	}
	
	public void initValues() {
		this.openDB();
		
		String sData = mDbMana.getTestModeValue();
		String[] data = sData.split(",");
		
		for (int i=0; i<data.length; i++) {
			boolean value = (data[i].equals("1")) ? true : false;
			mItems[i].setSelected(value);
		}
	}
	
	private void setSelectedItem(int pos) {
		if (!mItems[pos].isSelected()) {
			mItems[pos].setSelected(true);
		} else {
			mItems[pos].setSelected(false);
		}
	}
	
	private boolean openDB() {
		if (mDbMana == null)
			mDbMana = new SettingDBManager(mContext);
		
		if (mDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mDbMana != null) {
			mDbMana.close();
			mDbMana = null;
		}
	}
	
	@Override
	public void dismiss() {
		this.closeDB();		
	    super.dismiss();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(mContext);

		switch (v.getId()) {
			case R.id.btnItem1:
				setSelectedItem(0);
				break;
			case R.id.btnItem2:
				setSelectedItem(1);
				break;
			case R.id.btnItem3:
				setSelectedItem(2);
				break;
			case R.id.btnItem4:
				setSelectedItem(3);
				break;
			case R.id.btnSettingOk:
				if (this.openDB()) {
					String comma = ",";
					String value = "";
					boolean isNone = true;
					
					for (int i=0; i<mItems.length; i++) {
						comma = (i==0) ? "" : ",";
						
						if (mItems[i].isSelected()) {
							value += comma+"1";
							isNone = false;
						} else {
							value += comma+"0";
						}							
					}
					
					value = (!isNone) ? value : "1,0,0,0";
					mDbMana.setTestModeValue(value);
				}
				
				dismiss();
				break;
			case R.id.btnSettingCancel:
				dismiss();
				break;
		}
	}

}
