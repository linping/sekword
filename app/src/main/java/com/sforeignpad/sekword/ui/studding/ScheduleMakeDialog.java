package com.sforeignpad.sekword.ui.studding;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.utils.ESUtils;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ScheduleMakeDialog extends Dialog {

	private Context m_context = null;
	
	private ImageView m_ivNum1 = null;
	private ImageView m_ivNum2 = null;
	private ImageView m_ivNum3 = null;
	
	private TextView m_tvSchedulePerior = null;
	private TextView m_tvScheduleEndDate = null;
	
	private int m_nCount = 20;	//하루에 학습할 단어개수
	private int m_nTotalWordCount = 1000;	//급수에 따르는 단어총개수
	private int m_nTotalDays = 0;	//학습기일
	
	private int m_nStudiedWordCount = 0;		//이미 학습한 날자수
	private int m_nStudiedDays = 0;				//이미 학습한 날수
	private long m_nStartDate = 0;				//시작날자
	
	private OnScheduleMakeListener m_listener;
	
	public ScheduleMakeDialog(Context context) {
		super(context, R.style.MyDialog);
		// TODO Auto-generated constructor stub
		m_context = context;
		
		initDialog();
	}

	private void initDialog() {
		View contentView = LayoutInflater.from(m_context).inflate(R.layout.schedule_make_dialog, null);
		this.setContentView(contentView);
		
		this.setCanceledOnTouchOutside(false);		
		
		m_ivNum1 = (ImageView)findViewById(R.id.ivNum1);
		m_ivNum2 = (ImageView)findViewById(R.id.ivNum2);
		m_ivNum3 = (ImageView)findViewById(R.id.ivNum3);
		
		m_tvSchedulePerior = (TextView)findViewById(R.id.tvStudyCountDay);
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvSchedulePerior, Define.getMainFont());
		
		m_tvScheduleEndDate = (TextView)findViewById(R.id.tvStudyEndDate);
		ESUtils.setTextViewTypeFaceByRes(m_context, m_tvScheduleEndDate, Define.getMainFont());
		
		setCountView();
		
		Button btnCountUp = (Button)findViewById(R.id.btnCountUp);
		btnCountUp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				// TODO Auto-generated method stub
				if(m_nCount < 100)
					m_nCount += 10;
				setCountView();
			}
		});
		
		Button btnCountDown = (Button)findViewById(R.id.btnCountDown);
		btnCountDown.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				// TODO Auto-generated method stub
				if(m_nCount > 10)
					m_nCount -= 10;
				setCountView();
			}
		});
		
		Button btnOk = (Button)findViewById(R.id.btnOk);
		btnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				// TODO Auto-generated method stub
				m_listener.onOK();
				ScheduleMakeDialog.this.dismiss();
			}
		});
		
		Button btnCancel = (Button)findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(m_context);

				// TODO Auto-generated method stub
				m_listener.onCancel();
				ScheduleMakeDialog.this.dismiss();
			}
		});
	}
	
	public void initWordCount() {
		m_nCount = 20;
		setCountView();
	}
	
	public void setWordCount(int wordCount){
		
		if (wordCount == 0) {
			m_nCount = 20;
		}else{
			m_nCount = wordCount;
		}
		setCountView();
	}
	
	//학습단어개수 표시
	private void setCountView() {
		String strCount = Integer.toString(m_nCount);
		
		if(m_nCount == 100) {
			m_ivNum3.setVisibility(View.VISIBLE);
			
			m_ivNum1.setImageResource(R.drawable.words_num1);
			m_ivNum2.setImageResource(R.drawable.words_num0);
			m_ivNum3.setImageResource(R.drawable.words_num0);
		}
		else {
			String strNum = strCount.substring(0, 1);
			
			m_ivNum1.setImageResource(R.drawable.words_num0 + Integer.valueOf(strNum));
			m_ivNum2.setImageResource(R.drawable.words_num0);
			m_ivNum3.setVisibility(View.GONE);
		}
			
		setCountDays();
	}
	
	//학습일수, 학습완료일 설정
	private void setCountDays() {
		m_nTotalDays = (m_nTotalWordCount - m_nStudiedWordCount)/m_nCount;	//총 단어수-이미 학습한 단어수
		if(m_nTotalWordCount % m_nCount != 0) m_nTotalDays += 1 ;
		
		long nEndMill = ESUtils.getTimeMilliSeconds(m_context, m_nTotalDays-1);	//완료일은 오늘로부터 계산
		
		String strEndDate = ESUtils.getDateFormat(m_context, nEndMill);
		m_tvScheduleEndDate.setText(m_context.getString(R.string.schedule_study_endday) + strEndDate);
		
		m_nTotalDays = m_nStudiedDays + m_nTotalDays;	//이미 학습한 날과 이제 학습해야 할 날
		String strPerior = m_context.getString(R.string.schedule_study_perio) + Integer.toString(m_nTotalDays).toString() +
				m_context.getString(R.string.day);
		m_tvSchedulePerior.setText(strPerior);
	}
	
	public int getTotalDays() {
		return m_nTotalDays;
	}
	
	public int getWordCountOfDay() {
		return m_nCount;
	}
	
	//단어총개수 설정
	public void setTotalWordCount(int nCount) {
		m_nTotalWordCount = nCount;
		setCountView();
	}
	
	//이미 학습한 날자수, 이미 학습한 단어개수
	public void setStudiedDaysAndWords(int nDays, int nWords) {
		m_nStudiedDays = nDays;
		m_nStudiedWordCount = nWords;
	}
	
	//시작날자
	public void setStartDate(long nStartDate) {
		m_nStartDate = nStartDate;
	}
	
	public void setScheduleMakeListener(OnScheduleMakeListener listener) {
		m_listener = listener;
	}
	
	public abstract interface OnScheduleMakeListener {
		public abstract void onOK();
		public abstract void onCancel();
	}
}
