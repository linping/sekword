/*
 * Copyright (C) 2010 Neil Davies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * This code is base on the Android Gallery widget and was Created 
 * by Neil Davies neild001 'at' gmail dot com to be a Coverflow widget
 * 
 * @author Neil Davies
 */
package com.sforeignpad.sekword.ui.studding.test;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.sforeignpad.sekword.R;

public class TestProgressBar extends ProgressBar
{
	private Bitmap m_bmpBg = null;
	private Paint m_paint = new Paint();
	
//	   private static final Interpolator DEFAULT_INTERPOLATER = new AccelerateDecelerateInterpolator();
//
//	    private ValueAnimator animator;
//	    private ValueAnimator animatorSecondary;
//	    private boolean animate = true;
//	
	
	
	public TestProgressBar(Context context)
	{
		super(context);
		initProgressBar();
	}
	 
	public TestProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initProgressBar();
	}

	
//	public boolean isAnimate() {
//        return animate;
//    }
//
//    public void setAnimate(boolean animate) {
//        this.animate = animate;
//    }
//	
//    @SuppressLint("NewApi") @Override
//    public synchronized void setProgress(int progress) {
//        if (!animate) {
//            super.setProgress(progress);
//            return;
//        }
//        if (animator != null)
//            animator.cancel();
//        if (animator == null) {
//            animator = ValueAnimator.ofInt(getProgress(), progress);
//            animator.setInterpolator(DEFAULT_INTERPOLATER);
//            animator.addUpdateListener(new AnimatorUpdateListener() {
//
//                @Override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                	TestProgressBar.super.setProgress((Integer) animation.getAnimatedValue());
//                }
//            });
//        } else
//            animator.setIntValues(getProgress(), progress);
//        animator.start();
//
//    }
//
//    @SuppressLint("NewApi") @Override
//    public synchronized void setSecondaryProgress(int secondaryProgress) {
//        if (!animate) {
//            super.setSecondaryProgress(secondaryProgress);
//            return;
//        }
//        if (animatorSecondary != null)
//            animatorSecondary.cancel();
//        if (animatorSecondary == null) {
//            animatorSecondary = ValueAnimator.ofInt(getProgress(), secondaryProgress);
//            animatorSecondary.setInterpolator(DEFAULT_INTERPOLATER);
//            animatorSecondary.addUpdateListener(new AnimatorUpdateListener() {
//
//                @Override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                	TestProgressBar.super.setSecondaryProgress((Integer) animation
//                            .getAnimatedValue());
//                }
//            });
//        } else
//            animatorSecondary.setIntValues(getProgress(), secondaryProgress);
//        animatorSecondary.start();
//    }
//
//    @SuppressLint("NewApi") @Override
//    protected void onDetachedFromWindow() {
//        super.onDetachedFromWindow();
//        if (animator != null)
//            animator.cancel();
//        if (animatorSecondary != null)
//            animatorSecondary.cancel();
//    }
    
	private void initProgressBar() {
		m_bmpBg = BitmapFactory.decodeResource(getResources(), R.drawable.test_progress_bg);
	}

	public void initProgressBarManullay(boolean bAbove){
		if(bAbove){
			m_bmpBg = BitmapFactory.decodeResource(getResources(), R.drawable.test_progress_bg_green);

		}else {
			m_bmpBg = BitmapFactory.decodeResource(getResources(), R.drawable.test_progress_bg_grey);

		}
	}
	@SuppressLint("DrawAllocation")
	public void onDraw(Canvas canvas)
	{
		if (m_bmpBg == null)
			return;
		
		int	w = m_bmpBg.getWidth();
		int	h = m_bmpBg.getHeight();
		int nProgrss = getProgress();
		Rect	rcSrc = new Rect(0, 0, w * nProgrss / 100, h);
		Rect	rcDst = new Rect(rcSrc);
		canvas.drawBitmap(m_bmpBg, rcSrc, rcDst, m_paint);
	}
}
