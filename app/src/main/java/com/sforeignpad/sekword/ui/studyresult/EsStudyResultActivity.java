package com.sforeignpad.sekword.ui.studyresult;

import java.util.ArrayList;
import java.util.Calendar;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.ScheduleDBManager;
import com.sforeignpad.sekword.database.StudyResultDBManager;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.effect.ScaleAnimEffect;
import com.sforeignpad.sekword.structs.DateData;
import com.sforeignpad.sekword.structs.ScheduleData;
import com.sforeignpad.sekword.ui.studding.SekWordPopupOneButton;
import com.sforeignpad.sekword.ui.studding.SekWordPopupOneButton.OnSEKLaunchListener;
import com.sforeignpad.sekword.utils.ESUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author RWI
 * 2015.6.28
 * 학습결과
 */

@SuppressLint({ "ShowToast", "HandlerLeak" })
public class EsStudyResultActivity extends Activity implements View.OnClickListener {
	
	private int mUId;
	private int mTabIndex = 3;
	private int[] mDateIndex;
	
	private RelativeLayout mLayoutTab;
	private FrameLayout[] mLayoutTabMenus = new FrameLayout[4];
	private Button[] mBtnSelectTabs = new Button[4];
	private Button[] mBtnDefaultTabs = new Button[4];
	private Button[] mBtnPrev = new Button[3];
	private Button[] mBtnNext = new Button[3];
	private FrameLayout[] mLayoutTabContents = new FrameLayout[3];	
	private TextView[] mDateText = new TextView[3];
	private TimeGraphView mTimeGraphView;
	private WordGraphView mWordGraphView;
	private MarkGraphView mMarkGraphView;
	
	private ScaleAnimEffect mAnimEffect = new ScaleAnimEffect(); 
	
	private ScheduleData mScheduleData;
	private ArrayList<DateData> mDateList;
	
	private UserDBManager mUserDbMana;
	private StudyResultDBManager mStudyResultDBMana;
	private ScheduleDBManager mScheduleDBMana;
	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_study_result);
		
		Intent intent = this.getIntent();
		mUId = intent.getIntExtra(Define.USER_ID, -1);
		
		if (!openDB()) {
			finish();
			return;
		}
		
		mScheduleData = mScheduleDBMana.getScheduleData();	
		
		if (mScheduleData == null) {
			Toast.makeText(this, this.getString(R.string.studyresult_msg1), 8000).show();
			finish();
			return;
		}
		
		mDateList = mStudyResultDBMana.getDateList(mScheduleData.getStartDate(), mScheduleDBMana.getLastDate(mScheduleData));
		
		if (mDateList.isEmpty()) {
			Toast.makeText(this, this.getString(R.string.studyresult_msg2), 8000).show();
			finish();
			return;
		}


		int value = mUserDbMana.getIsUseResultHelp(mUId);
		if (value == 1) {
			/*new AlertDialog.Builder(this)
			.setTitle(R.string.msgTitle)
			.setMessage(R.string.studyResultMsg)
			.setCancelable(false)
			.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
			
				public void onClick(DialogInterface dialog, int which) {	
					mUserDbMana.setNoUseResultHelp(mUId);
				}
			})
			.show();*/
			SekWordPopupOneButton popup;		
			popup = new SekWordPopupOneButton(EsStudyResultActivity.this);
			popup.setMessage(this.getString(R.string.studyResultMsg));
			//popup.setMessageSize(16);
			popup.setListener(new OnSEKLaunchListener() {

				@Override
				public void onOK() {
					mUserDbMana.setNoUseResultHelp(mUId);
				}
			});
			popup.show();
		}
		
		initViews();
	} 
	
	private void initViews() {
		initVariables();
		initTabBar();		
		setTabLayout();
	}
	
	private void initTabBar() {
		mLayoutTab = (RelativeLayout)findViewById(R.id.TabLayout);
		
		mLayoutTabMenus[3] = (FrameLayout)findViewById(R.id.TabTimeLayout);
		mLayoutTabMenus[2] = (FrameLayout)findViewById(R.id.TabWordLayout);
		mLayoutTabMenus[1] = (FrameLayout)findViewById(R.id.TabMarkLayout);
		mLayoutTabMenus[0] = (FrameLayout)findViewById(R.id.TabExitLayout);
		
		mLayoutTabContents[2] = (FrameLayout)findViewById(R.id.layoutStudyResultTab1);
		mLayoutTabContents[1] = (FrameLayout)findViewById(R.id.layoutStudyResultTab2);
		mLayoutTabContents[0] = (FrameLayout)findViewById(R.id.layoutStudyResultTab3);
		
		mBtnSelectTabs[3] = (Button)findViewById(R.id.btnTabTimeOff);
		mBtnSelectTabs[2] = (Button)findViewById(R.id.btnTabWordOff);
		mBtnSelectTabs[1] = (Button)findViewById(R.id.btnTabMarkOff);
		mBtnSelectTabs[0] = (Button)findViewById(R.id.btnExitOff);
		
		mBtnDefaultTabs[3] = (Button)findViewById(R.id.btnTabTimeOn);
		mBtnDefaultTabs[2] = (Button)findViewById(R.id.btnTabWordOn);
		mBtnDefaultTabs[1] = (Button)findViewById(R.id.btnTabMarkOn);
		mBtnDefaultTabs[0] = (Button)findViewById(R.id.btnExitOn);
		
		mTimeGraphView = (TimeGraphView)findViewById(R.id.viewGraph1);
		mWordGraphView = (WordGraphView)findViewById(R.id.viewGraph2);
		mMarkGraphView = (MarkGraphView)findViewById(R.id.viewGraph3);
		
		mDateText[2] = (TextView)findViewById(R.id.txtStudyResultTab1Page);
		mDateText[1] = (TextView)findViewById(R.id.txtStudyResultTab2Page);
		mDateText[0] = (TextView)findViewById(R.id.txtStudyResultTab3Page);
		
		mBtnPrev[2] = (Button)findViewById(R.id.btnStudyResultTab1Left);
		mBtnPrev[1] = (Button)findViewById(R.id.btnStudyResultTab2Left);
		mBtnPrev[0] = (Button)findViewById(R.id.btnStudyResultTab3Left);
		
		mBtnNext[2] = (Button)findViewById(R.id.btnStudyResultTab1Right);
		mBtnNext[1] = (Button)findViewById(R.id.btnStudyResultTab2Right);
		mBtnNext[0] = (Button)findViewById(R.id.btnStudyResultTab3Right);

		for(int i = 0; i < mDateText.length; i++){
			ESUtils.setTextViewTypeFaceByRes(this, mDateText[i], Define.getMainFont());
		}

		for(int i=0; i<4; i++) {
			mBtnSelectTabs[i].setVisibility(View.INVISIBLE);			
			mBtnDefaultTabs[i].setOnClickListener(this);
		}
		
		for (int i=0; i<mBtnPrev.length; i++) {
			mBtnPrev[i].setOnClickListener(this);
			mBtnNext[i].setOnClickListener(this);
		}
		
		reAddAllTab();
		
		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				showOnFocusAnimation(mTabIndex, 10);
			}
		};
		handler.sendEmptyMessageDelayed(0, 10);
	}
	
	private void initVariables() {
		mDateIndex = new int[3];
		mDateIndex[0] = 0;
		mDateIndex[1] = 0;
		mDateIndex[2] = 0;

		Calendar calendar = Calendar.getInstance();

		long timeInMillis = calendar.getTimeInMillis();

		mDateIndex[0] = mDateList.size() - 1;
		mDateIndex[1] = mDateList.size() - 1;
		mDateIndex[2] = mDateList.size() - 1;

		for( int i = 1; i < mDateList.size(); i++){
			DateData data = mDateList.get(i);
			if(timeInMillis < data.getStartValue()){
				mDateIndex[0] = i - 1;
				mDateIndex[1] = i - 1;
				mDateIndex[2] = i - 1;

				break;
			}
		}
	}
	
	private void showOnFocusAnimation(final int index, int duration) {
	    this.mLayoutTabMenus[index].bringToFront();
	    this.mAnimEffect.setAttributs(1.0F, 1.0F, 0.9F, 1.0F, duration);
	    
	    Animation anim = this.mAnimEffect.createAnimation();
	    anim.setAnimationListener(new Animation.AnimationListener() {
	    	public void onAnimationEnd(Animation paramAnimation) {}

	    	public void onAnimationRepeat(Animation paramAnimation) { }
	    	public void onAnimationStart(Animation paramAnimation) { }
	    });
	    
	    mBtnDefaultTabs[index].setVisibility(View.INVISIBLE);
	    mBtnSelectTabs[index].setVisibility(View.VISIBLE);
	    mBtnSelectTabs[index].startAnimation(anim);
	}
	
	private void showLooseFocusAinimation(final int index) {
		reAddAllTab();
		
		mBtnSelectTabs[index].setVisibility(View.INVISIBLE);
		mBtnDefaultTabs[index].setVisibility(View.VISIBLE);
	
	}
	
	private void reAddAllTab() {		
		RelativeLayout.LayoutParams[] layoutParams = new RelativeLayout.LayoutParams[4];
		for(int i=0; i<4; i++)
			layoutParams[i] = (RelativeLayout.LayoutParams)mLayoutTabMenus[i].getLayoutParams();
		
		mLayoutTab.removeAllViews();
		
		for(int i=0; i<4; i++)
			mLayoutTabMenus[i].setLayoutParams(layoutParams[i]);
		
		mLayoutTab.addView(mLayoutTabMenus[0]);
		mLayoutTab.addView(mLayoutTabMenus[1]);
		mLayoutTab.addView(mLayoutTabMenus[2]);
		mLayoutTab.addView(mLayoutTabMenus[3]);
	}
	
	public void setTabLayout() {
		if (mTabIndex == 0) return;
		
		for (int i=0; i<mLayoutTabContents.length; i++) {
			mLayoutTabContents[i].setVisibility(View.INVISIBLE);
		}
		
		mLayoutTabContents[mTabIndex-1].setVisibility(View.VISIBLE);
		setGraphData();
	}
	
	private void setGraphData() {
		setGraphFinish();
		
		if (mDateIndex[mTabIndex-1] == 0) {
			mBtnPrev[mTabIndex-1].setBackgroundResource(R.drawable.word_left_disable);
		} else {
			mBtnPrev[mTabIndex-1].setBackgroundResource(R.drawable.btn_word_left);
		}
		if (mDateIndex[mTabIndex-1] == mDateList.size()-1) {
			mBtnNext[mTabIndex-1].setBackgroundResource(R.drawable.word_right_disable);
		} else {
			mBtnNext[mTabIndex-1].setBackgroundResource(R.drawable.btn_word_right);
		}
		
		mDateText[mTabIndex-1].setText(mDateList.get(mDateIndex[mTabIndex-1]).getDate());
		
		if (mTabIndex == 3) {
			ArrayList<Integer> graphData = mStudyResultDBMana.getTimeDataList(mScheduleData, 
														mDateList.get(mDateIndex[mTabIndex-1]).getStartValue(), 
														mDateList.get(mDateIndex[mTabIndex-1]).getEndValue());
			mTimeGraphView.setGraphData(graphData, mDateList.get(mDateIndex[mTabIndex-1]).getDays());
		} else if (mTabIndex == 2) {
			ArrayList<Integer> graphData = mStudyResultDBMana.getWordDataList(mScheduleData, 
							   							mDateList.get(mDateIndex[mTabIndex-1]).getStartValue(), 
							   							mDateList.get(mDateIndex[mTabIndex-1]).getEndValue());
			mWordGraphView.setGraphData(graphData, mDateList.get(mDateIndex[mTabIndex-1]).getDays());
		} else if (mTabIndex == 1) {
			ArrayList<Integer> graphData = mStudyResultDBMana.getMarkDataList(mScheduleData, 
					   									mDateList.get(mDateIndex[mTabIndex-1]).getStartValue(), 
					   									mDateList.get(mDateIndex[mTabIndex-1]).getEndValue());
			mMarkGraphView.setGraphData(graphData, mDateList.get(mDateIndex[mTabIndex-1]).getDays());
		}
	}
	
	private void setGraphFinish() {
		if (mTimeGraphView!=null) mTimeGraphView.finish();
		if (mWordGraphView!=null) mWordGraphView.finish();
		if (mMarkGraphView!=null) mMarkGraphView.finish();
	}
	
	private boolean openDB() {
		if (mStudyResultDBMana == null)
			mStudyResultDBMana = new StudyResultDBManager(this, mUId);
		
		if (mStudyResultDBMana == null)
			return false;
		
		if (mScheduleDBMana == null)
			mScheduleDBMana = new ScheduleDBManager(this, mUId);
		
		if (mScheduleDBMana == null)
			return false;
		
		if (mUserDbMana == null)
			mUserDbMana = new UserDBManager(this);
		
		if (mUserDbMana == null)
			return false;
		
		return true;
	}
	
	private void closeDB() {
		if (mStudyResultDBMana != null) {
			mStudyResultDBMana.close();
			mStudyResultDBMana = null;
		}
		
		if (mStudyResultDBMana != null) {
			mStudyResultDBMana.close();
			mStudyResultDBMana = null;
		}
		
		if (mUserDbMana != null) {
			mUserDbMana.close();
			mUserDbMana = null;
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		ESUtils.playBtnSound(this);

		showLooseFocusAinimation(mTabIndex);
		
		switch(arg0.getId()) {
		
			case R.id.btnTabTimeOn:
				mTabIndex = 3;
				break;
				
			case R.id.btnTabWordOn:
				mTabIndex = 2;
				break;
				
			case R.id.btnTabMarkOn:
				mTabIndex = 1;
				break;
				
			case R.id.btnExitOn:
				mTabIndex = 0;
				this.finish();
				break;
				
			case R.id.btnStudyResultTab1Left:
				if (mDateIndex[2] > 0) {
					mDateIndex[2]--;
					setGraphData();
				}
				break;
				
			case R.id.btnStudyResultTab2Left:
				if (mDateIndex[1] > 0) {
					mDateIndex[1]--;
					setGraphData();
				}
				break;
				
			case R.id.btnStudyResultTab3Left:
				if (mDateIndex[0] > 0) {
					mDateIndex[0]--;
					setGraphData();
				}
				break;
				
			case R.id.btnStudyResultTab1Right:
				if (mDateIndex[2] < mDateList.size()-1) {
					mDateIndex[2]++;
					setGraphData();
				}
				break;
				
			case R.id.btnStudyResultTab2Right:
				if (mDateIndex[1] < mDateList.size()-1) {
					mDateIndex[1]++;
					setGraphData();
				}
				break;
				
			case R.id.btnStudyResultTab3Right:
				if (mDateIndex[0] < mDateList.size()-1) {
					mDateIndex[0]++;
					setGraphData();
				}
				break;
		}		
		
		showOnFocusAnimation(mTabIndex, 150);
		setTabLayout();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		this.closeDB();		
		this.setGraphFinish();
		super.onDestroy();
	}
}
