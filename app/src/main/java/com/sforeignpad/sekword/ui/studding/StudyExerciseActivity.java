package com.sforeignpad.sekword.ui.studding;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sforeignpad.sekword.R;
import com.sforeignpad.sekword.common.Define;
import com.sforeignpad.sekword.database.PAKDBManager;
import com.sforeignpad.sekword.database.ScheduleDBManager;
import com.sforeignpad.sekword.database.SettingDBManager;
import com.sforeignpad.sekword.database.StudyDBManager;
import com.sforeignpad.sekword.database.UserDBManager;
import com.sforeignpad.sekword.database.WordpadDBManager;
import com.sforeignpad.sekword.structs.ScheduleDateData;
import com.sforeignpad.sekword.structs.WordData;
import com.sforeignpad.sekword.ui.studding.SekWordPopup.OnSEKLaunchListener;
import com.sforeignpad.sekword.ui.studding.study.StudyWordActivity;
import com.sforeignpad.sekword.ui.studding.test.TestLayout;
import com.sforeignpad.sekword.utils.ESUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class StudyExerciseActivity extends Activity {

	private FrameLayout m_layoutParent;
	

	private int m_nTrueResCount = 0;	//í•©ê²©í•œ ê°œìˆ˜
	private int m_nFalseResCount = 0;	//í‹€ë¦° ê°œìˆ˜
	
	private int m_nTestLimitTime = 7;	//ì„¤ì •ì—�ì„œ ì„¤ì •ë�œ ì‹œí—˜ì‹œê°„
	private int m_nTestLimitTime_typing = 14;	//ì„¤ì •ì—�ì„œ ì„¤ì •ë�œ ì‹œí—˜ì‹œê°„
	private int m_nCurrentTestProblemCount = 0;	//í˜„ìž¬ ë¬¸ì œì œì‹œìˆ˜
	private int m_nTotalTestProblemCount = 0;	//ì´�ì‹œí—˜ë¬¸ì œìˆ˜
	
	protected int m_nTestCurrentProblemIndex = -1;	//단계별 전체 문제개수
	protected Boolean[] is_successProblem = new Boolean[Define.LevelTestProblemCounts];
	
	private SettingDBManager mSettingDbMana = null;
	private StudyDBManager m_studyDbMana = null;
	private ScheduleDBManager m_scheduleDbMana = null;
	private WordpadDBManager m_wordpadDbMana = null;
	private UserDBManager m_userDbMana = null;
	private ScheduleDateData m_scheduleData = null;
	private PAKDBManager m_pakManager = null;

	private int m_nUserId = -1;		//ì‚¬ìš©ìž�ì•„ì�´ë””
	private int m_nScheduleId = -1;	//ì�¼ì •ì•„ì�´ë””
	private int m_nStudyScheduleId = -1;

	private boolean m_bIsTestOrder = false;		//ê¸‰ìˆ˜ì‹œí—˜ì�´ë©´ true, í•™ìŠµí›„ ì‹œí—˜ì�´ë©´ false
	private int[] m_arrTestCase = {1, 1, 1, 1, 1};	//ë§¤ ì‹œí—˜ë‹¨ê³„ì�˜ ìˆœì„œ
	private int m_nTestPhase = -1;				//ì‹œí—˜ë‹¨ê³„

	protected ArrayList<WordData> m_arrAllWordData = new ArrayList<WordData>();	//전체시험문제
	protected WordData[] m_arrAllWordDataArray = new WordData[40];

	private GridView 	gridWords;
	private FrameLayout frameResult;
	private FrameLayout frameWord;
	private TextView	textEng;
	private TextView	textKor;
	private ImageView 	imageWord;
	private ImageButton btnClose;

	private ImageButton btnStudyStart;
	private int[] sequence;
	private int	m_CurrentWordId;

	private static StudyExerciseActivity mThis = null;

	
	private ExerciseLayout m_exerciseLayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_study_exercise);

		/*m_exerciseLayout = new ExerciseLayout(this);
		m_layoutParent = (FrameLayout)findViewById(R.id.TestParentLayout);
		m_layoutParent.addView(m_exerciseLayout);*/

		mThis = this;

		Intent intent = getIntent();
		m_nUserId = intent.getIntExtra(Define.USER_ID, -1);
		m_nScheduleId = intent.getIntExtra(Define.SCHEDULE_ID, -1);
		m_nStudyScheduleId = intent.getIntExtra(Define.STUDY_SCHEDULE_ID, -1);

		m_bIsTestOrder = intent.getBooleanExtra(Define.IS_ORDERTEST, false);

		if(!isConnectDB()){
			finish();
			return;
		}
		
		if(m_bIsTestOrder) //ê¸‰ìˆ˜ì‹œí—˜ì�´ë�¼ë©´ ì„¤ì •ì—�ì„œ ì„ íƒ�í•œ ì‹œí—˜ì�¼ì •ì�„ ê°€ì ¸ì˜¨ë‹¤.
		{
			m_nTotalTestProblemCount = Define.LevelTestProblemCounts;
		}else{
			m_nTotalTestProblemCount = getTestCaseSchedule();
		}
		if(m_nScheduleId != -1) getScheduleData();
		
		getTestTimeData();
		
		initLayouts();

		String strMsg = this.getResources().getString(R.string.start_exercise);

		SekWordPopupOneButton popup;

		popup = new SekWordPopupOneButton(this);
		popup.setMessage(strMsg);
		popup.setListener(new SekWordPopupOneButton.OnSEKLaunchListener() {

			@Override
			public void onOK() {
				performExercise();
			}
		});
		popup.show();
		//performExercise();
	}
	
	private void finishActivity() {		
		disableAllTestLayout();
		closeDB();

	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		doBack();
		//super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		finishActivity();
		
		super.onDestroy();
		System.gc();
	}

	private void initLayouts() {
		
		Typeface textTypeFace_gulim = Typeface.createFromAsset(getAssets(), Define.TEXTFONT_GULIM_PATH);
		m_layoutParent = (FrameLayout)findViewById(R.id.TestParentLayout);

		gridWords = (GridView) findViewById(R.id.gridWords);
		gridWords.setVisibility(View.VISIBLE);

		gridWords.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				m_CurrentWordId = sequence[position];
				showWordDetail(true);
			}
		});

		gridWords.setHorizontalSpacing(50);
		gridWords.setVerticalSpacing(50);

		frameResult = (FrameLayout) findViewById(R.id.frameResult);
		frameResult.setVisibility(View.GONE);

		frameWord = (FrameLayout) findViewById(R.id.frameWord);
		frameWord.setVisibility(View.GONE);

		textEng = (TextView) findViewById(R.id.textEng);
		textKor = (TextView) findViewById(R.id.textKor);

		ESUtils.setTextViewTypeFaceByRes(this, textEng, Define.getEnglishFont());
		ESUtils.setTextViewTypeFaceByRes(this, textKor, Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textExerciseTitle1), Define.getMainFont());
		ESUtils.setTextViewTypeFaceByRes(this, (TextView) findViewById(R.id.textExerciseTitle2), Define.getMainFont());

		imageWord = (ImageView) findViewById(R.id.imageWord);

		btnClose = (ImageButton) findViewById(R.id.btnClose);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(StudyExerciseActivity.this);
				showWordDetail(false);
			}
		});

		btnStudyStart = (ImageButton) findViewById(R.id.btnStartStudy);
		btnStudyStart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ESUtils.playBtnSound(StudyExerciseActivity.this);
				Intent intent = new Intent(StudyExerciseActivity.this, StudyWordActivity.class);
				intent.putExtra(Define.USER_ID, m_nUserId);
				intent.putExtra(Define.SCHEDULE_ID, m_nStudyScheduleId);
				startActivity(intent);
				finish();
			}
		});

	}

	public String getImageFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 0);
	}

	public String getAudioFileName(int nWordID){
		return m_pakManager.getFileName(nWordID, 1);
	}

	private void showWordDetail(boolean bShow){
		if(bShow){
			WordData data = m_arrAllWordData.get(m_CurrentWordId);

			String strEng = data.getMeanEn();
			String strKor = data.getMeanKr();
			Boolean bSuccess = is_successProblem[m_CurrentWordId];
			String strImageId = data.getImage();
			Bitmap img_bitmap = null;
			try {
				//String imgPath = Define.IMAGE_PATH + strImageId + ".png";
				String imgPath = getImageFileName(Integer.parseInt(strImageId));
				FileInputStream fis = null;
				fis = new FileInputStream(imgPath);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPurgeable = true;
				Bitmap wordPic = BitmapFactory.decodeStream(fis,null,options);
				img_bitmap = Bitmap.createScaledBitmap( wordPic, 400, 250, true );
				fis.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}


			textEng.setText(strEng);
			textKor.setText(strKor);
			imageWord.setImageBitmap(img_bitmap);
			if(bSuccess){
				textEng.setTextColor(Color.BLACK);
			}else
			{
				textEng.setTextColor(Color.RED);
			}
			frameWord.setVisibility(View.VISIBLE);
		} else {
			frameWord.setVisibility(View.GONE);
		}
	}

	private void doBack() {

		m_exerciseLayout.setTestPause(true);
		showTestStopDlg();

	}

	public static void playWordVoice(String strVoice) {

		if(strVoice.contains("success")){
			ESUtils.playSuccessSound(mThis);
		}else if(strVoice.contains("fail")){
			ESUtils.playFailSound(mThis);
		}


	}

	/*public void playVoice(String strVoice){
		int resID=m_soundMapTest.get(strVoice).intValue();

		MediaPlayer mediaPlayer=MediaPlayer.create(this,resID);
		mediaPlayer.start();
	}*/
	
	//DBì ‘ì†�
	private boolean isConnectDB() {
		m_studyDbMana = new StudyDBManager(this, m_nUserId);
		if(m_studyDbMana == null)
			return false;
		
		m_scheduleDbMana = new ScheduleDBManager(this, m_nUserId);
		if(m_scheduleDbMana == null)
			return false;
		
		mSettingDbMana = new SettingDBManager(this);
		if(mSettingDbMana == null)
			return false;
		
		m_wordpadDbMana = new WordpadDBManager(this, m_nUserId);
		if(m_wordpadDbMana == null)
			return false;
		
		m_userDbMana = new UserDBManager(this);
		if(m_userDbMana == null)
			return false;

		m_pakManager = new PAKDBManager(this);
		if(m_pakManager == null)
			return false;

		return true;
	}
	
	private void closeDB() {
		if (m_studyDbMana != null) {
			m_studyDbMana.close();
			m_studyDbMana = null;
		}
		
		if (m_scheduleDbMana != null) {
			m_scheduleDbMana.close();
			m_scheduleDbMana = null;
		}
		
		if (mSettingDbMana != null) {
			mSettingDbMana.close();
			mSettingDbMana = null;
		}
		
		if(m_wordpadDbMana != null) {
			m_wordpadDbMana.close();
			m_wordpadDbMana = null;
		}
		
		if(m_userDbMana != null) {
			m_userDbMana.close();
			m_userDbMana = null;
		}
	}
	
	//ì„¤ì •ë�œ ì‹œí—˜ì‹œê°„ ì–»ê¸°
	private void getTestTimeData() {
		m_nTestLimitTime = mSettingDbMana.getTestTimeValue();
	}
	
	//ì�¼ì •ì•„ì�´ë””ì—� ë”°ë¥´ëŠ” ì�¼ì •ìž�ë£Œ ì–»ê¸°
	private void getScheduleData() {
		m_scheduleData = m_scheduleDbMana.getScheduleDateData(m_nScheduleId);
	}
		
	//ì„¤ì •ì—�ì„œ ì„ íƒ�í•œ í•™ìŠµí›„ ì‹œí—˜ì�¼ì • ì–»ê¸°
	private int getTestCaseSchedule() {
		String strMode = mSettingDbMana.getTestModeValue();
		String[] strTemp = strMode.split(",");
		String strPhase = "";
		
		for(int i=0; i<4; i++) { 
			m_arrTestCase[i] = Integer.valueOf(strTemp[i]);
			if(m_arrTestCase[i] == 1)
				strPhase += "1";
		}
		
		return strPhase.length() * 10;	//ì´�ë¬¸ì œìˆ˜ ê·€í™˜ã„´
	}
	
	//ëª¨ë“  ì‹œí—˜ë ˆì�´ì•„ì›ƒ Disable
	private void disableAllTestLayout() {
		m_exerciseLayout.finishTest();
		m_layoutParent.removeAllViews();
	}

	private void performExercise(){
		m_exerciseLayout = new ExerciseLayout(this);
		m_exerciseLayout.setListener(new ExerciseLayout.OnEngKorTestFinishListener() {

			@Override
			public void onAfter(int nTestTotalCount, int nTrueCount, int nFalseCount,int m_nTestCurrentProblemIndex_,Boolean[] is_successProblem_,ArrayList<WordData> m_arrAllWordData_) {
				// TODO Auto-generated method stub
				m_nCurrentTestProblemCount = nTestTotalCount;
				m_nTrueResCount = nTrueCount;
				m_nFalseResCount = nFalseCount;
				m_nTestCurrentProblemIndex = m_nTestCurrentProblemIndex_;
				is_successProblem = is_successProblem_;

				m_arrAllWordData = m_arrAllWordData_;

				m_nTestPhase ++;

				showWords();

				//performTest();
			}
		});

		m_exerciseLayout.setParent(this);
		m_layoutParent.addView(m_exerciseLayout);

		initTestLayouts(m_exerciseLayout);
		initWordData(m_exerciseLayout);
		m_exerciseLayout.setAllTestWordData(m_arrAllWordData);
		m_exerciseLayout.performEngKorTest();
	}

	private void showWords(){

		int nLength = 10;
		sequence = new int[nLength];
		int nCount = 0;
		for (int i = 0; i < nLength; i++){
			if(is_successProblem[i] == false){
				sequence[nCount] = i;
				nCount++;
			}
		}

		for (int i = 0; i < nLength; i++){
			if(is_successProblem[i] == true){
				sequence[nCount] = i;
				nCount++;
			}
		}
		//gridWords.setVisibility(View.VISIBLE);
		frameResult.setVisibility(View.VISIBLE);
		m_layoutParent.setVisibility(View.GONE);

		gridWords.setAdapter(new WordAdapter(this));
	}

	private void initTestLayouts(TestLayout layout) {
		layout.setDbMana(m_studyDbMana, m_scheduleDbMana, m_wordpadDbMana);
		layout.setScheduleData(m_scheduleData);
		layout.setTestTimeLimit(m_nTestLimitTime);
		layout.setCurrentProblemCount(m_nCurrentTestProblemCount);
		layout.setTrueFalseWordCount(m_nTrueResCount, m_nFalseResCount);
		layout.setCurrentProblemIndex(m_nTestCurrentProblemIndex);
		layout.setIs_successProblem(is_successProblem);
		layout.setIsTestOrder(m_bIsTestOrder);
		layout.setTotalProblemCount(m_nTotalTestProblemCount);
		layout.setScheduleId(m_nScheduleId);
	}
	
	private void initWordData(TestLayout layout) {
		ArrayList<WordData> m_arrAllWordData_ = new ArrayList<WordData>();
		m_arrAllWordData_ = layout.initWordData();
		this.m_arrAllWordData=m_arrAllWordData_;
	}

	
	//ì‹œí—˜ì¤‘ì§€ëŒ€í™”ì°½ í˜„ì‹œ
	private void showTestStopDlg() {

		SekWordPopup popup;

		popup = new SekWordPopup(StudyExerciseActivity.this);
		popup.setMessage(getString(R.string.study_stop));
		popup.setListener(new OnSEKLaunchListener() {

			@Override
			public void onOK() {
				setTestPause(false);
				disableAllTestLayout();
				finish();
				/*m_nTestPhase = -1;
				m_layoutFirstStart.setVisibility(View.VISIBLE);
				
				m_nTrueResCount = 0;	//í•©ê²©í•œ ê°œìˆ˜
				m_nFalseResCount = 0;	//í‹€ë¦° ê°œìˆ˜					
				m_nCurrentTestProblemCount = 0;	//ë¬¸ì œì œì‹œìˆ˜**/
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				setTestPause(false);
			}

		});
		
		popup.show();


	}
	
	private void setTestPause(boolean bPause) {
		m_exerciseLayout.setTestPause(bPause);
		/*switch(m_nTestPhase) {
		case 0:
			m_layoutKorEngTest.setTestPause(bPause);
			break;
		case 1:
			m_layoutEngKorTest.setTestPause(bPause);
			break;
		case 2:
			m_layoutEngVoiceTest.setTestPause(bPause);
			break;
		case 3:
			m_layoutEngTypingTest.setTestPause(bPause);
			break;
		}*/
	}

	private void sendScheduleInfo(boolean bIsOrderTest, int nOrder) {
		Intent intent = new Intent(Define.SCHEDULE_VIEW_ACTION);
		intent.putExtra(Define.STUDY_ORDER, nOrder);
		sendBroadcast(intent);
	}
	
	private void sendStudyComplete() {
		Intent intent = new Intent(Define.USER_ORDER_ACTION);
		this.sendBroadcast(intent);
	}

	public class WordAdapter extends BaseAdapter {

		private Context m_context;
		private LayoutInflater m_inflater;

		public WordAdapter(Context ctx) {
			m_context = ctx;
			m_inflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		@Override
		public int getCount() {
			return 10	;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			//if( row == null){
				row = m_inflater.inflate(R.layout.exercise_item, parent, false);
			//}

			int nIndex = sequence[position];
			TextView    textItemName = (TextView) row.findViewById(R.id.textItem);
			TextView 	textTemp = (TextView) row.findViewById(R.id.textTemp);
			ImageView	imageBack = (ImageView) row.findViewById(R.id.imgBack);

			WordData data = m_arrAllWordData.get(nIndex);

			String strText = data.getMeanEn();
			textItemName.setText(strText);
			textTemp.setText(strText);

			ESUtils.setTextViewTypeFaceByRes(m_context, textItemName, Define.getEnglishFont());
			ESUtils.setTextViewTypeFaceByRes(m_context, textTemp, Define.getEnglishFont());

			boolean bSuccess = is_successProblem[nIndex];

			if(bSuccess){
				textItemName.setTextColor(0x000000);
				//textTemp.setTextColor(0x000000);
				//textTemp.setTextColor(Color.BLACK);
				textTemp.setTextColor(getResources().getColor(R.color.review_true));
				imageBack.setImageResource(R.drawable.review_item_true);


			}else{
				textItemName.setTextColor(0xff0000);
				//textTemp.setTextColor(0x000000);
				//textTemp.setTextColor(Color.RED);
				textTemp.setTextColor(getResources().getColor(R.color.review_false));
				imageBack.setImageResource(R.drawable.review_item_false);


			}
			return row;
		}
	}
}
